<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    /**
     * 基本設定を行う
     * ※フロント、管理画面含む全てに関わる記述のみ追加すること
     * TODO:公開時、プロファイラをFALSEにするのを忘れずに
     */
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Tokyo');
        error_reporting(E_ALL & ~E_NOTICE);
        //phpinfo();
        if (ENVIRONMENT === 'development') {
            //$this->output->enable_profiler(TRUE);
        }
        //$this->output->enable_profiler(true);

        //管理画面継続ログイン
        //pre_print_r($_COOKIE);
        if(isset($_COOKIE["user_code"])){
            //pre_print_r($_COOKIE["user_code"]);
            $val = $_COOKIE["user_code"];
        }else{
            $val = '';
        }
        //$val = get_cookie('user_code');
        if ($val != '') {
            $this->db->select('*');
            $this->db->where('m_admin.user_code', $val);
            $query = $this->db->get('m_admin');
            $result = $query->row_array();
            if ($result != array()) {
                $_SESSION['admin_dat'] = $result;
            }
        }

        ///////////////UA判別///////////////////
        $this->ua = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
        if (strpos($this->ua, 'iphone') !== false) {
            $device = 'mobile';
        } elseif (strpos($this->ua, 'ipod') !== false) {
            $device = 'mobile';
        } elseif ((strpos($this->ua, 'android') !== false) && (strpos($this->ua, 'mobile') !== false)) {
            $device = 'mobile';
        } elseif ((strpos($this->ua, 'windows') !== false) && (strpos($this->ua, 'phone') !== false)) {
            $device = 'mobile';
        } elseif ((strpos($this->ua, 'firefox') !== false) && (strpos($this->ua, 'mobile') !== false)) {
            $device = 'mobile';
        } elseif (strpos($this->ua, 'blackberry') !== false) {
            $device = 'mobile';
        } elseif (strpos($this->ua, 'ipad') !== false) {
            $device = 'tablet';
        } elseif ((strpos($this->ua, 'windows') !== false) && (strpos($this->ua, 'touch') !== false && (strpos($this->ua, 'tablet pc') == false))) {
            $device = 'tablet';
        } elseif ((strpos($this->ua, 'android') !== false) && (strpos($this->ua, 'mobile') === false)) {
            $device = 'tablet';
        } elseif ((strpos($this->ua, 'firefox') !== false) && (strpos($this->ua, 'tablet') !== false)) {
            $device = 'tablet';
        } elseif ((strpos($this->ua, 'kindle') !== false) || (strpos($this->ua, 'silk') !== false)) {
            $device = 'tablet';
        } elseif ((strpos($this->ua, 'playbook') !== false)) {
            $device = 'tablet';
        } else {
            $device = 'others';
        }
        if ($device === "mobile") {
            //$this->session->set_userdata('ua', 'sp');
        } elseif ($device === "tablet") {
            //$this->session->set_userdata('ua', 'sp');//tabletもスマホ
        } else {
            //$this->session->set_userdata('ua', 'pc');
        }
        ///////////////////////////////////////////////////////
    }

    /**
     * サブドメイン取得
     * @return bool
     */
    function get_subdomain()
    {
        $url = $_SERVER['HTTP_HOST'];
        $domain_array = explode('.', $url);
        $sub = $domain_array[0];
        $c_domain = count($domain_array);

        if ($c_domain > 2) {
            return $sub;
        } else {
            return false;
        }
    }

    /**
     * 表示画面レンダリング
     * @param $view
     * @param string $dat
     * @param string $type
     */
    protected function disp($view, $dat = '')
    {
        $this->load->view('/layout/header', $dat);
        //$this->load->view('/layout/side', $dat);
        $this->load->view($view, $dat);
        $this->load->view('/layout/footer', $dat);
    }

    /**
     * メール送信ロジック
     * @param $title //メールタイトル
     * @param $template //テンプレートファイル名
     * @param $dat //データ
     * @param $mail //送信先メール
     * @param $bcc_mail //bccあれば
     * @param $callback //送信後処理
     * @param string $url //メール記載URL
     */
    public function send_mail($title, $template, $dat, $mail, $bcc_mail = array(), $callback = '', $url = '')
    {
        $this->load->library('email');
        $this->load->library('parser');
        $this->load->library('MY_Email');

        $dat['reply_mail'] = REPLY_MAIL;
        $dat['admin_mail'] = ADMIN_MAIL;
        $dat['date'] = date('Y年m月d日 H:i');
        $dat['date_e'] = date('M d,Y H:i');
        if($template == 'answer'){
            $dat['date'] = $dat['created_at'];
            $dat['date_e'] = $dat['created_at'];
        }
        if($template == 'answer_to_admin'){
            $dat['date'] = $dat['created_at'];
            $dat['date_e'] = $dat['created_at'];
        }
        if($template == 'a_change'){
            $dat['date'] = $dat['created_at'];
            $dat['date_e'] = $dat['created_at'];
        }
        //$dat['id'] = sprintf('%04d', $dat['id']);
        $dat['url'] = $url;
        $dat['admin_url'] = base_url('admin');
        $dat['base_url'] = base_url();

        $message = $this->parser->parse('template/' . $template, $dat, TRUE);

        /**
         * commit注意
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = '465';
        $config['smtp_auth'] = true;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bassmicrobe@gmail.com';
        $config['smtp_pass'] = 'uofhqjtjeejusigq';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'text';
        $config['wordwrap'] = FALSE;
        $config['newline'] = "\r\n";
        $config['validation'] = FALSE; // bool whether to validate email or not
        */

        $config['charset'] = 'UTF-8';	//文字コードを設定
        $config['wrapchars'] = 76;	//改行文字数を設定
        $config['crlf'] = '\r\n';	//改行コードを設定
        $config['newline'] = '\n';	//改行文字を設定
        $config['wordwrap'] = false;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(ADMIN_MAIL);//送信元メールアドレス todo:system
        $this->email->to($mail);

        if ($bcc_mail != array()) {//
            foreach ($bcc_mail as $m) {
                $send_mail[] = $m;
            }
            $this->email->cc($send_mail);//
        }
        $this->email->subject($title);
        $this->email->message($message);
        $this->email->set_wordwrap(FALSE);

        if ($this->email->send()) {
            if ($callback != '') {
                redirect($callback);
            }
        } else {
            print_r('送信エラー');
            echo $this->email->print_debugger();
            exit;
        }
    }
}

/**
 * マイグレーション
 */
class MY_Dbforge extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
}

/**
 * 管理者画面用コンストラクタ
 */
class AdminController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['admin_dat'])) {
            //redirect();
            redirect('center/login?role=admin');
        }
    }
}

/**
 * 回答者画面用コンストラクタ
 */
class AnswerController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['answer_dat'])) {
            //redirect();
            redirect('center/login?role=answer');
        }
    }
}

