<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * 半角数字
 * @param $text
 * @return bool
 */
function is_num($text)
{
    if (preg_match("/^[0-9]+$/", $text)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * メールアドレス正規化
 * @param $text
 * @return bool
 */
function is_mail($text)
{
    if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $text)) {
        return TRUE;
    } else {
        return FALSE;
    }
}


/**
 * URL正規化
 * @param $text
 * @return bool
 */
function is_url($text)
{
    if (preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $text)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

