<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * デバッグ用
 * @param $dat
 */
function pre_print_r($dat)
{
    echo '<pre>';
    print_r($dat);
    echo '</pre>';
}

/**
 * 半角数字に変換
 * @param $str
 * @return string
 */
function half($str)
{
    return mb_convert_kana($str, 'n');
}


/**
 * 改行
 * @param $str
 * @return string
 */
function br($str)
{
    return str_replace(["\r\n","\r","\n"], "<br>", $str);
}


/**
* 行末文字削除
 * @param $str
 * @return string
 */
function eol($str)
{
    return str_replace(PHP_EOL, "", $str);
}



/**
 * 全角スペース->半角スペース
 * @param $str
 * @return string
 */
function sp($str)
{
    if($str == 'NULL'){
        $kana = '';
    }else{
        $kana = mb_convert_kana($str, 's', 'UTF-8');
    }
    return $kana;
}

/**
 * 環境判定
 * @return bool
 */
function decision()
{
    $S_AD = $_SERVER['SERVER_ADDR'];
    $R_AD = $_SERVER['REMOTE_ADDR'];

    if(substr($S_AD,0,mb_strrpos($S_AD,'.')) != substr($R_AD,0,mb_strrpos($R_AD,'.'))){
        return true;//production
    }else{
        return false;//development
    }
}

/**
 * ランダム文字列生成（ケタ数指定あり）
 * @param $length
 * @return null|string
 */
function makeRandStr($length) {
    $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
    $r_str = null;
    for ($i = 0; $i < $length; $i++) {
        $r_str .= $str[rand(0, count($str) - 1)];
    }
    return $r_str;
}

/**
 * ランダム文字列生成（ケタ数）
 * @param $length
 * @return null|string
 */
function makeRandInt($length) {
    $int = range('1', '9');
    $r_int = null;
    for ($i = 0; $i < $length; $i++) {
        $r_int .= $int[rand(0, count($int) - 1)];
    }
    return $r_int;
}

/**
 * 拡張子取得
 * @param $a
 * @return null|string
 */
function extension($a) {
    $b = strrpos($a, ".");
    //substr( "orange", 2 );
    return substr($a, $b);
}

/**
 * 配列内の全ての値が一意かチェックする
 * @param $target_array
 * @return bool
 */
function isUniqueArray ($target_array) {
    $unique_array = array_unique($target_array);
    if (count($unique_array) === count($target_array)) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * 文中にURLがあればリンクにする
 * @param $str
 * @return mixed
 */
function autoLinker($str)
{
    $pat_sub = preg_quote('-._~%:/?#[]@!$&\'()*+,;=', '/'); // 正規表現向けのエスケープ処理
    $pat = '/((http|https):\/\/[0-9a-z' . $pat_sub . ']+)/i'; // 正規表現パターン
    $rep = '<a href="\\1">\\1</a>'; // \\1が正規表現にマッチした文字列に置き換わります

    $str = preg_replace($pat, $rep, $str); // 実処理
    return $str;
}

/**
 * pager生成
 * @param $now_page
 * @param $total
 * @param $count
 * @param $key
 */
function pager($now_page, $total, $count, $key)
{
    $current_page = $now_page;     //現在のページ
    $total_rec = $total;    //総レコード数
    $page_rec = $count;   //１ページに表示するレコード
    $total_page = ceil($total_rec / $page_rec); //総ページ数
    $show_nav = 5;  //表示するナビゲーションの数
    $path = '?page=';   //パーマリンク

    //全てのページ数が表示するページ数より小さい場合、総ページを表示する数にする
    if ($total_page < $show_nav) {
        $show_nav = $total_page;
    }
    //トータルページ数が2以下か、現在のページが総ページより大きい場合表示しない
    if ($total_page <= 1 || $total_page < $current_page) return;
    //総ページの半分
    $show_navh = floor($show_nav / 2);
    //現在のページをナビゲーションの中心にする
    $loop_start = $current_page - $show_navh;
    $loop_end = $current_page + $show_navh;
    //現在のページが両端だったら端にくるようにする

    if ($loop_start <= 0) {
        $loop_start = 1;
        $loop_end = $show_nav;
    }
    if ($loop_end > $total_page) {
        $loop_start = $total_page - $show_nav + 1;
        $loop_end = $total_page;
    }

    ?>

    <div class="input-field btn-aligncenter mt50">
        <ul class="pagination">
            <?php
            //2ページ移行だったら「一番前へ」を表示
            //            $q_first = http_build_query(array('page' => 1) + $_GET, '', '&amp;');
            //            if ($current_page > 2) echo '<li class="prev"><a href="' .base_url($key) . '/?' . $q_first . '">&laquo;</a></li>';
            //最初のページ以外だったら「前へ」を表示
            $q_prev = http_build_query(array('page' => ($current_page - 1)) + $_GET, '', '&amp;');
            if ($current_page > 1){
                echo '<li class="waves-effect"><a href="' . base_url($key) . '/?' . $q_prev . '"><i class="material-icons">chevron_left</i></a></li>';
            }else{
                echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>';
            }
            for ($i = $loop_start; $i <= $loop_end; $i++) {
                $q = http_build_query(array('page' => $i) + $_GET, '', '&amp;');
                if ($i > 0 && $total_page >= $i) {
                    if ($i == $current_page) echo '<li class="active">';
                    else echo '<li class="waves-effect">';
                    echo '<a href="' . base_url($key) . '/?' . $q . '">' . $i . '</a>';
                    echo '</li>';
                }
            }
            //最後のページ以外だったら「次へ」を表示
            $q_next = http_build_query(array('page' => ($current_page + 1)) + $_GET, '', '&amp;');
            if ($current_page < $total_page){
                echo '<li class="waves-effect"><a href="' . base_url($key) . '/?' . $q_next . '"><i class="material-icons">chevron_right</i></a></li>';
            }else{
                echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>';
            }
            //最後から２ページ前だったら「一番最後へ」を表示
            //            $q_last = http_build_query(array('page' => $total_page) + $_GET, '', '&amp;');
            //            if ($current_page < $total_page - 1) echo '<li class="next"><a href="' .base_url($key) . '/?' . $q_last . '">&raquo;</a></li>';
            ?>
        </ul>
    </div>
    <?php
}

/**
 * サイドメニュー　動的にアクティブにする
 * @param string $uri
 * @return bool
 */
function is_current($uri = "")
{
    $uri = trim($uri, "/");
    $request_uri = $_SERVER['REQUEST_URI'];

    if ($uri && strpos($request_uri . "/", "/" . $uri . "/", 0) !== FALSE) {
        return true;
    }
    $request_uri = trim(str_replace("/index.php", "", $request_uri), '/');
    if (!$uri && !$request_uri) {
        return true;
    }
    return false;
}

/**
 * 上の関連
 * @param string $uri
 */
function _echo_current($uri = "")
{
    $request_uri = $_SERVER['REQUEST_URI'];
    $request_uri = trim(str_replace("/index.php", "", $request_uri), '/');
    $keys = parse_url($request_uri); //パース処理
    $path = explode("/", $keys['path']); //分割処理
    $last = end($path); //最後の要素を取得
    if ($last == $uri) {
        echo 'm-menu__item--active';
    };
}

/**
 * 上の関連
 * @param string $uri
 */
function echo_current($uri = "")
{
    if (is_current($uri)) {
        echo 'm-menu__item--active';
    };
}

/**
 * 上の関連（メニュー開閉）
 * @param string $uri
 */
function echo_current_open($uri = "")
{
    if (is_current($uri)) {
        echo 'm-menu__item--open m-menu__item--expanded';
    };
}

/**
 * 為替取得
 * @param $now_page
 * @param $total
 * @param $count
 * @param $key
 */
function exchange($now_page, $total, $count, $key)
{
    $result = file_get_contents('https://www.gaitameonline.com/rateaj/getrate');
    pre_print_r(json_decode($result));

}

/**
 * 自動翻訳（200万文字まで
 * @param $text
 * @param $to_lang
 * @param string $from_lang
 * @return bool|string
 */
function auto_trans($text, $to_lang, $from_lang = 'ja')
{

    if ($to_lang == $from_lang) {
        return nl2br($text);
    } else {
        $url = 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken';
        $options =
            [
                'http' => [
                    'method' => 'POST',
                    'content' => '',
                    'header' => [
                        'Content-Length: 0',
                        'Ocp-Apim-Subscription-Key: 734c108b0c1449f1956bbc45804ce207'
                    ]
                ]
            ];
        //トークン取得
        $contents = file_get_contents($url, false, stream_context_create($options));

        $token = $contents;
        $text = $text;
        $src = $from_lang;//翻訳元言語コード
        $dst = $to_lang;//翻訳先言語コード

        $data = array(
            'text' => $text,
            'from' => $src,
            'to' => $dst
        );

        $header = array(
            'Authorization: Bearer ' . $token,
            'Content-Type: application/x-www-form-urlencoded',
            'Access-Control-Allow-Origin: http://api.microsofttranslator.com'
        );

        $context = stream_context_create(array(
            "http" => array(
                "method" => "GET",
                "header" => implode("\r\n", $header),
            )
        ));

        $response = file_get_contents(
            'https://api.microsofttranslator.com/v2/Http.svc/Translate?' . http_build_query($data, "", "&"),
            false,
            $context
        );

        return strip_tags(nl2br($response));
    }
}


/**
 * 為替
 * @param $amount
 * @param $from
 * @param $to
 * @return bool
 */
function convert_currency($amount, $from, $to)
{
    $result=file_get_contents("http://info.finance.yahoo.co.jp/fx/convert/?a=".$amount."&s=".$from."&t=".$to);
    $pattern="/.*<td class=\"price noLine\">(.*)<\/td>.*/i";
    preg_match($pattern, $result, $matches);
    //$messages="1000ウォン-> $matches[1] 円";
    $num = str_replace(',','',$matches[1]);
    $result=floor($num);
    return $result;
}

/**
 * 時間
 * @param $range
 * @return array
 * @throws Exception
 */
function timeRange($range)
{
    $formatter = function ($datetime) {
        return $datetime->format('H:i');
    };
    $hours = array_map($formatter, iterator_to_array(new DatePeriod(
        new DateTime('1970-01-01 00:00:00'),
        new DateInterval('PT' . $range . 'M'),
        new DateTime('1970-01-01 23:59:59')
    )));
    return $hours;
}

/**
 * 都道譜面名取得
 * @param $prefecture_code
 * @return mixed
 */
function pref($prefecture_code)
{
    $this->load->model('master_model');
    $pref_name = $this->master_model->getPrefName($prefecture_code);
    return $pref_name;
}

/**
 * 市区町村名取得
 * @param $city_code
 * @return mixed
 */
function city($city_code)
{
    $this->load->model('master_model');
    $city_name = $this->master_model->getCityName($city_code);
    return $city_name;
}

// 暗号化
function token_encode($key, $plain_text)
{
    $td = base64_encode($plain_text);
    $len = strlen($td);
    for ($n = 1; $n <= 3; $n++) {
        for ($i = 0; $i < strlen($key); $i++) {
            $p = ord(substr($key, $i, 1)) % $len;
            $p1 = ($p + $n) % $len;
            $p2 = ($len - $p + $n) % $len;
            $c1 = substr($td, $p1, 1);
            $c2 = substr($td, $p2, 1);
            $td = substr_replace($td, $c2, $p1, 1);
            $td = substr_replace($td, $c1, $p2, 1);
        }
    }
    return sprintf("%04d", $len) . $td;
}

// 暗号の復元
function token_decode($key, $crypt_text)
{
    $len = (int)substr($crypt_text, 0, 4);
    $td = substr($crypt_text, 4, $len);
    for ($n = 3; $n >= 1; $n--) {
        for ($i = strlen($key) - 1; $i >= 0; $i--) {
            $p = ord(substr($key, $i, 1)) % $len;
            $p1 = ($p + $n) % $len;
            $p2 = ($len - $p + $n) % $len;
            $c1 = substr($td, $p1, 1);
            $c2 = substr($td, $p2, 1);
            $td = substr_replace($td, $c2, $p1, 1);
            $td = substr_replace($td, $c1, $p2, 1);
        }
    }
    $td = base64_decode($td);
    return $td;
}


///////sample
/**
 * POSTsample
 **/
function post_data()
{
    $json = array(
        'hoge' => 'fuga',
    );

    $headers = array(
        'Content-Type: application/json',
    );

    $context = stream_context_create(array(
        'http' => array(
            'method' => 'POST',
            'header' => implode("\n", $headers),
            'content' => json_encode($json),
        ),
    ));
    print_r($this->input->post());
}