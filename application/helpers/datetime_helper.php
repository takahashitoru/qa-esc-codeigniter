<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 経過時間を算出
 * @param $time
 * @return string
 */
function diff($time)
{
    $spltime = preg_split("/[ :-]/", $time);//正規表現を用いて第１引数に該当する文字列で配列化
    //MacOSの空白だとこう。windowsの空白だとpreg_split("/[¥s:-]");かも？
    $post_time = mktime($spltime[3], $spltime[4], $spltime[5], $spltime[1], $spltime[2], $spltime[0]);
    //配列化された文字列を使って1970年1月1日0時0分0秒からの経過秒数を算出
    //mktime(時,分,秒,月,日,年)の順で入れると秒数を返してくれる。

    $cur_time = time();//1970年1月1日0時0分0秒から現在までの経過秒数を算出
    $dif_time = $cur_time - $post_time;//現在時刻と$timeで得た時刻の差の計算

    if ($dif_time < 60) {
        $diff = floor($dif_time) . "秒前";//差が60秒より小さければ秒数を表示
    } else if ($dif_time <= 3600) {//1時間は3600秒なので分が出せる
        $diff = floor($dif_time / 60) . "分前";//floor関数で端数切捨て
    } else if ($dif_time <= 3600 * 24) {//1日は3600×24秒なので時間が出せる
        $diff = floor($dif_time / 3600) . "時間前";//floor関数で端数切捨て
    } else if ($dif_time <= 3600 * 24 * 365) {//1年は3600*24*365なので日数が出せる
        $diff = floor($dif_time / (3600 * 24)) . "日前";//floor関数で端数切捨て
    } else {
        $diff = '-';
    }
    //echo date('Y-m-d H:i:s')
    return $diff;
}

/**
 * 経過時間を算出によりセルの色を変更
 * @param $time
 * @return string
 */
function diff_color($time)
{
    $spltime = preg_split("/[ :-]/", $time);//正規表現を用いて第１引数に該当する文字列で配列化
    //MacOSの空白だとこう。windowsの空白だとpreg_split("/[¥s:-]");かも？
    $post_time = mktime($spltime[3], $spltime[4], $spltime[5], $spltime[1], $spltime[2], $spltime[0]);
    //配列化された文字列を使って1970年1月1日0時0分0秒からの経過秒数を算出
    //mktime(時,分,秒,月,日,年)の順で入れると秒数を返してくれる。

    $cur_time = time();//1970年1月1日0時0分0秒から現在までの経過秒数を算出
    $dif_time = $cur_time - $post_time;//現在時刻と$timeで得た時刻の差の計算

    if ($dif_time < 60) {
        $diff = 'background-color:aqua;';//差が60秒より小さければ秒数を表示
    } else if ($dif_time <= 3600) {//1時間は3600秒なので分が出せる
        $diff = 'background-color:#FFFF99;';//floor関数で端数切捨て
    } else if ($dif_time <= 3600 * 24) {//1日は3600×24秒なので時間が出せる
        $diff = 'background-color:#FF9999;';//floor関数で端数切捨て
    } else if ($dif_time <= 3600 * 24 * 365) {//1年は3600*24*365なので日数が出せる
        $diff = 'background-color:#FF9999;';//floor関数で端数切捨て
    } else {
        $diff = '';
    }
    return $diff;
}

/**
 * open to last 判別
 * @param $date
 * @param $open_dat
 * @param $close_dat
 * @param $time_range
 * @return string
 */
function time_diff($date, $open_dat, $close_dat, $time_range)
{
    //date('Y-m-d', strtotime('2017-02-09 +1 day'));sample
    //date("Y-m-d H:i:s",strtotime("$test +1 hours"));;sample
    //$close_dat_b = '19:00';//テスト
    $last_close = date("H:i", strtotime($close_dat . " -2 hours"));//予約は終了の2時間前まで

    $open_dat_a = strtotime($open_dat);
    $close_dat_a = strtotime($last_close);//
    $tmp_time = strtotime("23:59");
    $now = date('H:i');
    if (($open_dat_a < $close_dat_a) && ($close_dat_a <= $tmp_time)) {
        //echo 'day';
        $flag = 1;
        $open_dat = strtotime($date . " " . $open_dat);
        $close_dat = strtotime($date . " " . $last_close);
        if ($date == date('Y-m-d')) {
            if ($open_dat_a < strtotime($now)) {
                foreach ($time_range as $tr => $tr_val) {
                    if (strtotime($now) < strtotime($tr_val)) {
                        $open_dat = strtotime($tr_val);
                        break;
                    }
                }
            }
        }
    } else {
        //echo 'allnight';
        $flag = 2;
        $next_date = date('Y-m-d', strtotime($date . ' +1 day'));
        $open_dat = strtotime($date . " " . $open_dat);
        $close_dat = strtotime($next_date . " " . $last_close);
        if (strtotime('05:00') > strtotime(date('H:i'))) {
            $target = date('Y-m-d', strtotime('-1 day', time()));
        } else {
            $target = date('Y-m-d');
        }
        if ($target == $date) {
            if ($open_dat_a < strtotime($now)) {
                foreach ($time_range as $tr => $tr_val) {
                    if (strtotime($now) < strtotime($tr_val)) {
                        $open_dat = strtotime($tr_val);
                        break;
                    }
                }
            }
            if ($close_dat_a > strtotime($now)) {
                foreach ($time_range as $tr => $tr_val) {
                    if (strtotime($now) < strtotime($tr_val)) {
                        $open_dat = strtotime($tr_val);
                        break;
                    }
                }
                $flag = 1;
            }
        }
    }

    $result['flag'] = $flag;
    //$result['open'] = $open_dat;
    $result['open'] = (date('H:i', ($open_dat)));
    //$result['close'] = $close_dat;
    $result['close'] = (date('H:i', ($close_dat)));
    //print_r($result);
    //print_r(strtotime(date('H:i', ($open_dat))));
    //echo '<br/>';
    //print_r(date('Y-m-d H:i:s', ($close_dat)));
    //echo '<br/>';
    //print_r($close_dat_a);
    //echo '<br/>';
    //print_r($tmp_time);
    //exit;
    return $result;
}

/**
 * 予約時間プルダウン
 * @param $date
 * @param $open_dat
 * @param $close_dat
 * @param $time_range
 * @param string $form_dat
 * @param $start //プラン受付開始時間
 * @param $end //プラン受付終了時間
 * @return string
 */
function reserve_time($date, $open_dat, $close_dat, $time_range, $form_dat = '', $start='', $end='')
{

    $time_dat = time_diff($date, $open_dat, $close_dat, $time_range);
    if($start != ''){
        $open = $start;
    }else{
        $open = $time_dat['open'];
    }
    if($end != ''){
        $close = $end;
    }else{
        $close = $time_dat['close'];
    }
    $flag = $time_dat['flag'];
    $now = strtotime(date('Y-m-d H:i:s'));
    if ($open < $now) {
        $now_r = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i", ($now)) . ' +1 hour'));
        //$open = strtotime($now_r);
        if ($close < strtotime($now_r)) {
            //$open = strtotime($now_r);
        } else {
            $open = strtotime($now_r);
        }
    }

    if ($flag == 1) {//day
        //exit;
        $result = '';
        foreach ($time_range as $tr => $tr_val) {
            if (strtotime($open) > strtotime($tr_val)) {
                continue;
            }
            if (strtotime($close) < strtotime($tr_val)) {
                break;
            }
            $sel = '';
            if ($form_dat != '') {
                if ($form_dat == $tr_val) {
                    $sel = 'selected';
                }
            }
            $result .= '<option value="' . $tr_val . '"' . $sel . '>' . $tr_val . '</option>';
        }
    } elseif ($flag == 2) {//allnight
        $result = '';
        foreach ($time_range as $tr => $tr_val) {
            if (strtotime($open) > strtotime($tr_val)) {
                continue;
            }
            if (strtotime($close) < strtotime($tr_val)) {
                //break;
            }
            $sel = '';
            if ($form_dat != '') {
                if ($form_dat == $tr_val) {
                    $sel = 'selected';
                }
            }
            $result .= '<option value="' . $tr_val . '"' . $sel . '>' . $tr_val . '</option>';

        }
        foreach ($time_range as $tr_a => $tr_val_a) {
            //$targetTime = strtotime(date('Y-m-d') . ' ' . $tr_val_a);
            if (strtotime($close) < strtotime($tr_val_a)) {
                break;
            }
            $sel = '';
            if ($form_dat != '') {
                if ($form_dat == $tr_val_a) {
                    $sel = 'selected';
                }
            }
            $result .= '<option value="' . $tr_val_a . '"' . $sel . '>' . $tr_val_a . '</option>';

        }
        //exit;
    }
    return $result;
}

/**
 * 当日予約判別
 * //5時から29時までの予約は当日予約
 * @param $datetime
 * @return false|string
 */
function today_reserve($datetime)
{
    $targetTime = strtotime($datetime);
    $next_time = date('Y-m-d H:i:s', strtotime('+1 day', date($targetTime)));
    $next_date = date('Y-m-d', strtotime('+1 day', date($targetTime)));
    $now_time = $datetime;
    $now_date = date('Y-m-d', $targetTime);

    //日付け判別//5時から29時までの予約は当日予約
    if ($targetTime > strtotime(date('Y-m-d 05:00:00'))) {
        $result["date"] = date('Y-m-d');
        $result["flag"] = true;
    } else {
        $result["date"] = date('Y-m-d', strtotime('-1 day'));
        $result["flag"] = false;
    }

    return $result;
}

/**
 * google API 祝日取得
 * @param $year
 * @param $month
 * @return array
 */
function get_holidays_this_month($year, $month)
{
    // 月初日
    $first_day = mktime(0, 0, 0, intval($month), 1, intval($year));
    // 月末日
    $last_day = strtotime('-1 day', mktime(0, 0, 0, intval($month) + 1, 1, intval($year)));
    $api_key = 'AIzaSyAwoAr939xXyNicmvRiOf0NcMhsJUefdXA';
    //$holidays_id = 'outid3el0qkcrsuf89fltf7a4qbacgt9@import.calendar.google.com';  // mozilla.org版
    $holidays_id = 'japanese__ja@holiday.calendar.google.com';  // Google 公式版日本語
    //$holidays_id = 'japanese@holiday.calendar.google.com';  // Google 公式版英語
    $holidays_url = sprintf(
        'https://www.googleapis.com/calendar/v3/calendars/%s/events?' .
        'key=%s&timeMin=%s&timeMax=%s&maxResults=%d&orderBy=startTime&singleEvents=true',
        $holidays_id,
        $api_key,
        date('Y-m-d', $first_day) . 'T00:00:00Z',  // 取得開始日
        date('Y-m-d', $last_day) . 'T00:00:00Z',   // 取得終了日
        31            // 最大取得数
    );
    if ($results = file_get_contents($holidays_url)) {
        $results = json_decode($results);
        $holidays = array();
        foreach ($results->items as $item) {
            $date = strtotime((string)$item->start->date);
            $title = (string)$item->summary;
            $holidays[date('Y-m-d', $date)] = $title;
        }
        ksort($holidays);
    }
    return $holidays;
}

/**
 * 経過営業日取得
 * @param $date //当日
 * @param $count //営業日
 * @return DateTime|false|int
 */
function get_course($date, $count)
{
    $year = substr($date, 0, 4);
    $month = substr($date, 5, 2);
    $day = substr($date, 8, 2);

    // 月初日
    $first_day = mktime(0, 0, 0, intval($month), 1, intval($year));
    // 月末日
    $last_day = strtotime('-1 day', mktime(0, 0, 0, intval($month) + 1, 1, intval($year)));
    $api_key = 'AIzaSyAwoAr939xXyNicmvRiOf0NcMhsJUefdXA';
    //$holidays_id = 'outid3el0qkcrsuf89fltf7a4qbacgt9@import.calendar.google.com';  // mozilla.org版
    $holidays_id = 'japanese__ja@holiday.calendar.google.com';  // Google 公式版日本語
    //$holidays_id = 'japanese@holiday.calendar.google.com';  // Google 公式版英語
    $holidays_url = sprintf(
        'https://www.googleapis.com/calendar/v3/calendars/%s/events?' .
        'key=%s&timeMin=%s&timeMax=%s&maxResults=%d&orderBy=startTime&singleEvents=true',
        $holidays_id,
        $api_key,
        date('Y-m-d', $first_day) . 'T00:00:00Z',  // 取得開始日
        date('Y-m-d', $last_day) . 'T00:00:00Z',   // 取得終了日
        31            // 最大取得数
    );
    if ($results = file_get_contents($holidays_url)) {
        $results = json_decode($results);
        $holidays = array();
        foreach ($results->items as $item) {
            $date = strtotime((string)$item->start->date);
            $title = (string)$item->summary;
            $holidays[date('Y-m-d', $date)] = $title;
        }
        ksort($holidays);
    }

    $date = new DateTime($year . '-' . $month . '-' . $day);
    $oneday = new DateInterval("P1D");
    while ($count > 0) {
        $date->add($oneday);
        $str = $date->format("Y-m-d");
        $w = $date->format("w");
        if ($w < 1 #日曜
            || $w > 5 #土曜
            || isset($holiday[$str])
        ) { #祝日
            continue;
        }
        $count--;
    }
    return $date;
}