<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 画像パスを返す
 * @param $id
 * @param $type // クラブ写真
 * @param string $file_name
 * @param string $ext //拡張子
 * @param string $path_return
 * @param string $thumbnail
 * @return string
 */
function img_path($id, $type, $file_name = "", $ext = "", $path_return = "", $thumbnail = "")
{
    $base_path = "./shared_img/";

    $relative_path = $base_path . $type . '/' . $id . '/';
    $full_path = base_url() . "shared_img/" . $type . '/' . $id . '/';
    $db_path = "/shared_img/" . $type . '/' . $id . '/';
    if (!file_exists($relative_path)) {
        mkdir($relative_path, 0777, true);
    }

    if ($path_return == "dir") {
        $img_path = $relative_path;//./shared_img/~/
    } elseif ($path_return == "dir_file") {
        $img_path = $relative_path . $file_name . '.' . $ext;
    } elseif ($path_return == "file") {
        if ($thumbnail != '') {
            $img_path = $full_path . $thumbnail . '/' . $file_name . '.' . $ext;//http://club-port.com/shared_img/~/file
        } else {
            $img_path = $full_path . $file_name . '.' . $ext;
        }
    } elseif ($path_return == "url") {
        $img_path = $full_path;
    } else {
        if (file_exists($relative_path . $file_name . '.' . $ext)) {
            if ($thumbnail != '') {
                $img_path = $relative_path . $thumbnail . '/' . $file_name . '.' . $ext;//./shared_img/~/file
            } else {
                $img_path = $db_path . $file_name . '.' . $ext;//./shared_img/~/file
            }
        } else {


        }
    }
    return $img_path;
}

/**
 * jquery upload用 //201803追加
 * アップロード時、削除時のみ使用
 * @param $id
 * @param $type //catalog等
 * @param string $file_name
 * @param string $path_return
 * @return string
 */
function upload_path($id, $type, $file_name = "", $path_return = "")
{
    $base_path = "./shared_img/u/" . $type . "/" . $id;
    $path = "/u/" . $type . "/" . $id;

    if (!file_exists($base_path)) {
        mkdir($base_path, 0777, true);
    }
    if ($path_return == "dir") {
        $catalog_path = $base_path;
    } elseif ($path_return == "file") {
        $catalog_path = base_url('shared_img') . $path .'/'. $file_name;
    } else {
        $catalog_path = base_url('shared_img') . $path;
    }
    return $catalog_path;
}
