<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author    CodeIgniter community
 * @copyright    Copyright (c) 2014 - 2016; British Columbia Institute of Technology (http://bcit.ca/)
 * @license    http://opensource.org/licenses/MIT	MIT License
 * @link    http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["attachment_file"] = "Click here to download attached file";
$lang["system_name"] = "Environmental Science Center Q&A system; the University of Tokyo";
$lang["question_form"] = "Question form";
$lang["question_form_edit"] = "Question edit form";
$lang["question_form_confirm"] = "Question form confirm";
$lang["question_form_confirm_edit"] = "Question edit form confirm";
$lang["compulsory"] = "Compulsory field";
$lang["require"] = "※Required";
$lang["for_confirmation"] = "For confirmation";
$lang["does_not_match"] = "It does not match for confirmation";
$lang["outside_campus"] = "Outside Campus";
$lang["within_campus"] = "Within Campus";
$lang["name"] = "Name";
$lang["q_id"] = "Question ID";
$lang["q_date"] = "Question date";
$lang["sorting_date"] = "Sorting date";
$lang["affiliation"] = "Affiliation";
$lang["affiliation_category"] = "Affiliation category";
$lang["educational_institute"] = "Educational institute";
$lang["research_institute"] = "Research institute";
$lang["private_company"] = "Private company";
$lang["government\/public_office"] = "Government/Public office";
$lang["local_resident"] = "Local resident";
$lang["other"] = "Other";
$lang["faculty_school"] = "Faculty・School";
$lang["department"] = "Department";
$lang["laboratory"] = "Laboratory";
$lang["question_category"] = "Question category";
$lang["laboratory_waste"] = "Laboratory waste";
$lang["daily-living_waste"] = "Daily-living waste";
$lang["unkown_waste"] = "Unkown waste";
$lang["environmental_safety_education"] = "Environmental safety education (Course; Tour; Certification; Exercise; Attendance management system; etc.)";
$lang["chemical_management"] = "Chemical management";
$lang["prtr_registration"] = "PRTR registration";
$lang["environment_and_safety_guideline"] = "Environment and Safety Guideline";
$lang["esc_quarterly_magazine"] = "ESC quarterly magazine";
$lang["phone"] = "Phone(ext.)";
$lang["question_title"] = "Question title";
$lang["question"] = "Question";
$lang["attachment_file"] = "Attachment file";
$lang["nonselect_file"] = "File not selected";
$lang["prev"] = "Prev";
$lang["submit_from"] = "Submit";
$lang["confirm_submit"] = "Confirm & Submit";
$lang["confirm_info"] = "If you 've confirmed; please submit";
$lang["exit_from"] = "Exit";
$lang["complete_msg1"] = "Your question has been submitted .";
$lang["complete_msg2"] ="<p> A confirmation email will be sent to the entered address . </p><p> Notification email will be sent once an answer is made .</p><p> Please wait a while.</p>";
$lang["mail_info"] = "Your question has been submitted .<BR > A confirmation email will be sent to the entered address . <BR > Notification email will be sent once an answer is made .<BR > Please wait a while.";
$lang["please_select"] = "Please select";
$lang["fill_item"] = "Please fill in;completely";
$lang["unjust_letter"] = "Unjust letter";
$lang["receive_datetime"] = "Receive Date / time";
$lang["questioner_name"] = "Name";
$lang["answer"] = "Answer";
$lang["confirm"] = "Confirm";
$lang["sent_questioner_mes1"] = "Your answer has been submitted . ";
$lang["sent_questioner_mes2"] = "A confirmation email will be sent to the questioner . ";
$lang["date_time"] = "Date & time";
$lang["respondent"] = "Respondent";
$lang["additional_question"] = "Additional question";
$lang["confirmed"] = "Confirmed";
$lang["re-question_form"] = "Re-Question form";
$lang["re-question_title"] = "Re-Question title";
$lang["re-question"] = "Re - Question";
$lang["re-attachment_file"] = "Re-Attachment file";
$lang["question_notification"] = "【環境安全研究センター】 質問受付 Environmental Science Center Question ID => 12345";
$lang["submitted_below"] = "Your question has been submitted as below";
$lang["question_id"] = "Question ID";
$lang["not_eply_email_info"] = "This email has been sent from a send - only address . Do not reply to this email address . ";
$lang["inquery_to_esc"] = "Inquery to ESC( other than Q & A)";
$lang["answer_notification"] = "Answer notification for Question ID => 12345";
$lang["made_below"] = "An answer has been made as below . ";
$lang["click_reply"] = "Click here to make a reply";
$lang["return"] = "Top page";
$lang["return2"] = "Prev";
$lang["attention"] = "※ If you click 'Back', you need to select the attached file again";
$lang["answer_body"] = "Answer";
$lang["answer_confirm"] = "Answer Confirm";
$lang["answer_date"] = "Date & time";
$lang["answer_res"] = "Respondent";
$lang["img_delete"] = "delete";