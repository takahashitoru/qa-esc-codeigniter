<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author    CodeIgniter community
 * @copyright    Copyright (c) 2014 - 2016; British Columbia Institute of Technology (http://bcit.ca/)
 * @license    http://opensource.org/licenses/MIT	MIT License
 * @link    http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["attachment_file"] = "こちらをクリックで添付ファイルダウンロード";
$lang["system_name"] = "東京大学環境安全研究センターQ&Aシステム";
$lang["question_form"] = "質問登録フォーム";
$lang["question_form_edit"] = "質問編集フォーム";
$lang["question_form_confirm"] = "質問登録フォーム（確認）";
$lang["question_form_confirm_edit"] = "質問編集フォーム（確認）";
$lang["compulsory"] = "必須項目(※は入力必須です)";
$lang["require"] = "※必須";
$lang["for_confirmation"] = "確認用";
$lang["does_not_match"] = "確認用と一致しません";
$lang["outside_campus"] = "学外の方";
$lang["within_campus"] = "学内の方";
$lang["name"] = "お名前";
$lang["q_id"] = "質問ID";
$lang["q_date"] = "質問日時";
$lang["sorting_date"] = "振分日時";
$lang["affiliation"] = "ご所属";
$lang["affiliation_category"] = "ご所属分類";
$lang["educational_institute"] = "教育機関";
$lang["research_institute"] = "研究機関";
$lang["private_company"] = "民間企業";
$lang["government\/public_office"] = "官公庁";
$lang["local_resident"] = "地域住民";
$lang["other"] = "その他";
$lang["faculty_school"] = "部局";
$lang["department"] = "専攻";
$lang["laboratory"] = "研究室";
$lang["question_category"] = "質問カテゴリ";
$lang["laboratory_waste"] = "実験系廃棄物";
$lang["daily-living_waste"] = "生活系廃棄物";
$lang["unkown_waste"] = "内容不明廃棄物";
$lang["environmental_safety_education"] = "環境安全教育（講習会・見学会・修了証・実習・受講管理システム等） ";
$lang["chemical_management"] = "化学物質管理";
$lang["prtr_registration"] = "PRTR登録";
$lang["environment_and_safety_guideline"] = "環境安全指針";
$lang["esc_quarterly_magazine"] = "季刊誌｢環境安全」";
$lang["phone"] = "電話番号(内線)";
$lang["question_title"] = "質問タイトル";
$lang["question"] = "質問内容";
$lang["attachment_file"] = "添付ファイル";
$lang["nonselect_file"] = "ファイル未選択";
$lang["prev"] = "戻る";
$lang["submit_from"] = "登録";
$lang["confirm_submit"] = "確認登録";
$lang["confirm_info"] = "内容がよろしければ確認登録をお願いします";
$lang["exit_from"] = "終了";
$lang["complete_msg1"] = "ご質問を受け付けました。";
$lang["complete_msg2"] = "<p>確認のメールが登録されたメールアドレスに送信されます。</p><p>回答がされますと、メールで通知されます。</p><p>いましばらくお待ちください。</p>";
$lang["mail_info"] = "ご質問を受け付けました。<BR>確認のメールが登録されたメールアドレスに送信されます。<BR>回答がされますと、メールで通知されます。<BR>いましばらくお待ちください。";
$lang["please_select"] = "選択してください";
$lang["fill_item"] = "未入力・未選択があります";
$lang["unjust_letter"] = "不正な入力です";
$lang["receive_datetime"] = "受信日時";
$lang["questioner_name"] = "質問者";
$lang["answer"] = "回答表示";
$lang["confirm"] = "確認";
$lang["sent_questioner_mes1"] = "回答を登録しました。";
$lang["sent_questioner_mes2"] = "確認のメールが質問者に送信されます。";
$lang["date_time"] = "受付日時";
$lang["respondent"] = "回答者";
$lang["additional_question"] = "再質問";
$lang["confirmed"] = "閲覧済";
$lang["re-question_form"] = "再質問登録フォーム";
$lang["re-question_title"] = "再質問タイトル";
$lang["re-question"] = "再質問内容";
$lang["re-attachment_file"] = "再添付ファイル";
$lang["question_notification"] = "【環境安全研究センター】 質問受付 Environmental Science Center Question ID => 12345";
$lang["submitted_below"] = "以下のとおりご質問を受け付けました。";
$lang["question_id"] = "ID";
$lang["not_eply_email_info"] = "このメールは送信専用メールアドレスから配信されています。ご返信いただいてもお答えできませんのでご了承ください。";
$lang["inquery_to_esc"] = "当センターへのお問い合わせ（質問・回答以外）";
$lang["answer_notification"] = "回答通知　ID：12345";
$lang["made_below"] = "以下のとおり回答されました。";
$lang["click_reply"] = "回答の登録はこちらから";
$lang["return"] = "Topへ戻る";
$lang["return2"] = "戻る";
$lang["attention"] = "※「戻る」をクリックした場合には、添付ファイルを再度選択する必要があります";
$lang["answer_body"] = "回答";
$lang["answer_confirm"] = "回答登録確認";
$lang["answer_date"] = "回答日時";
$lang["answer_res"] = "回答者";
$lang["img_delete"] = "削除";
$lang["send"] = "送信";