<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('answer_model');
        $this->load->model('question_model');
        $this->load->model('activity_model');
        $this->load->model('faq_model');
    }

    /**
     * FAQ
     */
    public function index()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        if ($this->input->get('category')) {
            $category = $this->input->get('category');
        } else {
            $category = '';
        }
        $dat['category_dat'] = $this->master_model->get_category('ja');

        $dat['result_dat'] = $this->faq_model->get_data($page, 9999, 'list', '', $category);
        $total_count = $this->faq_model->get_data('', '', 'count', '', $category);
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'FAQ';

        $this->disp('faq', $dat);
    }

    /**
     * データ移行
     */
    public function set()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        if ($this->input->get('category')) {
            $category = $this->input->get('category');
        } else {
            $category = '';
        }
        $set_dat = $this->question_model->get_data($page, 9999, 'list', '', $category);
        pre_print_r($set_dat);
        exit;
        unset($_SESSION['admin_dat']);
        unset($_SESSION['answer_dat']);
        redirect('');
    }
}
