<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Center extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('center_model');
        $this->load->model('auth_model');
    }

    /**
     * センタ用初期画面
     */
    public function index()
    {
        if(isset($_SESSION['admin_dat'])){

        }
        if(isset($_SESSION['answer_dat'])){
            redirect('answer');
        }
        redirect('center/login?role=answer');
        //$dat['page_type'] = 'common';
        //$dat['page_title'] = 'センタ用初期画面';
        //$this->disp('center/index', $dat);
    }

    /**
     * 認証画面
     */
    public function login()
    {
        $dat['page_type'] = 'common';
        $dat['page_title'] = '認証画面';
        $this->disp('center/login', $dat);
    }

    /**
     * 認証処理
     */
    public function login_process()
    {
        $dat = $this->input->post();
        if (empty($dat['role'])) {
            redirect('center');
        }

        /*validation*/
        $er_flg = false;
        if ($dat['user_code'] == '') {
            $er_flg = true;

        }
        if ($dat['password'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_required', true);
        }

        if ($er_flg == true) {
            redirect('center/login?role=' . $dat['role']);
        }

        $result = $this->auth_model->login($dat);
        if ($result == true) {
            if ($this->input->post('remember') == 1) {
                setcookie('user_code', $this->input->post('user_code'), time() + 60 * 60 * 24 * 30);
            }
            if ($dat['role'] == 'admin') {
                redirect('admin');
            } else {
                redirect('answer');
            }
        } else {
            $this->session->set_flashdata('er_msg_wrong', true);
            redirect('center/login?role=' . $dat['role']);
        }
    }

    /**
     *  logout処理
     */
    public function logout()
    {
        unset($_SESSION['admin_dat']);
        unset($_SESSION['answer_dat']);
        redirect('center');
    }

    /**
     *  パスワードリマインダー処理
     */
    public function reminder()
    {
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }
        if (empty($_GET['role'])) {
            redirect();
        }
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'パスワードリマインダー';
        $this->disp('center/reminder', $dat);
    }

    /**
     *  パスワードリマインダー処理
     */
    public function reminder_process()
    {
        $dat = $this->input->post();
        $_SESSION['form_dat'] = $dat;
        /*validation*/
        $er_flg = false;
        if ($dat['user_code'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_user_code', true);
        }
        if ($dat['email'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_email', true);
        }

        if ($er_flg == true) {
            redirect('center/reminder?role='.$dat['role']);
        }

        $result = $this->auth_model->remind_chk($dat);
        //pre_print_r($result);
        //exit;
        if ($result == true) {
            unset($_SESSION['form_dat']);
            /*todo:mail送信*/
            $title = MAIL_TITLE_REMINDER;
            $template = 'reminder';
            $hash = token_encode(ANGOU_KEY, $result['id']);
            $url = base_url('center/pwd_resetting?hash=' . $hash.'&role='.$dat['role']);
            $this->send_mail($title, $template, $result, $result['email'], array(), '', $url);
            redirect('center/reminder?role='.$dat['role']);
        } else {
            $this->session->set_flashdata('er_msg_wrong', true);
            redirect('center/reminder?role='.$dat['role']);
        }
    }

    /**
     *  パスワード再設定画面
     */
    public function pwd_resetting()
    {
        $hash = $this->input->get('hash');
        $role = $this->input->get('role');
        if ($hash == '') {
            redirect('center/reminder?role='.$role);
        }
        if ($hash == '') {
            redirect('center/reminder?role='.$role);
        }
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }
        if($role =='admin'){
            $name ='管理者';
        }else{
            $name ='回答者';
        }
        $dat['role'] = $role;
        $dat['hash'] = $hash;
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'パスワード再設定画面 ('.$name.')';
        $this->disp('center/pwd_resetting', $dat);
    }

    /**
     *  パスワード再設定処理
     */
    public function pwd_resetting_process()
    {
        $hash = token_decode(ANGOU_KEY, $this->input->post('hash'));
        $dat = $this->input->post();
        $dat['hash'] = $hash;
        $_SESSION['form_dat'] = $dat;

        /*validation*/
        $er_flg = false;
        if ($dat['user_code'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_user_code', true);
        }
        if ($dat['password'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_password', true);
        }
        if ($dat['password2'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_password_confirm', true);
        }
        if ($dat['password'] != $dat['password2']) {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_password_confirm', true);
        }
        if ($er_flg == true) {
            redirect('center/reminder?role='.$dat['role']);
        }

        $result = $this->auth_model->pwd_resetting($dat);
        //pre_print_r($result);
        //exit;
        if ($result == true) {
            unset($_SESSION['form_dat']);
            redirect('center/');
        } else {
            $this->session->set_flashdata('er_msg_wrong', true);
            redirect('center/');
        }
    }
}
