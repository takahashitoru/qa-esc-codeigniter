<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Top extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 質問者用初期画面
     * 言語選択
     */
    public function index()
    {
        //$dat['page_type'] = 'common';
        //$dat['page_title'] = '質問者用初期画面';
        //$this->disp('questioner/select_lang',$dat);
        unset($_SESSION['form_dat']);
        redirect('question');
    }

    /**
     * ログアウト
     */
    public function logout()
    {
        unset($_SESSION['admin_dat']);
        unset($_SESSION['answer_dat']);
        redirect('');
    }
}
