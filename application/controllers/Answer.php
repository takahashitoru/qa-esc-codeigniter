<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Answer extends AnswerController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('answer_model');
        $this->load->model('question_model');
    }

    /**
     * 回答者用質問リスト
     */
    public function index()
    {
        unset($_SESSION['tmp_key']);
        if (!isset($_SESSION['answer_dat']['id'])) {
            redirect('center/login?role=answer');
        }
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $id = $_SESSION['answer_dat']['id'];
        $dat['my_dat'] = $this->answer_model->get_detail($id);

        if ($this->input->get('search') == 'all') {
            $dat['result_dat'] = $this->question_model->get_mydata($page, 20, 'list', $id);
            $total_count = $this->question_model->get_mydata('', '', 'count', $id);
        } elseif ($this->input->get('search') == 'unanswered') {
            $dat['result_dat'] = $this->question_model->get_mydata($page, 20, 'list', $id, 'unanswered');
            $total_count = $this->question_model->get_mydata('', '', 'count', $id, 'unanswered');
        } else {
            $dat['result_dat'] = $this->question_model->get_mydata($page, 20, 'list', $id);
            $total_count = $this->question_model->get_mydata('', '', 'count', $id);
        }
        $dat['answer_dat'] = $this->answer_model->get_data('', '', 'all');//回答者選択用全件取得
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = '回答者用質問リスト画面';
        $this->disp('answer/q_list', $dat);
    }

    /**
     * answer_model
     */
    public function entry()
    {
        if (!$this->input->get('q_id')) {
            redirect('answer');
        }

        $q_id = $this->input->get('q_id');

        $q_id = base64_decode($q_id);
        //新規登録時は仮IDセット
        if (!isset($_SESSION['tmp_key'])) {
            $this->session->set_userdata('tmp_key', date('YmdHis'));
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        } else {
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        }

        $dat['form_dat'] = $this->question_model->get_detail($q_id);
        if ($dat['form_dat'] == false) {
            redirect('answer');
        }
        /*form初期値*/
        if (isset($_SESSION['form_dat']['answer_body'])) {
            $dat['form_dat']['answer_body'] = $_SESSION['form_dat']['answer_body'];
        }
        /* 多言語ファイル */
        $lang = 'ja';//日本語固定
        $this->lang->load('site', $lang);

        if (isset($_SESSION['re_form_dat'])) {
            $dat['re_form_dat'] = $_SESSION['re_form_dat'];
        }
        $id = $_SESSION['answer_dat']['id'];
        $dat['affiliation'] = $this->master_model->get_affiliation('ja');//日本語固定
        $dat['my_dat'] = $this->answer_model->get_detail($id);
        $dat['faculty'] = $this->master_model->get_faculty('ja');//日本語固定
        $dat['category'] = $this->master_model->get_category('ja');//日本語固定
        $dat['answer_dat'] = $this->answer_model->get_data('', '', 'all');//回答者選択用全件取得
        $dat['page_type'] = 'common';
        $dat['page_title'] = '回答者登録用画面';
        $this->disp('answer/entry', $dat);
    }

    /**
     * 確認画面
     */
    public function confirm()
    {
        $lang = 'ja';
        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $q_id = $this->input->post('q_id');
        $q_id = base64_decode($q_id);
        if ($q_id == '') {
            redirect('answer');
        }
        if (!empty($_POST['prev'])) {
            redirect('answer');
        }
        //新規登録時は仮IDセット
        if (!isset($_SESSION['tmp_key'])) {
            $this->session->set_userdata('tmp_key', date('YmdHis'));
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        } else {
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        }

        /*form初期値*/
        $qdat = $this->input->post();
        $qdat['q_id'] = $q_id;
        $_SESSION['re_form_dat'] = $qdat;
        $dat['re_form_dat'] = $_SESSION['re_form_dat'];
        $dat['form_dat'] = $this->question_model->get_detail($q_id);

        /*validation*/
        $er_flg = false;
        if ($dat['re_form_dat']['answer_body'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_answer_body', true);
        }

        //メール送信者cc追加
        if (!empty($dat['re_form_dat']['add_mail_free'])) {
            $add_mail_free_str = rtrim($dat['re_form_dat']['add_mail_free'], ",");
            $add_mail_free = explode(',', $add_mail_free_str);
            if (!empty($dat['re_form_dat']['add_mail'])) {
                foreach ($add_mail_free as $amf => $val) {
                    foreach ($dat['re_form_dat']['add_mail'] as $add_mail) {
                        if ($val == $add_mail) {
                            unset($add_mail_free[$amf]);
                        }
                    }
                }
            }
            $add_mail_free = array_unique($add_mail_free);
            $add_mail_free_str = implode(",", $add_mail_free);
            $_SESSION['re_form_dat']['add_mail_free'] = $add_mail_free_str;
            $dat['re_form_dat']['add_mail_free'] = $_SESSION['re_form_dat']['add_mail_free'];
            $mail_er_flg = false;
            foreach ($add_mail_free as $v) {
                if (filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    //
                } else {
                    $mail_er_flg = true;
                }
            }
            if ($mail_er_flg == true) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_add_mail_free', true);
            }
        }

        /*validation error*/
        if ($er_flg == true) {
            redirect('answer/entry?q_id=' . base64_encode($q_id));
        }


        if (!empty($_FILES['userfile']['name'])) {
            $this->file_model->multi_upload($_FILES, 'answer', $_SESSION['tmp_key']);
        }

        $id = $_SESSION['answer_dat']['id'];
        $dat['affiliation'] = $this->master_model->get_affiliation('ja');//日本語固定
        $dat['my_dat'] = $this->answer_model->get_detail($id);
        $dat['faculty'] = $this->master_model->get_faculty('ja');//日本語固定
        $dat['category'] = $this->master_model->get_category('ja');//日本語固定
        $dat['answer_dat'] = $this->answer_model->get_data('', '', 'all');//回答者選択用全件取得
        $dat['page_type'] = 'common';
        $dat['page_title'] = '回答登録確認画面';
        $this->disp('answer/confirm', $dat);
    }

    /**
     * 回答登録処理
     */
    public function process()
    {
        $q_id = $this->input->post('q_id');
        $q_id = base64_encode($q_id);
        if ($q_id == '') {
            redirect('answer');
        }
        if (!empty($_POST['prev'])) {
            redirect('answer/entry?q_id=' . $q_id);
        }

        /*form初期値*/
        $dat['form_dat'] = $_SESSION['re_form_dat'];
        $id = $_SESSION['answer_dat']['id'];
        $dat['my_dat'] = $this->answer_model->get_detail($id);
        $send_lang = $this->question_model->get_lang($dat['form_dat']['q_id']);

        //pre_print_r($dat['form_dat']['q_id']);
        $result = $this->answer_model->set_answer($dat, $id);

        if ($result == true) {
            $send_dat = $this->answer_model->get_answer($result);
            $this->file_model->renum($send_dat['id'], $_SESSION['tmp_key'], 'answer');
            $q_dat = $this->question_model->get_detail($send_dat['question_id']);

            if ($q_dat['manage_id'] == 0) {
                $q_dat['id'] = $q_dat['id'] . '-' . $q_dat['manage_id'];
            } else {
                $q_dat['id'] = $q_dat['re_question_id'] . '-' . $q_dat['manage_id'];
            }

            /*todo:mail送信 to質問者*/

            /*
            if ($q_dat['lang_cd'] == 'ja') {
                $template = 'answer';
            } elseif ($send_dat['lang_cd'] == 'en') {
                $template = 'answer_e';//todo:英語版テンプレート
            } else {
                $template = 'answer';
            }
            */

            /*
            if ($send_lang == 'ja') {
                $template_q = 'answer';
                $template_a = 'answer';
                $title = MAIL_TITLE_A;
            } elseif ($send_lang == 'en') {
                //$template_q = 'answer_e';//todo:英語版テンプレート
                $template_q = 'answer_e';
                $template_a = 'answer';
                $title = MAIL_TITLE_A_EN;
            } else {
                $template_q = 'answer';
                $template_a = 'answer';
                $title = MAIL_TITLE_A;
            }
            */
            //cc追加ありの時
            $encrypted_id = base64_encode($send_dat['question_id']);
            $url_a = base_url('question/answer?lang=' . $send_lang . '&id=' . $encrypted_id);//todo:URL生成 to管理者
            $add_mail_free_array = array();
            if (isset($_SESSION['re_form_dat']['add_mail_free'])) {
                $add_mail_free_str = rtrim($_SESSION['re_form_dat']['add_mail_free'], ",");
                $add_mail_free_array = explode(',', $add_mail_free_str);
            }
            if (isset($_SESSION['re_form_dat']['add_mail'])) {
                foreach ($_SESSION['re_form_dat']['add_mail'] as $add_mail) {
                    array_push($add_mail_free_array, $add_mail);
                }
            }
            $sender_dat = $this->answer_model->get_detail($send_dat['answer_id']);
            $add_mail_free_array = array_merge($add_mail_free_array, array($sender_dat['email']));
            //pre_print_r($add_mail_free_array);exit;

            /*20210709追加*/
            $q_dat['faculty'] = '';
            $faculty_data = false;
            if (!empty($q_dat['faculty_code'])) {
                $faculty_data = $this->master_model->get_faculty_data($q_dat['faculty_code']);
            }
            if ($faculty_data != false) {
                $q_dat['faculty'] = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
            }
            $q_dat['affiliation_category'] = '';
            $affiliation_category_data = $this->master_model->get_affiliation_category_data($q_dat['affiliation_category']);
            if ($affiliation_category_data != false) {
                $q_dat['affiliation_category'] = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
            }
            if ($q_dat["question_category_id"] == '') {
                $q_dat['category'] = '';
            } else {
                $q_dat['category'] = $this->master_model->get_category_detail('ja', $q_dat["question_category_id"]);
            }


            $q_dat['answer_body'] = $send_dat['answer_body'];
            $template_q = 'answer';
            $template_a = 'answer_to_admin';
            $title = '【東大ESC/ESC UTokyo】回答がありました/Question answered ID：' . $q_dat['id'];

            $url_q = base_url('question/answer?lang=' . $send_lang . '&id=' . $encrypted_id);//todo:URL生成
            //$callback = 'question/complete';

            //$this->send_mail($title, $template_q, $q_dat, $q_dat['email'], array(), '', $url_q);

            /*todo:CC送信者*/
            //pre_print_r($add_mail_free_array);
            //exit;
            $to_mail_cc = array();
            if ($add_mail_free_array != array()) {
                foreach ($add_mail_free_array as $amfa) {
                    $to_mail_cc[]=$amfa;
                    if ($amfa != '') {
                        //$this->send_mail($title, 'answer_to_bcc', $q_dat, $amfa, array(), '', $url_q);
                    }
                }
            }
            $this->send_mail($title, $template_q, $q_dat, $q_dat['email'], array(), '', $url_q);

            /*todo:mail送信to管理者*/
            //$title = MAIL_TITLE_A_ADMIN;
            //$template = 'answer_to_admin';
            //$callback = 'question/complete';
            $this->send_mail($title, $template_a, $q_dat, REPLY_MAIL, $to_mail_cc, '', $url_a);
            unset($_SESSION['form_dat']);
            unset($_SESSION['re_form_dat']);
            redirect('answer/complete');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * 回答登録完了画面
     */
    public function complete()
    {
        if (isset($_SESSION['lang'])) {
            if ($_SESSION['lang'] == '') {
                $lang = 'ja';
            } else {
                //$lang = $this->session->userdata('lang');
                $lang = 'ja';//日本語固定
            }
        } else {
            $lang = 'ja';
        }

        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $dat['page_type'] = 'common';
        $dat['page_title'] = '回答登録完了画面';

        /*form session削除*/
        unset($_SESSION['form_dat']);
        $this->disp('answer/complete', $dat);
    }

    /**
     * 回答者変更
     */
    public function answer_change()
    {
        //回答者未選択
        if (empty($_POST['answer_id'])) {
            redirect('answer');
        }

        $dat = $this->input->post();
        $q_id = $dat['id'];//質問
        $a_id = $dat['answer_id'];//回答者

        $q_dat = $this->question_model->get_detail($q_id);
        $manage_id = $q_dat['re_question_id'] . '-' . $q_dat['manage_id'];

        //pre_print_r($q_dat);exit;
        $result = $this->question_model->set_rel_answer($dat);


        /*20210709追加*/
        $q_dat['faculty'] = '';
        $faculty_data = false;
        if (!empty($q_dat['faculty_code'])) {
            $faculty_data = $this->master_model->get_faculty_data($q_dat['faculty_code']);
        }
        if ($faculty_data != false) {
            $q_dat['faculty'] = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
        }
        $q_dat['affiliation_category'] = '';
        $affiliation_category_data = $this->master_model->get_affiliation_category_data($q_dat['affiliation_category']);
        if ($affiliation_category_data != false) {
            $q_dat['affiliation_category'] = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
        }
        if ($q_dat["question_category_id"] == '') {
            $q_dat['category'] = '';
        } else {
            $q_dat['category'] = $this->master_model->get_category_detail('ja', $q_dat["question_category_id"]);
        }
//pre_print_r($q_dat);exit;

        $send_dat = $this->answer_model->get_detail($a_id);
        $send_dat['id'] = $manage_id;
        $q_dat['id'] = $manage_id;
        $q_dat['answer_name'] = $q_dat['name'];
        $q_dat['name'] = $send_dat['name'];
        $url = base_url('center/');//todo:URL生成 to回答者 該当の質問へのURL
        /*todo:mail送信 to回答者*/
        $title = MAIL_TITLE_A_CNG . $manage_id . '【振分不要】';
        $template = 'a_change';
        $this->send_mail($title, $template, $q_dat, $send_dat['email'], array(), '', $url);

        //管理者向け
        $template = 'a_change_admin';
        //$this->send_mail($title, $template, $send_dat, ADMIN_MAIL, array(), '', $url);

        redirect('answer');
    }

    /**
     * 質問/回答内容確認
     */
    public function display()
    {
        $id = $this->input->get('q_id');
        $q_id = base64_decode($id);

        if ($q_id != '') {
            $dat['result_dat'] = $this->question_model->get_detail($q_id, 0);//回答済みは除く
            $dat['answer_dat'] = $this->answer_model->get_qa($q_id);

            /*validation*/
            if ($dat['result_dat'] == false) {
                show_404();
            }


            //$lang = $this->input->get('lang');
            $lang = 'en';
            /* 多言語ファイル */
            $this->lang->load('site', $lang);

            $dat['page_type'] = 'confirm';
            $dat['page_title'] = '質問回答表示画面';

            /*form session削除*/
            unset($_SESSION['form_dat']);
            $this->disp('answer/display', $dat);
        } else {
            //404
            show_404();
        }
    }

    /**
     * ログアウト
     */
    public function logout()
    {
        unset($_SESSION['answer_dat']);
        redirect('center');
    }
}