<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('answer_model');
        $this->load->model('question_model');
        $this->load->model('activity_model');
        $this->load->model('faq_model');
        $this->load->model('bcc_model');
    }

    /**
     * 管理者用初期画面
     */
    public function index()
    {
        if (isset($_SESSION['admin_dat'])) {
            $dat['page_type'] = 'common';
            $dat['page_title'] = '管理者用初期画面';
            $this->disp('admin/index', $dat);
        } elseif (isset($_SESSION['answer_dat'])) {

        }
    }

    /**
     * 各ページ割り振り
     */
    public function page()
    {
        $page = $this->input->post('type');
        if ($page == 'list') {
            redirect('admin/q_list');
        } else if ($page == 'user_manage') {
            redirect('admin/user_manage');
        } else if ($page == 'bcc_manage') {
            redirect('admin/bcc_manage');
        } else if ($page == 'list_maintenance') {
            redirect('admin/list_maintenance');
        } else if ($page == 'log_manage') {
            redirect('admin/log_manage');
        } else if ($page == 'log_output') {
            redirect('admin/output_log');
        } else if ($page == 'faq_edit') {
            redirect('admin/faq_edit');
        } else if ($page == 'faq_maintenance') {
            redirect('admin/faq_maintenance');
        }
    }

    /**
     * ログ出力
     */
    public function output()
    {
        $array = explode("-", $_POST['range']);

        if (count($array) <= 1) {
            //$this->session->set_flashdata('er_msg', '取得期間が選択されていません');
            //redirect('admin/output_log');
            $dat["s_date"] = $_POST['range'];
            $dat["e_date"] = "";
        } else {
            $dat["s_date"] = date('Y/m/d', strtotime($array[0]));
            $dat["e_date"] = date('Y/m/d', strtotime($array[1]));
        }
        //pre_print_r($_POST);exit;
        $result_dat = $this->question_model->get_data(1, 9999, 'list', $dat);
        //pre_print_r($dat["s_date"]);;
        //pre_print_r($dat["e_date"]);;
        //pre_print_r($result_dat);exit;
        /*#################################################
            # エクセルで開いた際に文字化けしないようにエンコードしてcsvを生成
            #################################################*/
        // ファイル名
        $file_path = "./uploads/tmp/csv" . date('Ymd') . ".csv";
        // ------------------------------
        // csv生成
        // ------------------------------

        // CSVに出力するタイトル行
        //$title_s = array("質問ID", "カテゴリ", "質問タイトル", "質問内容", "回答", "質問日時");
        //質問日時,質問者,カテゴリ,タイトル,質問内容,振分日時,ステータス,回答者,回答者変更回数,回答内容,回答日時の順で表示
        //$title_s = array("ID","質問日時","質問者", "カテゴリ", "タイトル", "質問内容","振分日時","ステータス","回答者", "回答者変更回数","回答内容", "回答日時");//test
        //$title_s = array("質問ID", "質問日時", "質問者", "カテゴリ", "タイトル", "質問内容", "振分日時", "ステータス", "回答者", "回答者変更回数", "回答内容", "回答日時");
        $title_s = array("質問ID", "質問日時", "質問者", "E-mail", "電話番号", "部局(学内)", "専攻(学内)", "研究室(学内)", "所属分類(学外)", "カテゴリ", "タイトル", "質問内容", "振分日時", "ステータス", "回答者", "回答者変更回数", "回答内容", "回答日時");

        // CSVに出力する内容
        $i = 1;
        if($result_dat) {
            foreach ($result_dat as $v) {
                $category_name = '';
                $rel_answer = '';
                $id = '';
                $category_name = $this->master_model->get_category_detail("", $v['question_category_id']);
                $rel_answer = $this->question_model->get_rel_answer($v['id']);
                if ($v['re_question_id'] == '') {
                    $id = $v['id'] . '-0';
                } else {
                    $id = $v['re_question_id'] . '-' . $v['manage_id'];
                }

                $answer = $this->answer_model->get_answer_detail($v['id']);
                $answer_change_count = $this->question_model->get_answer_count($v['id']);
                if ($answer != array()) {
                    $date = $answer[0]['updated_at'];//最終回答日
                    $answer_count = count($answer);//回答数合計
                } else {
                    $date = '未回答';
                    $answer_count = 0;
                }
                /*
                $body_s[$i]['A'] = $id;//質問ID
                $body_s[$i]['B'] = $category_name;//カテゴリ
                $body_s[$i]['C'] = $v['question_title'];//質問内容
                $body_s[$i]['D'] = $v['question_body'];//質問内容
                $body_s[$i]['E'] = $v['answer_body'];//回答
                $body_s[$i]['F'] = $v['created_at'];//質問日時
                */
                //pre_print_r($v['question_category_id']);
                //pre_print_r($category_name);exit;
                if (!empty($answer[0]['answer_body'])) {
                    if ($answer[0]['answer_body'] == '') {
                        $status = '未';
                    } else {
                        $status = '完了';
                    }
                } else {
                    $status = '完了';
                }
                $faculty_data = $this->master_model->get_faculty_data($v['faculty_code']);
                if ($faculty_data == false) {
                    $faculty = false;
                } else {
                    $faculty = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
                }
                $affiliation_category_data = $this->master_model->get_affiliation_category_data($v['affiliation_category']);
                if ($affiliation_category_data == false) {
                    $affiliation_category = false;
                } else {
                    $affiliation_category = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
                }

                $body_s[$i]['id'] = '"' . $id . '"';//質問ID
                $body_s[$i]['A'] = $v['created_at'];//質問日時
                $body_s[$i]['B'] = $v['name'];//質問者
                $body_s[$i]['B1'] = $v['email'];//E-mail
                $body_s[$i]['B2'] = $v['tel'];//電話番号
                $body_s[$i]['B3'] = $faculty;//専攻(学内)
                $body_s[$i]['B4'] = $v['department'];//部局(学内)
                $body_s[$i]['B5'] = $v['laboratory'];//研究室(学内)
                $body_s[$i]['B6'] = $affiliation_category;//所属分類(学外)

                $body_s[$i]['C'] = $category_name;//カテゴリ
                $body_s[$i]['D'] = $v['question_title'];//タイトル
                $body_s[$i]['E'] = $v['question_body'];//質問内容
                $body_s[$i]['F'] = $v['updated_at'];//振分日時
                $body_s[$i]['G'] = $status;//ステータス
                if (!empty($rel_answer['name'])) {
                    $answer_name = $rel_answer['name'];
                } else {
                    $answer_name = '';
                }
                if (!empty($answer[0]['answer_body'])) {
                    $answer_body = $answer[0]['answer_body'];
                } else {
                    $answer_body = '';
                }
                $body_s[$i]['H'] = $answer_name;//回答者
                $body_s[$i]['I'] = $answer_change_count;//回答者変更回数
                $body_s[$i]['J'] = $answer_body;//回答内容
                $body_s[$i]['K'] = $date;//回答日時
                ++$i;
            }
        }else{
            $i = 1;
            $body_s[$i]['id'] = '';//質問ID
            $body_s[$i]['A'] = '';//質問日時
            $body_s[$i]['B'] = '';;//質問者
            $body_s[$i]['B1'] = '';;//E-mail
            $body_s[$i]['B2'] = '';;//電話番号
            $body_s[$i]['B3'] = '';;//専攻(学内)
            $body_s[$i]['B4'] = '';;//部局(学内)
            $body_s[$i]['B5'] = '';;//研究室(学内)
            $body_s[$i]['B6'] = '';;//所属分類(学外)
            $body_s[$i]['C'] = '';;//カテゴリ
            $body_s[$i]['D'] = '';//タイトル
            $body_s[$i]['E'] = '';//質問内容
            $body_s[$i]['F'] = '';//振分日時
            $body_s[$i]['G'] = '';;//ステータス
            $body_s[$i]['H'] = '';;//回答者
            $body_s[$i]['I'] = '';;//回答者変更回数
            $body_s[$i]['J'] = '';;//回答内容
            $body_s[$i]['K'] = '';;//回答日時
        }
        if (touch($file_path)) {
            // オブジェクト生成
            $file = new SplFileObject($file_path, "w");
            // タイトル行のエンコードをSJIS-winに変換（一部環境依存文字に対応用）
            $header_s = array();
            foreach ($title_s as $key => $val) {
                $header_s[] = mb_convert_encoding($val, 'SJIS-win', 'UTF-8');
            }
            // エンコードしたタイトル行を配列ごとCSVデータ化
            $file->fputcsv($header_s);
            foreach ($body_s as $value) {
                $csv = array($value['id'], $value['A'], $value['B'], $value['B1'], $value['B2'], $value['B3'],
                    $value['B4'], $value['B5'], $value['B6'], $value['C'], $value['D'], $value['E'], $value['F']
                , $value['G'], $value['H'], $value['I'], $value['J'], $value['K']);
                foreach ($csv as $key => $val) {
                    $csv[$key] = mb_convert_encoding($val, 'SJIS-win', 'UTF-8');
                }
                $file->fputcsv($csv);
            }
        } else {
            die('file touch error');
        }
        // ------------------------------
        // csvダウンロード
        // ------------------------------
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . basename($file_path) . ';');
        header("Content-Transfer-Encoding: binary");
        readfile("$file_path");
        exit;
    }

    /**
     * ログ出力画面
     */
    public function output_log()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $dat['datatable'] = true;
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['result_dat'] = $this->answer_model->get_data($page, 999, 'list');
        $total_count = $this->answer_model->get_data('', '', 'count');
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'ログ出力画面(管理者)';

        $this->disp('admin/output_log', $dat);
    }

    /**
     * bcc送信者管理
     */
    public function bcc_manage()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $dat['datatable'] = true;
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['result_dat'] = $this->bcc_model->get_data($page, 999, 'list');
        $total_count = $this->answer_model->get_data('', '', 'count');
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'bcc送信者管理画面(管理者)';

        $this->disp('admin/bcc_manage', $dat);
    }

    /**
     * 回答者管理
     */
    public function user_manage()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $dat['datatable'] = true;
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['result_dat'] = $this->answer_model->get_data($page, 999, 'list');
        $total_count = $this->answer_model->get_data('', '', 'count');
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = '回答者管理画面(管理者)';

        $this->disp('admin/user_manage', $dat);
    }

    /**
     * 回答者登録フォーム
     */
    public function answer_entry()
    {
        /*form初期値*/
        $dat['form_dat']['name'] = '';
        $dat['form_dat']['email'] = '';
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['page_type'] = 'one';
        $dat['page_title'] = '回答者登録画面(管理者)';
        $this->disp('admin/answer_entry', $dat);
    }

    /**
     * 回答者編集フォーム
     */
    public function answer_edit()
    {
        $id = $_POST['id'];
        /*form初期値*/
        $dat['form_dat'] = $this->answer_model->get_detail($id);
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['page_type'] = 'one';
        $dat['page_title'] = '回答者登録画面(管理者)';
        $this->disp('admin/answer_entry', $dat);
    }

    /**
     * bcc送信者登録フォーム
     */
    public function bcc_sender_entry()
    {
        /*form初期値*/
        $dat['form_dat']['name'] = '';
        $dat['form_dat']['email'] = '';
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'bcc送信者登録画面(管理者)';
        $this->disp('admin/bcc_sender_entry', $dat);
    }

    /**
     * bcc送信者編集フォーム
     */
    public function bcc_sender_edit()
    {
        $id = $_POST['id'];
        /*form初期値*/
        $dat['form_dat'] = $this->bcc_model->get_detail($id);
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'bcc送信者登録画面(管理者)';
        $this->disp('admin/bcc_sender_entry', $dat);
    }

    /**
     * 管理者登録フォーム
     */
    public function admin_entry()
    {
        /*form初期値*/
        $dat['form_dat']['name'] = '';
        $dat['form_dat']['email'] = '';
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }
        $dat['category'] = $this->master_model->get_category('ja');
        $dat['page_type'] = 'one';
        $dat['page_title'] = '管理者登録画面(管理者)';
        $this->disp('admin/admin_entry', $dat);
    }

    /**
     * 回答者削除ロジック
     */
    public function answer_delete()
    {
        $a_id = $this->input->post('id');
        $send_dat = $this->answer_model->get_detail($a_id);
        $result = $this->answer_model->delete_data($a_id);
        if ($result == true) {
            $url = '';
            /*todo:mail送信 to回答者*/
            $title = MAIL_TITLE_A_DELETE;
            $template = 'a_delete';
            $this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url);
            redirect('admin/user_manage');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * bcc送信者削除ロジック
     */
    public function bcc_sender_delete()
    {
        $a_id = $this->input->post('id');
        $send_dat = $this->bcc_model->get_detail($a_id);
        $result = $this->bcc_model->delete_data($a_id);
        if ($result == true) {
            $url = '';
            /*todo:mail送信 to回答者*/
            $title = MAIL_TITLE_A_DELETE;
            $template = 'a_delete';
            //$this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url);
            redirect('admin/bcc_manage');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * 回答者登録/編集ロジック
     */
    public function answer_form()
    {
        if (!empty($_POST['prev'])) {
            redirect('admin/user_manage');
        }

        /*form初期値*/
        $_SESSION['form_dat'] = $this->input->post();
        $dat['form_dat'] = $_SESSION['form_dat'];

        /*validation*/
        $er_flg = false;
        if ($dat['form_dat']['name'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_name', true);
        }
        if ($dat['form_dat']['email'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_email', true);
        }
        if ($dat['form_dat']['user_code'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_user_code', true);
        }
        if ($_POST['id'] == '') {
            if ($dat['form_dat']['password'] == '') {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_password', true);
            }
            if ($dat['form_dat']['password'] != $dat['form_dat']['password_confirm']) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_password_confirm', true);
            }
            if (!$this->answer_model->user_code_chk($dat['form_dat']['user_code'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_user_code_double', true);
            }
            if (!$this->answer_model->email_chk($dat['form_dat']['email'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        } else {

            if (($dat['form_dat']['password'] != '') or ($dat['form_dat']['password_confirm'] != '')) {
                if ($dat['form_dat']['password'] != $dat['form_dat']['password_confirm']) {
                    $er_flg = true;
                    $this->session->set_flashdata('er_msg_password_confirm', true);
                }
            }
            if (!$this->answer_model->user_code_chk($dat['form_dat']['user_code'], $_POST['id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_user_code_double', true);
            }
            if (!$this->answer_model->email_chk($dat['form_dat']['email'], $_POST['id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        }

        //pre_print_r($er_flg);
        //pre_print_r($_SESSION);
        //exit;
        /*validation error*/
        if ($er_flg == true) {
            redirect('admin/answer_entry');
        }
        $dat = $_SESSION['form_dat'];
        $a_id = $this->answer_model->set_data($dat);

        $send_dat = $this->answer_model->get_detail($a_id);
        $url = '';
        /*todo:mail送信 to回答者*/
        if ($_POST['id'] == '') {//新規
            $title = MAIL_TITLE_A_ENTRY;
            $template = 'a_entry';
        } else {
            $title = MAIL_TITLE_A_EDIT;
            $template = 'a_edit';
        }

        $this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url);

        if ($a_id == true) {
            /*form session削除*/
            unset($_SESSION['form_dat']);
            redirect('admin/user_manage');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * bcc送信者登録/編集ロジック
     */
    public function bcc_sender_form()
    {
        if (!empty($_POST['prev'])) {
            redirect('admin/bcc_manage');
        }

        /*form初期値*/
        $_SESSION['form_dat'] = $this->input->post();
        $dat['form_dat'] = $_SESSION['form_dat'];

        /*validation*/
        $er_flg = false;
        if ($dat['form_dat']['name'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_name', true);
        }
        if ($dat['form_dat']['email'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_email', true);
        }
        if ($_POST['id'] == '') {
            if (!$this->bcc_model->email_chk($dat['form_dat']['email'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        } else {
            if (!$this->bcc_model->email_chk($dat['form_dat']['email'], $_POST['id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        }

        //pre_print_r($er_flg);
        //pre_print_r($_SESSION);
        //exit;
        /*validation error*/
        if ($er_flg == true) {
            redirect('admin/bcc_sender_entry');
        }
        $dat = $_SESSION['form_dat'];
        $a_id = $this->bcc_model->set_data($dat);

        if ($a_id == true) {
            /*form session削除*/
            unset($_SESSION['form_dat']);
            redirect('admin/bcc_manage');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * 管理者登録ロジック
     */
    public function admin_form()
    {
        if (!empty($_POST['cancel'])) {
            redirect('admin/user_manage');
        }

        /*form初期値*/
        $_SESSION['form_dat'] = $this->input->post();
        $dat['form_dat'] = $_SESSION['form_dat'];

        /*validation*/
        $er_flg = false;
        if ($dat['form_dat']['name'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_name', true);
        }
        if ($dat['form_dat']['user_code'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_user_code', true);
        }
        if ($dat['form_dat']['email'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_email', true);
        }
        if ($dat['form_dat']['password'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_user_code', true);
        }
        if ($_POST['id'] == '') {
            if ($dat['form_dat']['password'] == '') {
                $er_flg = 1;
                $this->session->set_flashdata('er_msg_password', true);
            }
            if (!$this->admin_model->user_code_chk($dat['form_dat']['user_code'])) {
                $er_flg = 2;
                $this->session->set_flashdata('er_msg_user_code_double', true);
            }
            if (!$this->admin_model->email_chk($dat['form_dat']['email'])) {
                $er_flg = 3;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        } else {
            if (!$this->admin_model->user_code_chk($dat['form_dat']['user_code'], $_POST['id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_user_code_double', true);
            }
            if (!$this->admin_model->email_chk($dat['form_dat']['email'], $_POST['id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_double', true);
            }
        }
        if ($er_flg == true) {
            redirect('admin/admin_entry');
        }
        $dat = $_SESSION['form_dat'];
        $a_id = $this->admin_model->set_data($dat);

        $send_dat = $this->admin_model->get_detail($a_id);
        $url = '';

        /*todo:mail送信 to回答者*/
        if ($_POST['id'] == '') {//新規
            $title = MAIL_TITLE_A_ENTRY_ADMIN;
            $template = 'admin_entry';
        } else {
            $title = MAIL_TITLE_A_EDIT_ADMIN;
            $template = 'admin_edit';
        }

        $this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url);

        if ($a_id == true) {
            /*form session削除*/
            unset($_SESSION['form_dat']);
            redirect('admin');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * 質問リスト
     */
    public function q_list()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $search = array();
        if ($this->input->get('search')) {
            $search = $this->input->get('search');
        }
        $dat['answer_dat'] = $this->answer_model->get_data('', '', 'all');//回答者選択用全件取得
        $dat['result_dat'] = $this->question_model->get_data($page, 20, 'list', $search);
        if ($dat['result_dat'] == array()) {
            //redirect('admin');
        }
        $total_count = $this->question_model->get_data('', '', 'count', $search);
        if ($total_count != array()) {
            $dat['total_count'] = $total_count['count'];
        } else {
            $dat['total_count'] = 0;
        }

        $dat['page_type'] = 'one';
        $dat['page_title'] = '管理者用質問リスト画面';

        $this->disp('admin/q_list', $dat);
    }

    /**
     * 回答者変更
     */
    public function answer_change()
    {
        //回答者未選択
        if (empty($_POST['answer_id'])) {
            redirect($_SERVER['HTTP_REFERER']);
        }

        $dat = $this->input->post();
        $q_id = $dat['id'];//質問
        $a_id = $dat['answer_id'];//回答者

        $send_dat = $this->answer_model->get_detail($a_id);
        $q_dat = $this->question_model->get_detail($q_id);
        $manage_id = $q_dat['re_question_id'] . '-' . $q_dat['manage_id'];

        /*20210709追加*/
        $q_dat['faculty'] = '';
        $faculty_data = false;
        if (!empty($q_dat['faculty_code'])) {
            $faculty_data = $this->master_model->get_faculty_data($q_dat['faculty_code']);
        }
        if ($faculty_data != false) {
            $q_dat['faculty'] = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
        }
        $q_dat['affiliation_category'] = '';
        $affiliation_category_data = $this->master_model->get_affiliation_category_data($q_dat['affiliation_category']);
        if ($affiliation_category_data != false) {
            $q_dat['affiliation_category'] = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
        }
        if ($q_dat["question_category_id"] == '') {
            $q_dat['category'] = '';
        } else {
            $q_dat['category'] = $this->master_model->get_category_detail('ja', $q_dat["question_category_id"]);
        }

        $send_dat['id'] = $q_dat['id'];
        $answer_change_dat = $this->question_model->get_answer_change_dat($q_dat);
        if ($answer_change_dat != false) {
            $send_dat['question_title'] = $answer_change_dat['question_title'];
            $send_dat['question_body'] = $answer_change_dat['question_body'];
        } else {
            $send_dat['question_title'] = '';
            $send_dat['question_body'] = '';
        }
        $q_dat['answer_name'] = $q_dat['name'];
        $q_dat['name'] = $send_dat['name'];
        $url = base_url('center/');//todo:URL生成 to回答者 該当の質問へのURL
        $chk_result = $this->question_model->chk_rel_answer($q_id);
        /*
        if ($chk_result != array()) {
            $title = MAIL_TITLE_A_CNG . $manage_id . '【振分不要】';
        } else {
            $title = MAIL_TITLE_A_CNG . $manage_id;
        }

        */
        /*todo:mail送信 to管理者*/
        $template = 'a_change_admin';
        //$this->send_mail($title, $template, $send_dat, ADMIN_MAIL, array(), '', $url);
        //【東大ESC】回答してください (Q&A回答依頼)　管理ID: 8-1
        $title = '【東大ESC/ESC UTokyo】回答してください (Q&A回答依頼)　ID:' . $manage_id;
        /*todo:mail送信 to回答者*/
        $send_dat['id'] = $manage_id;
        $q_dat['id'] = $manage_id;
        $template = 'a_change';
        $this->send_mail($title, $template, $q_dat, $send_dat['email'], array(), '', $url);

        $result = $this->question_model->set_rel_answer($dat);

        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * リストメンテナンス
     * （マスタ管理）
     */
    public function list_maintenance()
    {
        $lang = 'all';
        if ($this->input->get('search')) {
            $search = $this->input->get('search');
        } else {
            $search = 'class';
        }

        if ($search == 'class') {
            $dat['result_dat'] = $this->master_model->get_affiliation($lang);
        } elseif ($search == 'section') {
            $dat['result_dat'] = $this->master_model->get_faculty($lang);
        } elseif ($search == 'category') {
            $dat['result_dat'] = $this->master_model->get_category($lang);
        } else {
            show_404();
        }
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'リストメンテナンス画面(管理者)';

        $this->disp('admin/list_maintenance', $dat);
    }

    /**
     * リストメンテナンス
     * （マスタ管理）
     * 削除
     */
    public function list_delete()
    {
        $form_dat = $this->input->post();
        if ($this->master_model->list_delete($form_dat)) {
            $this->session->set_flashdata('msg', 'リストを削除しました');
        } else {
            $this->session->set_flashdata('er_msg', 'リストの削除に失敗しました');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * リストメンテナンス
     * （マスタ管理）
     * 公開/非公開
     */
    public function list_public()
    {
        $form_dat = $this->input->post();
        if ($this->master_model->list_public($form_dat)) {
            $this->session->set_flashdata('msg', 'リストの公開設定を変更しました');
        } else {
            $this->session->set_flashdata('er_msg', 'リストの公開設定変更に失敗しました');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * リストメンテナンス処理
     * （マスタ管理）
     */
    public function list_change()
    {
        $this->master_model->change($this->input->post());
        redirect('admin/list_maintenance?search=' . $this->input->post('type'));
    }

    /**
     * リストメンテナンス処理
     * （マスタ管理）
     *  マスタ登録
     */
    public function list_entry()
    {
        $this->master_model->entry($this->input->post());
        redirect('admin/list_maintenance?search=' . $this->input->post('type'));
    }

    /**
     * ログ管理
     */
    public function log_manage()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        $dat['result_dat'] = $this->activity_model->get_data($page, 20, 'list');
        $total_count = $this->answer_model->get_data('', '', 'count');
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        //$dat['page_title'] = 'ログ管理画面(管理者)';

        //$this->disp('admin/log_manage', $dat);
        $dat['page_title'] = 'ログ出力画面(管理者)';
        $this->disp('admin/output_log', $dat);
    }

    /**
     * FAQメンテナンス
     */
    public function faq_maintenance()
    {
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        } else {
            $page = 1;
        }
        if ($this->input->get('category')) {
            $category = $this->input->get('category');
        } else {
            $category = '';
        }
        $dat['category_dat'] = $this->master_model->get_category('ja');

        $dat['result_dat'] = $this->faq_model->get_data($page, 9999, 'list', '', $category);
        $total_count = $this->faq_model->get_data('', '', 'count', '', $category);
        $dat['total_count'] = $total_count['count'];
        $dat['page_type'] = 'one';
        $dat['page_title'] = 'FAQ管理画面(管理者)';

        $this->disp('admin/faq_maintenance', $dat);
    }

    /**
     * FAQ編集
     */
    public function faq_edit()
    {
        $dat_a['question_body'] = $_POST['question_body'];
        $dat_a['answer_body'] = $_POST['answer_body'];
        $this->db->where('s_faq_hst.id', $_POST['id']);
        $this->db->where('s_faq_hst.del_flg', 0);
        $this->db->update('s_faq_hst', $dat_a);
        $this->session->set_flashdata('msg', '内容の編集をしました');
        redirect("admin/faq_maintenance");
    }

    /**
     * FAQ編集
     */
    public function _faq_edit()
    {
        $dat_a['question_body'] = $_POST['question_body'];
        $this->db->where('s_question_hst.id', $_POST['id']);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->update('s_question_hst', $dat_a);
        $dat_b['answer_body'] = $_POST['answer_body'];
        $this->db->where('s_answer_hst.question_id', $_POST['id']);
        $this->db->where('s_answer_hst.del_flg', 0);
        $this->db->update('s_answer_hst', $dat_b);
        $this->session->set_flashdata('msg', '内容の編集をしました');
        redirect("admin/faq_maintenance");
    }

    /**
     * 公開/非公開
     * @param $type
     * @param $id
     * @param $roll
     */
    public function publish($type, $id, $roll = '')
    {
        if ($this->faq_model->publish_data($type, $id, $roll)) {
            $this->session->set_flashdata('msg', '公開設定の変更をしました');
        } else {
            $this->session->set_flashdata('er_msg', '公開設定の変更に失敗しました');
        }
        redirect("admin/faq_maintenance");
    }

    /**
     * 公開/非公開
     * @param $type
     * @param $id
     * @param $roll
     */
    public function delete($type, $id, $roll = '')
    {
        if ($this->faq_model->delete_data($type, $id, $roll)) {
            $this->session->set_flashdata('msg', '削除をしました');
        } else {
            $this->session->set_flashdata('er_msg', '削除に失敗しました');
        }
        redirect("admin/faq_maintenance");
    }

    /**
     * 強制完了
     */
    public function forced_done()
    {
        $id = $_GET['q_id'];
        if ($this->question_model->forced_done($id)) {
            $this->session->set_flashdata('msg', '強制完了をしました');
        } else {
            $this->session->set_flashdata('er_msg', '強制完了に失敗しました');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * 質問/回答内容確認
     */
    public function confirm()
    {
        $id = $_GET['q_id'];
        $q_id = base64_decode($id);

        if ($q_id != '') {
            $dat['result_dat'] = $this->question_model->get_detail($q_id, 0);//回答済みは除く
            $dat['answer_dat'] = $this->answer_model->get_qa($q_id);

            /*validation*/
            if ($dat['result_dat'] == false) {
                show_404();
            }

            //$lang = $this->input->get('lang');
            $lang = 'en';
            /* 多言語ファイル */
            $this->lang->load('site', $lang);

            $dat['page_type'] = 'confirm';
            $dat['page_title'] = '質問/回答表示画面';

            /*form session削除*/
            unset($_SESSION['form_dat']);
            $this->disp('answer/display', $dat);
        } else {
            //404
            show_404();
        }
    }

    /**
     * ログアウト
     */
    public function logout()
    {
        unset($_SESSION['admin_dat']);
        redirect('admin');
    }
}
