<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Question extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_model');
        $this->load->model('answer_model');
    }

    /**
     * 質問者用初期画面
     * 言語選択
     */
    public function index()
    {
        $dat['page_type'] = 'common';
        $dat['page_title'] = '質問者用初期画面';
        $this->disp('questioner/index', $dat);
    }

    /**
     * 質問登録フォーム
     * //outside
     * //within
     */
    public function input($action = '')
    {
        $action_get = $this->input->get('action');
        if ($action_get != '') {
            $action = $action_get;
        }
        if ($action != ('within') and $action != ('outside')) {
            show_404();
        }
        if ($action == '') {
            show_404();
        }
        $edit = $this->input->get('id');
        if ($edit != '') {
            $edit_flag = true;
            $dat['tmp_key'] = base64_decode($edit);
            $q_id = $dat['tmp_key'];
            $this->session->set_userdata('tmp_key', $q_id);
            //回答済みか確認
            //pre_print_r(strstr($q_id, '-', true));
            if ($this->question_model->chk_answer($q_id)) {
                echo '回答済みの質問のため編集できません';
                exit;
            }
        } else {
            $edit_flag = false;
            //新規登録時は仮IDセット
            if (!isset($_SESSION['tmp_key'])) {
                $this->session->set_userdata('tmp_key', date('YmdHis'));
                $dat['tmp_key'] = $this->session->userdata('tmp_key');
            } else {
                $dat['tmp_key'] = $this->session->userdata('tmp_key');
            }
        }

        /*form初期値*/
        $dat['form_dat']['id'] = '';
        $dat['form_dat']['name'] = '';
        $dat['form_dat']['faculty_code'] = '';
        $dat['form_dat']['department'] = '';
        $dat['form_dat']['laboratory'] = '';
        $dat['form_dat']['tel'] = '';
        $dat['form_dat']['email'] = '';
        $dat['form_dat']['email_check'] = '';
        $dat['form_dat']['question_category_id'] = '';
        $dat['form_dat']['question_title'] = '';
        $dat['form_dat']['question_body'] = '';
        $dat['form_dat']['affiliation'] = '';
        $dat['form_dat']['affiliation_category'] = '';
        if (isset($_SESSION['form_dat'])) {
            $dat['form_dat'] = $_SESSION['form_dat'];
        }

        $q_id = $this->input->get('id');
        if ($q_id != '') {
            $q_id = base64_decode($q_id);
            $dat['form_dat'] = $this->question_model->get_detail($q_id);
        }
        $dat['affiliation'] = $this->master_model->get_affiliation();
        $dat['faculty'] = $this->master_model->get_faculty();
        $dat['category'] = $this->master_model->get_category();
        $dat['page_type'] = 'common';
        $dat['action'] = $action;
        if ($edit == true) {
            $dat['edit'] = true;
            $txt = '質問編集';
        } else {
            $dat['edit'] = false;
            $txt = '質問登録';
        }
        if ($action == 'outside') {
            $dat['page_title'] = $txt . '画面（学外）';
        } else {
            $dat['page_title'] = $txt . '画面（学内）';
        }
        $this->disp('questioner/input', $dat);
    }

    /**
     * 質問登録フォーム/確認
     */
    public function confirm()
    {
        if (empty($_POST['type'])) {
            show_404();
        }
        $action = $_POST['type'];

        if (!empty($_POST['prev'])) {
            redirect('question/input/' . $action);
        }

        /*form初期値*/
        $_SESSION['form_dat'] = $this->input->post();
        $_SESSION['form_dat']['lang'] = $this->input->post('lang');
        $dat['form_dat'] = $_SESSION['form_dat'];

        /*validation*/
        $er_flg = false;
        if ($dat['form_dat']['type'] == 'outside') {//学外
            if ($dat['form_dat']['affiliation'] == '') {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_affiliation', true);
            }
            if (empty($dat['form_dat']['affiliation_category'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_affiliation_category', true);
            }
        } elseif ($dat['form_dat']['type'] == 'within') {//学内
            if (empty($dat['form_dat']['faculty_code'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_faculty_code', true);
            }
            if ($dat['form_dat']['department'] == '') {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_department', true);
            }
            if ($dat['form_dat']['laboratory'] == '') {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_laboratory', true);
            }
            if (empty($dat['form_dat']['question_category_id'])) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_question_category_id', true);
            }
        }
        if ($dat['form_dat']['name'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_name', true);
        }
        if ($dat['form_dat']['email'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_email', true);
        }
        if ($dat['form_dat']['edit'] == false) {
            if ($dat['form_dat']['email'] != $dat['form_dat']['email_check']) {
                $er_flg = true;
                $this->session->set_flashdata('er_msg_email_check', true);
            }
        }
        if ($dat['form_dat']['question_title'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_question_title', true);
        }
        if ($dat['form_dat']['question_body'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_question_body', true);
        }


        if ($er_flg == true) {
            if ($dat['form_dat']['secret_id'] != '') {
                redirect('question/input/' . $dat['form_dat']['type'] . '?id=' . $dat['form_dat']['secret_id']);
            } else {
                redirect('question/input/' . $dat['form_dat']['type']);
            }

        }

        if (!empty($_FILES['userfile']['name'])) {
            $this->file_model->multi_upload($_FILES, 'question', $_SESSION['tmp_key']);
        }

        $dat['affiliation'] = $this->master_model->get_affiliation();
        $dat['faculty'] = $this->master_model->get_faculty();
        $dat['category'] = $this->master_model->get_category();
        $dat['page_type'] = 'common';
        if ($dat['form_dat']['edit'] == false) {
            $txt = '質問登録';
            $dat['edit'] = false;
        } else {
            $txt = '質問編集';
            $dat['edit'] = true;
        }
        if ($action == 'outside') {
            $dat['page_title'] = $txt . '確認画面（学外）';
        } else {
            $dat['page_title'] = $txt . '確認画面（学内）';
        }
        $dat['action'] = $action;
        $this->disp('questioner/confirm', $dat);
    }

    /**
     * 質問登録フォーム/確認
     * 質問完了後メール送信
     */
    public function process()
    {
        $lang = 'ja';
        /*
        if (isset($_SESSION['lang'])) {
            if ($_SESSION['lang'] == '') {
                $lang = 'ja';
            } else {
                $lang = $this->session->userdata('lang');
            }
        } else {
            $lang = 'ja';
        }
         */
        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        if (!empty($_POST['prev'])) {
            //redirect('question/input?lang=' . $lang);
            redirect('question/input');
        }
        $dat = $_SESSION['form_dat'];


        $result = $this->question_model->set_question($dat);
        if ($result == true) {
            $send_dat = $this->question_model->get_detail($result);
            $this->file_model->renum($send_dat['id'], $_SESSION['tmp_key'], 'question');
            $send_dat['base_url'] = base_url();

            $send_dat = $this->question_model->get_detail($result);

            $this->db->where('r_question_answer.question_id', $send_dat['id']);
            $this->db->where('r_question_answer.del_flg', 0);
            $query = $this->db->get('r_question_answer');
            $result = $query->row_array();
            $manage_id = $send_dat['re_question_id'] . '-' . $send_dat['manage_id'];


            /*20210709追加*/
            $send_dat_answer['faculty'] = '';
            $send_dat['faculty'] = '';
            $faculty_data = false;
            if (!empty($send_dat['faculty_code'])) {
                $faculty_data = $this->master_model->get_faculty_data($send_dat['faculty_code']);
            }
            if ($faculty_data != false) {
                $send_dat_answer['faculty'] = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
                $send_dat['faculty'] = $send_dat_answer['faculty'];
            }
            $send_dat['affiliation_category'] = '';
            $send_dat_answer['affiliation_category'] = '';
            $affiliation_category_data = $this->master_model->get_affiliation_category_data($send_dat['affiliation_category']);
            if ($affiliation_category_data != false) {
                $send_dat_answer['affiliation_category'] = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
                $send_dat['affiliation_category'] = $send_dat_answer['affiliation_category'];
            }
            if ($send_dat["question_category_id"] == '') {
                $send_dat_answer['category'] = '';
                $send_dat['category'] = '';
            } else {
                $send_dat_answer['category'] = $this->master_model->get_category_detail('ja', $send_dat["question_category_id"]);
                $send_dat['category'] = $send_dat_answer['category'];
            }


            if ($result == array()) {//初回質問
                $title_a = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $manage_id;
                //メールタイトル初回質問
                //$title_a = '【東大ESC/ESC UTokyo】　ご質問を受け付けました/Question received ID：' . $send_dat['re_question_id'].'-'.$send_dat['manage_id'];
                $title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $manage_id;
                $flag_a = false;
            } else {//編集
                $title_a = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $manage_id . '【振分不要】';
                //メールタイトル
                //$title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $send_dat['id'] . '【振分不要】';
                $title = '【東大ESC/ESC UTokyo】回答してください (Q&A回答依頼)ID：' . $manage_id;
                //$title = MAIL_TITLE_Q_ADMIN;
                //$template_re = 're_' . $template;
                $flag_a = true;
            }
            if ($flag_a == true) {
                /*todo:mail送信 to回答者*/
                $template_re = 're_question';
                //$callback = 'question/complete';
                $send_dat_answer = $send_dat;
                $send_dat_answer['id'] = $manage_id;
                $answer_mail = $this->question_model->get_answer_mail($send_dat_answer['id']);
                $answer_name = $this->question_model->get_answer_mail($send_dat_answer['id']);
                $send_dat_answer['name'] = $answer_name;
                $url_a = base_url('center/login?role=answer');//todo:URL生成 to回答者
                $this->send_mail($title, $template_re, $send_dat_answer, $answer_mail, array(), '', $url_a);
            }


            //メールタイトル初回質問
            //$title_a = '【東大ESC/ESC UTokyo】　ご質問を受け付けました/Question received ID：' . $send_dat['re_question_id'].'-'.$send_dat['manage_id'];
            $title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $send_dat['re_question_id'] . '-' . $send_dat['manage_id'];
            /*todo:mail送信 to質問者*/
            $template = 'question';
            $encrypted_id = base64_encode($send_dat['id']);
            $send_dat['id'] = $send_dat['re_question_id'] . '-' . $send_dat['manage_id'];
            $url_q = base_url('question/input?action=' . $send_dat['type'] . '&id=' . $encrypted_id);//todo:URL生成
            $url_a = base_url('center/login?role=admin');//todo:URL生成 to管理者
            //$callback = 'question/complete';
            $this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url_q);

            /*todo:mail送信 to管理者*/
            $template = 'question_to_admin';
            //$callback = 'question/complete';
            $this->send_mail($title_a, $template, $send_dat, REPLY_MAIL, array(), '', $url_a);
            unset($_SESSION['form_dat']);
            redirect('question/complete');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }

    /**
     * 質問者登録完了画面
     */
    public function complete()
    {
        /*
        if (isset($_SESSION['lang'])) {
            if ($_SESSION['lang'] == '') {
                $lang = 'ja';
            } else {
                $lang = $this->session->userdata('lang');
            }
        } else {
            $lang = 'ja';
        }
        */
        $lang = 'ja';
        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $dat['page_type'] = 'common';
        $dat['page_title'] = '質問者登録完了画面';

        /*form session削除*/
        unset($_SESSION['tmp_key']);
        unset($_SESSION['form_dat']);
        $this->disp('questioner/complete', $dat);
    }

    /**
     * 回答画面表示
     */
    public function answer()
    {
        $q_id = $this->input->get('id');
        $q_id = base64_decode($q_id);
        if ($q_id != '') {
            $dat['result_dat'] = $this->question_model->get_detail($q_id, 2);//回答済みは除く
            $dat['answer_dat'] = $this->answer_model->get_qa($q_id);

            /*validation*/
            if ($dat['result_dat'] == false) {
                redirect();
            }
            if ($dat['answer_dat'] == false) {
                redirect();
            }


            //$lang = $this->input->get('lang');
            $lang = 'en';
            /* 多言語ファイル */
            $this->lang->load('site', $lang);

            $dat['page_type'] = 'confirm';
            $dat['page_title'] = '質問者回答表示画面';

            /*form session削除*/
            unset($_SESSION['form_dat']);
            $this->disp('questioner/answer', $dat);
        } else {
            //不正なURLはredirect
            redirect();
        }
    }

    /**
     * 回答確認処理
     */
    public function answer_complete()
    {
        $dat = $this->input->post();
        if ($dat['action'] == 'complete') {
            $this->question_model->set_status($dat['q_id'], 4);//回答完了処理
            redirect();//3-0-1へ
        } else {
            $result_dat = $this->question_model->get_detail(base64_decode($dat['q_id']), 2);
            //redirect('question/re_question?id=' . $dat['q_id'] . '&lang=' . $result_dat['lang_cd'] . '&action=' . $result_dat['type']);
            redirect('question/re_question?id=' . $dat['q_id'] . '&action=' . $result_dat['type']);
        }
    }

    /**
     * 再質問画面
     */
    public function re_question()
    {
        /*
        $lang = $this->input->get('lang');
        if ($lang == '') {
            if (isset($_SESSION['lang'])) {
                if ($_SESSION['lang'] == '') {
                    $lang = 'ja';
                } else {
                    $lang = $this->session->userdata('lang');
                }
            } else {
                $lang = 'ja';
            }
        }
        */
        //新規登録時は仮IDセット
        if (!isset($_SESSION['tmp_key'])) {
            $this->session->set_userdata('tmp_key', date('YmdHis'));
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        } else {
            $dat['tmp_key'] = $this->session->userdata('tmp_key');
        }

        $lang = 'ja';
        $action = $this->input->get('action');
        if ($action == '') {
            $action = 'outside';
        }
        if (isset($_SESSION['form_dat'])) {
            $dat['re_form_dat'] = $_SESSION['form_dat'];
        }

        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $q_id = $this->input->get('id');
        $q_id = base64_decode($q_id);
        if ($q_id == '') {
            redirect();
        }
        $dat['form_dat'] = $this->question_model->get_detail($q_id);
        $dat['answer_dat'] = $this->answer_model->get_qa($q_id);

        $dat['affiliation'] = $this->master_model->get_affiliation($lang);
        $dat['faculty'] = $this->master_model->get_faculty();
        $dat['category'] = $this->master_model->get_category();
        $dat['page_type'] = 'common';
        $dat['edit'] = false;
        $dat['q_id'] = $q_id;
        if ($action == 'outside') {
            $dat['page_title'] = '再質問登録画面（学外）';
        } else {
            $dat['page_title'] = '再質問登録画面（学内）';
        }

        $this->disp('questioner/re_question', $dat);
    }

    /**
     * 再質問処理
     */
    public function re_confirm()
    {
        /*
        $lang = $this->input->get('lang');
        if ($lang == '') {
            if ($_SESSION['lang'] == '') {
                $lang = 'ja';
            } else {
                $lang = $this->session->userdata('lang');
            }
        }
        */

        $lang = 'ja';
        $action = $this->input->get('action');
        if ($action == '') {
            $action = 'outside';
        }

        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $post_dat = $this->input->post();
        $dat['re_form_dat'] = $post_dat;

        $deq_id = $this->input->get('id');
        $q_id = base64_decode($deq_id);
        if ($q_id == '') {
            redirect();
        }

        $er_flg = false;
        if ($post_dat['question_title'] == '') {
            //再質問タイトル不要
            //$er_flg = true;
            //$this->session->set_flashdata('er_msg_question_title', true);
        }
        if ($post_dat['question_body'] == '') {
            $er_flg = true;
            $this->session->set_flashdata('er_msg_question_body', true);
        }

        if ($er_flg == true) {
            //redirect('question/re_question?id=' . $q_id . '&lang=' . $lang . '&action=' . $action);
            redirect('question/re_question?id=' . $deq_id . '&action=' . $action);
        }

        unset($_SESSION['form_dat']);
        $_SESSION['form_dat'] = $post_dat;
        if (!empty($_FILES['userfile']['name'])) {
            $this->file_model->multi_upload($_FILES, 'question', $_SESSION['tmp_key']);
        }

        $dat['form_dat'] = $this->question_model->get_detail($q_id);
        $dat['answer_dat'] = $this->answer_model->get_qa($q_id);

        $dat['affiliation'] = $this->master_model->get_affiliation($lang);
        $dat['faculty'] = $this->master_model->get_faculty();
        $dat['category'] = $this->master_model->get_category();
        $dat['page_type'] = 'common';
        $dat['edit'] = false;
        $dat['q_id'] = $q_id;
        if ($action == 'outside') {
            $dat['page_title'] = '再質問登録確認画面（学外）';
        } else {
            $dat['page_title'] = '再質問登録確認画面（学内）';
        }

        $_SESSION['lang'] = $lang;
        $this->disp('questioner/re_confirm', $dat);
    }


    /**
     * 再質問登録フォーム
     * 質問完了後メール送信
     */
    public function re_process()
    {
        /*
        if ($_SESSION['lang'] == '') {
            $lang = 'ja';
        } else {
            $lang = $this->session->userdata('lang');
        }
        */

        $lang = 'en';
        /* 多言語ファイル */
        $this->lang->load('site', $lang);

        $dat = $_SESSION['form_dat'];
        $dat['q_id'] = ($dat['id']);
        $dat['re_question_id'] = $_POST['re_question_id'];
        $dat['manage_id'] = $_POST['manage_id'];
        $dat['id'] = '';

        $result = $this->question_model->set_question($dat, 're');//再質問の場合
        if ($result == true) {
            $send_dat = $this->question_model->get_detail($result);


            /*20210709追加*/
            $send_dat_answer['faculty'] = '';
            $send_dat['faculty'] = '';
            $faculty_data = false;
            if (!empty($send_dat['faculty_code'])) {
                $faculty_data = $this->master_model->get_faculty_data($send_dat['faculty_code']);
            }
            if ($faculty_data != false) {
                $send_dat_answer['faculty'] = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
                $send_dat['faculty'] = $send_dat_answer['faculty'];
            }
            $send_dat['affiliation_category'] = '';
            $send_dat_answer['affiliation_category'] = '';
            $affiliation_category_data = $this->master_model->get_affiliation_category_data($send_dat['affiliation_category']);
            if ($affiliation_category_data != false) {
                $send_dat_answer['affiliation_category'] = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
                $send_dat['affiliation_category'] = $send_dat_answer['affiliation_category'];
            }
            if ($send_dat["question_category_id"] == '') {
                $send_dat_answer['category'] = '';
                $send_dat['category'] = '';
            } else {
                $send_dat_answer['category'] = $this->master_model->get_category_detail('ja', $send_dat["question_category_id"]);
                $send_dat['category'] = $send_dat_answer['category'];
            }


            $this->file_model->renum($send_dat['id'], $_SESSION['tmp_key'], 'question');
            $send_dat['base_url'] = base_url();
            $data_id = $send_dat['id'];

            $answer_mail = $this->question_model->get_answer_mail($send_dat['id']);
            $answer_dat = $this->question_model->get_answer($send_dat['id']);

            if ($send_dat['manage_id'] == 0) {
                $send_dat['id'] = $send_dat['id'] . '-' . $send_dat['manage_id'];
            } else {
                $send_dat['id'] = $send_dat['re_question_id'] . '-' . $send_dat['manage_id'];
            }

            /*todo:mail送信 to質問者*/

            $send_dat['base_url'] = base_url();
            $send_dat['admin_url'] = base_url('admin');

            //メールタイトル
            $title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $send_dat['id'];
            $template = 'question';

            $answer_dat['id'] = $answer_dat['question_id'];

            $encrypted_id = base64_encode($data_id);
            //$url_q = base_url('question/input?action=' . $send_dat['type'] . '&lang=' . $send_dat['lang_cd'] . '&id=' . $encrypted_id);//todo:URL生成
            $url_q = base_url('question/input?action=' . $send_dat['type'] . '&id=' . $encrypted_id);//todo:URL生成
            $url_a = base_url('center/login?role=answer');//todo:URL生成 to回答者
            //$callback = 'question/complete';
            $this->send_mail($title, $template, $send_dat, $send_dat['email'], array(), '', $url_q);

            /*todo:mail送信 to回答者*/
            //メールタイトル
            //$title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $send_dat['id'] . '【振分不要】';
            $title = '【東大ESC/ESC UTokyo】回答してください (Q&A回答依頼)ID：' . $send_dat['id'];
            //$title = MAIL_TITLE_Q_ADMIN;
            //$template_re = 're_' . $template;
            $template_re = 're_question';
            //$callback = 'question/complete';
            $this->send_mail($title, $template_re, $answer_dat, $answer_mail, array(), '', $url_a);

            /*todo:mail送信 to管理者*/
            //メールタイトル
            $title = '【東大ESC/ESC UTokyo】ご質問を受け付けました/Question received ID：' . $send_dat['id'] . '【振分不要】';
            //$title = MAIL_TITLE_Q_ADMIN;
            //$template_re = 're_' . $template;
            $template_re = 're_question_admin';
            //$callback = 'question/complete';
            $this->send_mail($title, $template_re, $send_dat, ADMIN_MAIL, array(), '', $url_a);

            redirect('question/complete');
        } else {
            //todo:error画面は必要なし？
            pre_print_r('error');
            exit;
        }
    }
}
