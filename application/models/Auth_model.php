<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * ログイン処理
     * @param $form_dat
     * @return bool
     */
    public function login($form_dat)
    {
        if ($form_dat['role'] == 'admin') {
            $this->db->where('m_admin.user_code', $form_dat['user_code']);
            $this->db->where('m_admin.password', md5($form_dat['password']));
            $this->db->where('m_admin.del_flg', 0);
            $query = $this->db->get('m_admin');
            $result = $query->row_array();
        } else if ($form_dat['role'] == 'answer') {
            $this->db->where('m_answer.user_code', $form_dat['user_code']);
            $this->db->where('m_answer.password', md5($form_dat['password']));
            $this->db->where('m_answer.del_flg', 0);
            $query = $this->db->get('m_answer');
            $result = $query->row_array();
        } else {
            return false;
        }

        if ($result == array()) {
            return false;
        } else {
            unset($_SESSION['admin_dat']);
            unset($_SESSION['answer_dat']);
            if ($form_dat['role'] == 'admin') {
                $this->session->set_userdata('admin_dat', $result);
            } else {
                $this->session->set_userdata('answer_dat', $result);
            }
            return true;
        }
    }

    /**
     * リマインダー整合性チェック
     * @param $form_dat
     * @return bool
     */
    public function remind_chk($form_dat)
    {
        if ($form_dat['role'] == 'admin') {
            $this->db->where('m_admin.user_code', $form_dat['user_code']);
            $this->db->where('m_admin.email', ($form_dat['email']));
            $this->db->where('m_admin.del_flg', 0);
            $query = $this->db->get('m_admin');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } else {
            $this->db->where('m_answer.user_code', $form_dat['user_code']);
            $this->db->where('m_answer.email', ($form_dat['email']));
            $this->db->where('m_answer.del_flg', 0);
            $query_a = $this->db->get('m_answer');
            $result_a = $query_a->row_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }

        }
    }

    /**
     * リマインダー整合性チェック
     * @param $form_dat
     * @return bool
     */
    public function pwd_resetting($form_dat)
    {
        $this->db->trans_start();
        if($form_dat['role'] == 'admin'){
            $table = 'm_admin';
            $this->db->where('m_admin.user_code', $form_dat['user_code']);
            $this->db->where('m_admin.id', ($form_dat['hash']));
            $this->db->where('m_admin.del_flg', 0);
            $query = $this->db->get('m_admin');
            $result = $query->row_array();
        }else{
            $table = 'm_answer';
            $this->db->where('m_answer.user_code', $form_dat['user_code']);
            $this->db->where('m_answer.id', ($form_dat['hash']));
            $this->db->where('m_answer.del_flg', 0);
            $query = $this->db->get('m_answer');
            $result = $query->row_array();
        }
        if ($result != array()) {
            $insert_dat['password'] = md5($form_dat['password']);
            $this->db->where($table . '.id', $form_dat['hash']);
            $this->db->where($table . '.del_flg', 0);
            $this->db->update($table, $insert_dat);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }
}