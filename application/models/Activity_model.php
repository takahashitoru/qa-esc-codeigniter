<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 行動データ管理
 */
class Activity_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * ログ取得
     * @param string $page
     * @param string $limit
     * @param string $type
     * @return bool
     */
    public function get_data($page = '', $limit = '', $type = 'list')
    {
        if ($type == 'list') {//リスト取得
            $this->db->select('activity_log.id,m_activity.content AS name,activity_log.user_id,activity_log.answer_id,activity_log.updated_at');
            $this->db->join('m_activity', 'm_activity.id = activity_log.activity_id');
            if ($page == 1) {
                $this->db->limit($limit * $page);
            } else {
                $this->db->limit($limit, $limit * ($page - 1));
            }
            $this->db->order_by('activity_log.id', 'desc');
            $query = $this->db->get('activity_log');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'count') {//データ件数取得
            $this->db->select('COUNT(activity_log.id) AS count');
            $this->db->join('m_activity', 'm_activity.id = activity_log.activity_id');
            $query = $this->db->get('activity_log');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * 対象者名取得（質問者or回答者）
     * @param $dat
     * @return bool
     */
    public function get_act($dat)
    {
        if ($dat['user_id'] != '') {
            $table = 'user_data';
            $id = $dat['user_id'];
        } elseif ($dat['answer_id'] != '') {
            $table = 'm_answer';
            $id = $dat['user_id'];
        } else {
            return '管理者';
        }
        $this->db->select('name');
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['name'];
        }

    }
}