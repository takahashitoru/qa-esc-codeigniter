<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 回答者データ管理
 */
class Answer_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * 回答者一覧取得*
     * @param string $page
     * @param string $limit
     * @param string $type
     * @param string $category
     * @return bool
     */
    public function get_data($page = '', $limit = '', $type = 'list', $category = '')
    {
        if ($type == 'list') {//リスト取得
            $this->db->where('m_answer.del_flg', 0);
            if ($page == 1) {
                $this->db->limit($limit * $page);
            } else {
                $this->db->limit($limit, $limit * ($page - 1));
            }
            $this->db->order_by('m_answer.id', 'desc');
            $query = $this->db->get('m_answer');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'count') {//データ件数取得
            $this->db->select('COUNT(m_answer.id) AS count');
            $this->db->where('m_answer.del_flg', 0);
            $query = $this->db->get('m_answer');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'all') {//全件取得
            $this->db->where('m_answer.del_flg', 0);
            $this->db->order_by('m_answer.id', 'desc');
            $query = $this->db->get('m_answer');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * 回答者詳細取得
     * @param $id
     * @return bool
     */
    public function get_detail($id)
    {
        $this->db->where('m_answer.id', $id);
        $this->db->where('m_answer.del_flg', 0);
        $query = $this->db->get('m_answer');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 回答者詳細取得
     * @param $id
     * @return bool
     */
    public function get_answer($id)
    {
        $this->db->where('s_answer_hst.id', $id);
        $this->db->where('s_answer_hst.del_flg', 0);
        $query = $this->db->get('s_answer_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問に対する回答取得
     * @param $id
     * @return bool
     */
    public function get_qa($id)
    {
        $this->db->where('s_answer_hst.question_id', $id);
        $this->db->where('s_answer_hst.del_flg', 0);
        $query = $this->db->get('s_answer_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問に対する回答全取得
     * @param $id
     * @return bool
     */
    public function get_answer_detail($id)
    {
        $this->db->where('s_answer_hst.question_id', $id);
        $this->db->where('s_answer_hst.del_flg', 0);
        $query = $this->db->get('s_answer_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 回答者登録
     * @param $dat
     * @return bool
     */
    public function set_data($dat)
    {
        if ($dat['id'] == '') {//新規
            $insert_dat['name'] = $dat['name'];
            $insert_dat['email'] = $dat['email'];
            $insert_dat['user_code'] = $dat['user_code'];
            if (!empty($dat['category'])) {
                //$insert_dat['category'] = $dat['category'];
            }
            $insert_dat['password'] = md5($dat['password']);
            $this->db->insert('m_answer', $insert_dat);
            $result = $this->db->insert_id();
            if ($result == array()) {
                return false;
            } else {
                $this->category_entry($dat, $result);
                return $result;
            }
        } else {//編集
            $update_dat['name'] = $dat['name'];
            $update_dat['email'] = $dat['email'];
            $update_dat['user_code'] = $dat['user_code'];
            if (!empty($dat['category'])) {
                //$update_dat['category'] = $dat['category'];
            }
            $update_dat['password'] = md5($dat['password']);
            $this->db->where('m_answer.id', $dat['id']);
            $this->db->where('m_answer.del_flg', 0);
            if ($this->db->update('m_answer', $update_dat)) {
                $this->category_entry($dat, $dat['id']);
                return $dat['id'];
            } else {
                return false;
            }
        }
    }

    /**
     * カテゴリ登録
     * @param $form_dat
     * @param $answer_id
     */
    public function category_entry($form_dat, $answer_id)
    {
        $this->db->where('answer_id', $answer_id);
        $this->db->delete('r_answer_category');
        if (!empty($form_dat['category'])) {
            $categorys = ($form_dat['category']);
            foreach ($categorys as $c) {
                $dat_tag['category_id'] = $c;
                $dat_tag['answer_id'] = $answer_id;
                $this->db->insert('r_answer_category', $dat_tag);
            }
        }
    }

    /**
     * カテゴリ取得
     * @param $answer_id
     * @return bool
     */
    public function get_category($answer_id)
    {
        $this->db->where('answer_id', $answer_id);
        $query = $this->db->get('r_answer_category');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 回答登録
     * @param $dat
     * @param $answer_id
     * @return bool
     */
    public function set_answer($dat, $answer_id)
    {
        $this->db->trans_start();
        $insert_dat['lang_cd'] = 'ja';//一旦日本語で固定
        if (!empty($dat['form_dat']['file_path'])) {
            $insert_dat['file_name'] = $dat['form_dat']['file_name'];
            $insert_dat['file_url'] = $dat['form_dat']['file_url'];
        }
        $insert_dat['answer_body'] = $dat['form_dat']['answer_body'];
        $insert_dat['question_id'] = $dat['form_dat']['q_id'];
        $insert_dat['created_at'] = date('Y-m-d H:i:s');
        $insert_dat['answer_id'] = $answer_id;
        $this->db->insert('s_answer_hst', $insert_dat);
        $result = $this->db->insert_id();

        $act_dat['user_id'] = '';
        $act_dat['answer_id'] = $answer_id;
        $act_dat['activity_id'] = 3;
        $this->db->insert('activity_log', $act_dat);//user_dataは常に追記

        $update_dat['answer_status_id'] = 4;//回答済み
        $update_dat['pub_flg'] = 0;//デフォルトは非公開
        $this->db->where('s_question_hst.id', $dat['form_dat']['q_id']);
        $this->db->update('s_question_hst', $update_dat);

        //20190610追加
        $set_dat['question_id'] = $dat['form_dat']['q_id'];
        $set_dat['answer_id'] = $result;
        $this->comit_data($set_dat);//セットFAQ

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * FAQデータ追加
     * @param $dat
     * @return bool
     */
    public function comit_data($dat)
    {
        $this->db->trans_start();
        $this->db->where('s_question_hst.id', $dat['question_id']);
        $question_query = $this->db->get('s_question_hst');
        $question_dat = $question_query->row_array();

        $this->db->where('s_answer_hst.id', $dat['answer_id']);
        $answer_query = $this->db->get('s_answer_hst');
        $answer_dat = $answer_query->row_array();

        $act_dat = $question_dat;
        $act_dat['answer_body'] = $answer_dat['answer_body'];
        unset($act_dat['id']);
        $this->db->insert('s_faq_hst', $act_dat);//faqデータセット

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * メアドチェック
     * @param $mail
     * @param $id
     * @return bool
     */
    public function email_chk($mail, $id = '')
    {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        $this->db->where('email', $mail);
        $this->db->where('del_flg', 0);
        $query = $this->db->get('m_answer');
        $result = $query->row_array();
        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ログインIDチェック
     * @param $code
     * @return bool
     */
    public function user_code_chk($code, $id = '')
    {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        $this->db->where('user_code', $code);
        $this->db->where('del_flg', 0);
        $query = $this->db->get('m_answer');
        $result = $query->row_array();

        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 回答者削除
     * @param $id
     * @return bool
     */
    public function delete_data($id)
    {
        $del_dat['del_flg'] = 1;
        $this->db->where('m_answer.id', $id);
        if ($this->db->update('m_answer', $del_dat)) {
            return true;
        } else {
            return false;
        }
    }
}
