<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 質問データ管理
 */
class Question_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * 質問登録
     * @param $dat
     * @param string $type
     * @return bool
     */
    public function set_question($dat, $type = '')
    {
        /*data set*/
        $user_dat['name'] = $dat['name'];
        if ($dat['type'] == 'outside') {
            $user_dat['affiliation'] = $dat['affiliation'];
            $user_dat['affiliation_category'] = $dat['affiliation_category'];

        }
        if ($dat['type'] == 'within') {
            $user_dat['faculty_code'] = $dat['faculty_code'];
            $user_dat['department'] = $dat['department'];
            $user_dat['laboratory'] = $dat['laboratory'];
            $insert_dat['question_category_id'] = $dat['question_category_id'];

        }
        $user_dat['tel'] = $dat['tel'];
        $user_dat['email'] = $dat['email'];
        $insert_dat['question_title'] = $dat['question_title'];
        $insert_dat['question_body'] = $dat['question_body'];
        $insert_dat['type'] = $dat['type'];
        $insert_dat['lang_cd'] = $dat['lang'];
        if ($type == 're') {
            if ($dat['re_question_id'] == '') {
                $insert_dat['re_question_id'] = $dat['q_id'];
            } else {
                $insert_dat['re_question_id'] = $dat['re_question_id'];
            }
            $insert_dat['manage_id'] = $dat['manage_id'] + 1;
            $insert_dat['answer_status_id'] = 3;
        } else {
            $this->db->select('MAX(s_question_hst.re_question_id) AS id');
            $this->db->where('s_question_hst.del_flg', 0);
            $query = $this->db->get('s_question_hst');
            $result = $query->row_array();
            if ($result == array()) {
                $insert_dat['re_question_id'] = 1;
            } else {
                $insert_dat['re_question_id'] = $result['id'] + 1;
            }
            $insert_dat['manage_id'] = 1;
            $insert_dat['answer_status_id'] = 1;
        }
        $this->db->trans_start();

        /*user_data登録*/
        $this->db->insert('user_data', $user_dat);//user_dataは常に追記
        $user_id = $this->db->insert_id();


        if ($dat['id'] != '') {//編集
            $activity = 2;
            $insert_dat['user_id'] = $user_id;
            unset($insert_dat['re_question_id']);
            unset($insert_dat['manage_id']);
            $this->db->where('s_question_hst.id', $dat['id']);
            $this->db->update('s_question_hst', $insert_dat);
            $result_id = $dat['id'];
        } else {//新規
            if ($type == 're') {//再質問は振り分け済み
                $insert_dat['answer_change_flg_pre'] = 1;
                $activity = 4;
            } else {
                $activity = 1;
            }
            $insert_dat['created_at'] = date('Y-m-d H:i:s');

            /*質問内容登録*/
            $this->db->insert('s_question_hst', $insert_dat);
            $id = $this->db->insert_id();

            if ($type == 're') {//再質問は振り分け済み
                $req_dat['re_question_flag'] = 1;
                $this->db->where('s_question_hst.id', $dat['q_id']);
                $this->db->update('s_question_hst', $req_dat);

                $this->db->where('r_question_answer.del_flg', 0);
                $this->db->where('r_question_answer.question_id', $dat['q_id']);
                $this->db->order_by('r_question_answer.id', 'desc');
                $query_re = $this->db->get('r_question_answer');
                $result_re = $query_re->row_array();
                $re_dat['answer_id'] = $result_re['answer_id'];
                $re_dat['question_id'] = $id;
                $this->db->insert('r_question_answer', $re_dat);
            }
            /*file、user_data紐付け*/
            if ((!empty($dat['file_url']) && (!empty($dat['file_name'])))) {
                $file_dat['file_url'] = $dat['file_url'];
                $file_dat['file_name'] = $dat['file_name'];
            }
            $file_dat['user_id'] = $user_id;
            $this->db->where('s_question_hst.id', $id);
            $this->db->update('s_question_hst', $file_dat);
            $result_id = $id;
        }

        $act_dat['user_id'] = $user_id;
        $act_dat['answer_id'] = '';
        $act_dat['activity_id'] = $activity;

        $this->db->insert('activity_log', $act_dat);//user_dataは常に追記
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return $result_id;
        }
    }

    /**
     * 変更回数
     * @param $question_id
     * @return bool|int
     */
    public function get_answer_count($question_id)
    {
        $this->db->select("id");
        $this->db->where('r_question_answer.question_id', $question_id);
        //$this->db->where('r_question_answer.del_flg', 0);
        $query = $this->db->get('r_question_answer');
        $result = $query->result_array();
        if ($result == array()) {
            return 0;
        } else {
            return count($result);
        }
    }

    /**
     * 回答者変更時の時質問データ取得
     * @param $question_dat
     * @return bool|int
     */
    public function get_answer_change_dat($question_dat)
    {
        $this->db->where('s_question_hst.re_question_id', $question_dat['re_question_id']);
        $this->db->where('s_question_hst.manage_id', $question_dat['manage_id']);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->where('s_question_hst.pub_flg', 1);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return ($result);
        }
    }

    /**
     * 質問一覧取得*
     * @param string $page
     * @param string $limit
     * @param string $type
     * @param array $search
     * @param string $category
     * @return bool
     */
    public function get_data($page = '', $limit = '', $type = 'list', $search = array(), $category = '')
    {
        $result_dat = array();
        if ($type == 'list') {//リスト取得
            if ($search == 'notyet') {//未振分
                $this->db->select('s_question_hst.id');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                $this->db->where('s_question_hst.del_flg', 0);
                $this->db->where('r_question_answer.answer_id', NULL);
                if ($limit != 9999) {
                    if ($page == 1) {
                        $this->db->limit($limit * $page);
                    } else {
                        $this->db->limit($limit, $limit * ($page - 1));
                    }
                }
                $this->db->group_by('s_question_hst.id');
                $this->db->order_by('s_question_hst.id', 'desc');
                $query = $this->db->get('s_question_hst');
                $result = $query->result_array();
                if ($result == array()) {
                    $result_dat = array();
                } else {
                    $i = 0;
                    $result_dat = array();
                    foreach ($result as $v) {
                        $this->db->select('s_question_hst.id,user_data.name,s_question_hst.updated_at,s_question_hst.question_category_id,
                s_question_hst.manage_id,s_question_hst.re_question_id,r_question_answer.updated_at,
            s_question_hst.question_title,s_question_hst.question_body,s_question_hst.answer_status_id,s_question_hst.created_at,
            s_question_hst.answer_change_flg,s_answer_hst.answer_body');
                        $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                        $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                        $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                        $this->db->where('s_question_hst.del_flg', 0);
                        $this->db->where('s_question_hst.id', $v['id']);
                        $this->db->where('r_question_answer.answer_id', NULL);
                        if (!empty($search['s_date'])) {
                            if ($search['s_date'] != '') {
                                $this->db->where('s_question_hst.created_at >=', $search['s_date']);
                            }
                        }
                        if (!empty($search['e_date'])) {
                            if ($search['e_date'] != '') {
                                $this->db->where('s_question_hst.created_at <=', $search['e_date']);
                            }
                        }
                        $query = $this->db->get('s_question_hst');
                        $result_a = $query->row_array();
                        if ($result_a == array()) {
                            $result_dat[$i] = '';
                        } else {
                            $result_dat[$i] = $result_a;
                        }
                        ++$i;
                    }
                }
                return $result_dat;
            } elseif ($category != '') {//カテゴリー区分があれば
                $this->db->select('s_question_hst.id');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                if ($page == 1) {
                    $this->db->limit($limit * $page);
                } else {
                    $this->db->limit($limit, $limit * ($page - 1));
                }
                $this->db->group_by('s_question_hst.id');
                $this->db->order_by('s_question_hst.id', 'desc');
                $query = $this->db->get('s_question_hst');
                $result = $query->result_array();
                $i = 0;
                foreach ($result as $v) {
                    $this->db->select('s_question_hst.id,user_data.name,s_question_hst.updated_at,s_question_hst.question_category_id,s_question_hst.answer_change_flg,
            s_question_hst.question_title,s_question_hst.question_body,s_question_hst.question_body,s_question_hst.answer_status_id,s_question_hst.created_at,
            s_question_hst.manage_id,s_question_hst.re_question_id,');
                    $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                    $this->db->join('m_category', 'm_category.id = s_question_hst.question_category_id', 'left');
                    if ($category == 'OT') {
                        $array = array(99, 0);
                        $this->db->where_in('s_question_hst.question_category_id', $array);
                    } else {
                        $this->db->where('m_category.code', $category);
                    }
                    $this->db->where('s_question_hst.del_flg', 0);
                    $this->db->where('s_question_hst.id', $v['id']);
                    $query = $this->db->get('s_question_hst');
                    $result_a = $query->row_array();
                    if ($result == array()) {
                        $result_dat[$i] = '';
                    } else {
                        $result_dat[$i] = $result_a;
                    }
                    ++$i;
                }
                return $result_dat;
            } else {
                $this->db->select('s_question_hst.id');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                if ($limit != 9999) {
                    if ($page == 1) {
                        $this->db->limit($limit * $page);
                    } else {
                        $this->db->limit($limit, $limit * ($page - 1));
                    }
                }
                $this->db->where('s_question_hst.del_flg', 0);
                if (!empty($search['s_date'])) {
                    if ($search['s_date'] != '') {
                        $this->db->where('s_question_hst.created_at >=', $search['s_date'].' 00:00:00');
                    }
                }
                if (!empty($search['e_date'])) {
                    if ($search['e_date'] != '') {
                        $this->db->where('s_question_hst.created_at <=', $search['e_date'].' 23:59:00');
                    }
                }
                $this->db->group_by('s_question_hst.id');
                $this->db->order_by('s_question_hst.id', 'desc');
                $query = $this->db->get('s_question_hst');
                $result = $query->result_array();
                $i = 0;
                foreach ($result as $v) {
                    $this->db->select('s_question_hst.id,user_data.name,s_question_hst.updated_at,
                    s_question_hst.question_category_id,s_question_hst.manage_id,s_question_hst.re_question_id,
                    s_question_hst.question_title,s_question_hst.question_body,s_question_hst.answer_status_id,
                    s_question_hst.created_at,s_question_hst.answer_change_flg,s_answer_hst.answer_body,
                    user_data.email,user_data.tel,user_data.department,user_data.faculty_code,user_data.laboratory,
                    user_data.affiliation_category');
                    $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                    $this->db->where('s_question_hst.del_flg', 0);
                    $this->db->where('s_question_hst.id', $v['id']);
                    $query = $this->db->get('s_question_hst');
                    $result_a = $query->row_array();
                    if ($result == array()) {
                        $result_dat[$i] = '';
                    } else {
                        $result_dat[$i] = $result_a;
                    }
                    ++$i;
                }
                return $result_dat;
            }
        } elseif ($type == 'count') {//データ件数取得
            if ($search == 'notyet') {//未振分
                $this->db->select('COUNT(s_question_hst.id) AS count');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                $this->db->where('s_question_hst.del_flg', 0);
                $this->db->where('r_question_answer.answer_id', NULL);
                $query = $this->db->get('s_question_hst');
                $result = $query->row_array();
                if ($result == array()) {
                    return false;
                } else {
                    return $result;
                }
            } elseif ($category != '') {//カテゴリー区分があれば
                $this->db->select('COUNT(s_question_hst.id) AS count');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                $this->db->join('m_category', 'm_category.id = s_question_hst.question_category_id', 'left');
                if ($category == 'OT') {
                    $array = array(99, 0);
                    $this->db->where_in('s_question_hst.question_category_id', $array);
                } else {
                    $this->db->where('m_category.code', $category);
                }
                $this->db->where('s_question_hst.del_flg', 0);
                $this->db->where('r_question_answer.answer_id', NULL);
                $query = $this->db->get('s_question_hst');
                $result = $query->row_array();
                if ($result == array()) {
                    return false;
                } else {
                    return $result;
                }
            } else {
                $this->db->select('s_question_hst.id');
                $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                $this->db->where('s_question_hst.del_flg', 0);
                $this->db->group_by('s_question_hst.id');
                $this->db->order_by('s_question_hst.id', 'desc');
                $query = $this->db->get('s_question_hst');
                $result = $query->result_array();
                $i = 0;
                $result_dat['count'] = 0;
                foreach ($result as $v) {
                    $this->db->select('s_question_hst.id,user_data.name,s_question_hst.updated_at,s_question_hst.question_category_id,s_question_hst.manage_id,s_question_hst.re_question_id,
            s_question_hst.question_title,s_question_hst.question_body,s_question_hst.answer_status_id,s_question_hst.created_at,s_question_hst.answer_change_flg,s_answer_hst.answer_body');
                    $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
                    $this->db->where('s_question_hst.del_flg', 0);
                    $this->db->where('s_question_hst.id', $v['id']);
                    $query = $this->db->get('s_question_hst');
                    $result_a = $query->row_array();
                    if ($result_a == array()) {
                        //
                    } else {
                        ++$result_dat['count'];
                    }
                    ++$i;
                }
                return $result_dat;
            }
        }
    }

    /**
     * 質問一覧取得*
     * @param string $page
     * @param string $limit
     * @param string $type
     * @param string $my_id
     * @param string $category
     * @return bool
     */
    public function get_mydata($page = '', $limit = '', $type = 'list', $my_id = '', $category = '')
    {
        if ($type == 'list') {//リスト取得
            $this->db->select('s_question_hst.id,s_question_hst.question_body,user_data.name,s_question_hst.updated_at,s_question_hst.question_category_id,m_answer_status.title,s_question_hst.re_question_id,
            s_question_hst.question_title,s_question_hst.answer_status_id,s_question_hst.created_at,s_question_hst.answer_change_flg,s_question_hst.manage_id');
            $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
            $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
            $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
            $this->db->join('m_answer_status', 'm_answer_status.id = s_question_hst.answer_status_id');
            if ($category == 'unanswered') {//未回答
                $this->db->where('s_answer_hst.id', null);
            }
            $this->db->where('r_question_answer.del_flg', 0);
            $this->db->where('s_question_hst.del_flg', 0);
            $this->db->where('r_question_answer.answer_id', $my_id);
            if ($page == 1) {
                $this->db->limit($limit * $page);
            } else {
                $this->db->limit($limit, $limit * ($page - 1));
            }
            $this->db->order_by('s_question_hst.id', 'desc');
            $this->db->group_by('s_question_hst.id');
            $query = $this->db->get('s_question_hst');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'count') {//データ件数取得
            $this->db->select('COUNT(s_question_hst.id) AS count');
            $this->db->join('r_question_answer', 's_question_hst.id = r_question_answer.question_id', 'left');
            $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
            $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
            if ($category == 'unanswered') {
                $this->db->where('s_answer_hst.id', null);
            }
            $this->db->where('r_question_answer.del_flg', 0);
            $this->db->where('s_question_hst.del_flg', 0);
            $this->db->where('r_question_answer.answer_id', $my_id);
            $query = $this->db->get('s_question_hst');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * 再質問取得
     * @param $q_id
     * @return bool
     */
    public function get_re_question($q_id)
    {
        $this->db->where('s_question_hst.re_question_id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問毎に振り分けられた回答者の取得
     * @param $q_id
     * @return bool
     */
    public function get_lang($q_id)
    {
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['lang_cd'];
        }
    }

    /**
     * 質問毎に振り分けられた回答者の取得
     * @param $q_id
     * @return bool
     */
    public function get_rel_answer($q_id)
    {
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('r_question_answer.question_id', $q_id);
        $this->db->where('r_question_answer.del_flg', 0);
        $query = $this->db->get('r_question_answer');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     *  振り分け済みかの確認
     * @param $q_id
     * @return bool
     */
    public function chk_rel_answer($q_id)
    {
        $this->db->where('r_question_answer.question_id', $q_id);
        $this->db->where('r_question_answer.del_flg', 0);
        $query = $this->db->get('r_question_answer');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問毎に振り分けられた回答者の取得
     * @param $form_dat
     * @return bool
     */
    public function set_rel_answer($form_dat)
    {
        $this->db->trans_start();
        $del_dat['del_flg'] = 1;
        $this->db->where('r_question_answer.question_id', $form_dat['id']);
        $this->db->where('r_question_answer.del_flg', 0);
        $this->db->update('r_question_answer', $del_dat);

        //変更の際
        $cng_dat['answer_change_flg'] = 1;
        $this->db->where('s_question_hst.id', $form_dat['id']);
        $this->db->where('s_question_hst.answer_change_flg_pre', 1);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->update('s_question_hst', $cng_dat);

        //初回振り分け
        $pre_dat['answer_change_flg_pre'] = 1;
        $this->db->where('s_question_hst.id', $form_dat['id']);
        $this->db->where('s_question_hst.answer_change_flg_pre', 0);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->update('s_question_hst', $pre_dat);

        if ($form_dat['answer_id'] == '') {
            return false;
        }
        $dat['question_id'] = $form_dat['id'];
        $dat['answer_id'] = $form_dat['answer_id'];
        $this->db->insert('r_question_answer', $dat);

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 回答済みかチェック
     * @param $q_id
     * @return bool
     */
    public function chk_answer($q_id)
    {
        //$this->db->where_in('s_question_hst.answer_status_id', array(2, 4));
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.re_question_flag', 1);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 質問詳細取得
     * @param $q_id
     * @param string $status
     * @return bool
     */
    public function get_detail($q_id, $status = '')
    {
        $this->db->select('s_question_hst.id,s_question_hst.created_at,s_question_hst.updated_at,s_question_hst.question_title,
        s_question_hst.question_body,s_question_hst.file_url,s_question_hst.file_name,s_question_hst.lang_cd,s_question_hst.type,
        s_question_hst.question_category_id,user_data.id AS user_id,user_data.department,user_data.laboratory,user_data.faculty_code,
        user_data.name,user_data.email,user_data.tel,user_data.department,user_data.laboratory,user_data.faculty_code,
        user_data.affiliation,user_data.affiliation_category,s_question_hst.manage_id,s_question_hst.re_question_id,
        s_question_hst.answer_change_flg,');
        $this->db->join('user_data', 'user_data.id = s_question_hst.user_id');
        if ($status == 2) {
            $this->db->where_in('s_question_hst.answer_status_id', array(2, 4));
        }
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 再質問回答者メール取得
     * @param $q_id
     * @return bool
     */
    public function get_answer_mail($q_id)
    {
        $this->db->select('m_answer.email');
        $this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['email'];
        }
    }

    /**
     * 再質問回答者名前取得
     * @param $q_id
     * @return bool
     */
    public function get_answer_name($q_id)
    {
        $this->db->select('m_answer.name');
        $this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['name'];
        }
    }

    /**
     * 再質問回答者情報取得
     * @param $q_id
     * @return bool
     */
    public function get_answer($q_id)
    {
        $this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問取得取得（カテゴリごと）
     * @param string $category
     * @param string $front
     * @return bool
     */
    public function get_qa($category = '', $front = '')
    {
        $this->db->select('s_question_hst.id,s_question_hst.created_at,s_question_hst.updated_at,s_question_hst.question_title,
        s_question_hst.question_body,s_question_hst.file_url,s_question_hst.file_name,s_question_hst.lang_cd,s_question_hst.type,
        s_question_hst.question_category_id,s_question_hst.pub_flg,s_question_hst.answer_status_id');
        //$this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        //$this->db->join('s_answer_hst', 's_answer_hst.id = r_question_answer.answer_id');
        if ($category == 99) {
            $array = array(99, 0);
            $this->db->where_in('s_question_hst.question_category_id', $array);
        } else {
            $this->db->where('s_question_hst.question_category_id', $category);
        }

        if ($front == 'front') {
            $this->db->where('s_question_hst.pub_flg', 1);
        }
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->where_in('s_question_hst.answer_status_id', array(2, 4));
        $this->db->order_by('s_question_hst.id', 'desc');
        $query = $this->db->get('s_question_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問回答完了or再質問処理
     * @param $q_id
     * @param $status //2回答登録//3再質問
     * @return bool
     */
    public function set_status($q_id, $status)
    {
        $dat['answer_status_id'] = $status;
        $this->db->where('s_question_hst.id', $q_id);
        $this->db->where('s_question_hst.del_flg', 0);
        if ($this->db->update('s_question_hst', $dat)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 再質問取得
     * @param $q_id
     * @param string $front
     * @return bool
     */
    public function get_requestion($q_id, $front = '')
    {
        $this->db->select('s_question_hst.id,s_question_hst.created_at,s_question_hst.updated_at,s_question_hst.question_title,
        s_question_hst.question_body,s_question_hst.file_url,s_question_hst.file_name,s_question_hst.lang_cd,s_question_hst.type,
        s_question_hst.question_category_id,s_question_hst.pub_flg');
        $this->db->where('s_question_hst.re_question_id', $q_id);
        $this->db->where('s_question_hst.answer_status_id', 4);
        if ($front == 'front') {
            $this->db->where('s_question_hst.pub_flg', 1);
        }
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->order_by('s_question_hst.id', 'asc');
        $query = $this->db->get('s_question_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 質問ベース取得
     * @param $q_id
     * @param string $front
     * @return bool
     */
    public function get_base_q($q_id, $front = '')
    {
        $this->db->select('s_question_hst.re_question_id');
        $this->db->where('s_question_hst.id', $q_id);
        if ($front == 'front') {
            $this->db->where('s_question_hst.pub_flg', 1);
        }
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->order_by('s_question_hst.id', 'asc');
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            $this->db->select('s_question_hst.id,s_question_hst.created_at,s_question_hst.updated_at,s_question_hst.question_title,
        s_question_hst.question_body,s_question_hst.file_url,s_question_hst.file_name,s_question_hst.lang_cd,s_question_hst.type,
        s_question_hst.question_category_id,s_question_hst.pub_flg');
            $this->db->where('s_question_hst.re_question_id', $result['re_question_id']);
            $this->db->where('s_question_hst.answer_status_id', 4);
            if ($front == 'front') {
                $this->db->where('s_question_hst.pub_flg', 1);
            }
            $this->db->where('s_question_hst.del_flg', 0);
            $this->db->order_by('s_question_hst.id', 'asc');
            $query = $this->db->get('s_question_hst');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * 再質問取得（faq表示）
     * @param $q_id
     * @param string $front
     * @return bool
     */
    public function get_requestion_faq($q_id, $front = '')
    {
        $this->db->select('s_question_hst.id,s_question_hst.created_at,s_question_hst.updated_at,s_question_hst.question_title,
        s_question_hst.question_body,s_question_hst.file_url,s_question_hst.file_name,s_question_hst.lang_cd,s_question_hst.type,
        s_question_hst.question_category_id,s_question_hst.pub_flg');
        $this->db->where('s_question_hst.re_question_id', $q_id);
        if ($front == 'front') {
            $this->db->where('s_question_hst.pub_flg', 1);
        }
        $this->db->where_in('s_question_hst.answer_status_id', array(2, 4));
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->order_by('s_question_hst.id', 'asc');
        $query = $this->db->get('s_question_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * FAQ表示/非表示
     * @param $type
     * @param $id
     * @param $roll
     * @return bool
     */
    public function publish_data($type, $id, $roll)
    {
        if ($type == 'disp') {
            $type = 1;
        } else {
            $type = 0;
        }
        $dat['pub_flg'] = $type;
        $dat['updated_at'] = date("Y-m-d H:i:s", time());
        $this->db->where('s_question_hst.id', $id);

        if ($this->db->update('s_question_hst', $dat)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 統計(最終回答日
     * @param $a_id
     * @return bool
     */
    public function latest_answer_time($a_id)
    {
        $this->db->select('s_question_hst.updated_at');
        $this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('r_question_answer.answer_id', $a_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->order_by('s_question_hst.id', 'desc');
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['updated_at'];
        }
    }

    /**
     * 統計(最終回答数
     * @param $a_id
     * @return bool
     */
    public function latest_answer_count($a_id)
    {
        $this->db->select('count(s_question_hst.id) AS count');
        $this->db->join('r_question_answer', 'r_question_answer.question_id = s_question_hst.id');
        $this->db->join('m_answer', 'm_answer.id = r_question_answer.answer_id');
        $this->db->where('r_question_answer.answer_id', $a_id);
        $this->db->where('s_question_hst.del_flg', 0);
        $this->db->order_by('s_question_hst.id', 'desc');
        $query = $this->db->get('s_question_hst');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result['count'];
        }
    }

    /**
     * 強制完了
     * @param $id
     * @param int $type
     * @return bool
     */
    public function forced_done($id, $type = 4)
    {
        $type = 5;
        $dat['pub_flg'] = 1;
        $dat['del_flg'] = 0;
        $dat['answer_status_id'] = $type;
        $dat['updated_at'] = date("Y-m-d H:i:s", time());
        $this->db->where('s_question_hst.id', $id);
        if ($this->db->update('s_question_hst', $dat)) {
            return true;
        } else {
            return false;
        }
    }
}
