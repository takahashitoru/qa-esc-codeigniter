<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * bcc送信者データ管理
 */
class Bcc_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     *  bcc送信者一覧取得*
     * @param string $page
     * @param string $limit
     * @param string $type
     * @param string $category
     * @return bool
     */
    public function get_data($page = '', $limit = '', $type = 'list', $category = '')
    {
        if ($type == 'list') {//リスト取得
            $this->db->where('m_bcc_sender.del_flg', 0);
            if ($page == 1) {
                $this->db->limit($limit * $page);
            } else {
                $this->db->limit($limit, $limit * ($page - 1));
            }
            $this->db->order_by('m_bcc_sender.id', 'desc');
            $query = $this->db->get('m_bcc_sender');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'count') {//データ件数取得
            $this->db->select('COUNT(m_bcc_sender.id) AS count');
            $this->db->where('m_bcc_sender.del_flg', 0);
            $query = $this->db->get('m_bcc_sender');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } elseif ($type == 'all') {//全件取得
            $this->db->where('m_bcc_sender.del_flg', 0);
            $this->db->order_by('m_bcc_sender.id', 'desc');
            $query = $this->db->get('m_bcc_sender');
            $result = $query->result_array();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        }
    }

    /**
     * 回答者詳細取得
     * @param $id
     * @return bool
     */
    public function get_detail($id)
    {
        $this->db->where('m_bcc_sender.id', $id);
        $this->db->where('m_bcc_sender.del_flg', 0);
        $query = $this->db->get('m_bcc_sender');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 回答者登録
     * @param $dat
     * @return bool
     */
    public function set_data($dat)
    {
        if ($dat['id'] == '') {//新規
            $insert_dat['name'] = $dat['name'];
            $insert_dat['email'] = $dat['email'];
            $this->db->insert('m_bcc_sender', $insert_dat);
            $result = $this->db->insert_id();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } else {//編集
            $update_dat['name'] = $dat['name'];
            $update_dat['email'] = $dat['email'];
            $this->db->where('m_bcc_sender.id', $dat['id']);
            $this->db->where('m_bcc_sender.del_flg', 0);
            if ($this->db->update('m_bcc_sender', $update_dat)) {
                return $dat['id'];
            } else {
                return false;
            }
        }
    }

    /**
     * メアドチェック
     * @param $mail
     * @param $id
     * @return bool
     */
    public function email_chk($mail, $id = '')
    {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        $this->db->where('email', $mail);
        $this->db->where('del_flg', 0);
        $query = $this->db->get('m_bcc_sender');
        $result = $query->row_array();
        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }
    

    /**
     * bcc送信者削除
     * @param $id
     * @return bool
     */
    public function delete_data($id)
    {
        $del_dat['del_flg'] = 1;
        $this->db->where('m_bcc_sender.id', $id);
        if ($this->db->update('m_bcc_sender', $del_dat)) {
            return true;
        } else {
            return false;
        }
    }
}
