<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ファイル管理
 */
class File_model extends CI_Model
{

    /**
     * 複数ファイルアップロード
     * @param $file
     * @param $type
     * @param $id
     * @throws Exception
     */
    public function multi_upload($file, $type, $id)
    {
        $hash = hash('sha256', random_bytes(32));
        $count = count(array_filter($file['userfile']['name']));
        $datetime_y = date('Y');
        $datetime_m = date('m');
        $datetime_d = date('d');
        $datetime_t = date('His');
        $datetime = $datetime_y . '/' . $datetime_m . '/' . $datetime_d . '/' . $datetime_t;
        $relative_path = './uploads/' . $type . '/' . $datetime;//upload dir
        $full_path = base_url() . 'uploads/' . $type . '/' . $datetime;//upload dir
        $config['upload_path'] = $relative_path;
        $config['allowed_types'] = 'pdf|PDF';//ファイルタイプpdfのみ
        $config['max_size'] = 50000;//max約50MB
        $this->load->library('upload');
        if (!file_exists($relative_path)) {
            mkdir($relative_path, 0777, true);
        }

        //削除
        $del_dat['delete_flag'] = 1;
        $this->db->where('type', $type);
        $this->db->where('content_id', $id);
        $this->db->update('t_attachment_file', $del_dat);

        for ($i = 0; $i < $count; $i++) {
            $config['file_name'] = 'download_' . date('YmdHis').$i;
            $this->upload->initialize($config);
            $_FILES['userfile']['name'] = $file['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $file['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $file['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $file['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $file['userfile']['size'][$i];
            $filename = $_FILES['userfile']['name'];
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            if (!$this->upload->do_upload('userfile')) {
                $er_msg = $this->upload->display_errors();
                $er_msg .= $_FILES['userfile']['name'];
                $this->session->set_flashdata('file', $er_msg);
                redirect($_SERVER['HTTP_REFERER']);
                exit;
                //todo : アップに失敗したらrollbackさせる
            }
            $dat['type'] = $type;
            $dat['content_id'] = $id;
            $dat['file_url'] = $full_path . '/download_' . date('YmdHis') . $i.'.' . $extension;
            $dat['file_name'] = $filename;
            $dat['file_type'] = $_FILES['userfile']['type'];
            //$dat['last_updated_type'] = $_SESSION['admin_dat']['type'];
            //$dat['last_updated_id'] = $_SESSION['admin_dat']['id'];
            $dat['created_at'] = date('Y-m-d H:i:s');
            $dat['token'] = $hash;

            $this->db->insert('t_attachment_file', $dat);
        }
    }

    /**
     * 添付ファイル取得
     * @param $type
     * @param $content_id
     * @return bool
     */
    public function get_file($type, $content_id)
    {
        $this->db->where('type', $type);
        $this->db->where('content_id', $content_id);
        $this->db->where('delete_flag', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('t_attachment_file');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }


    /**
     * content_id振り直し
     * @param $id
     * @param $old_key
     * @param $type
     * @return bool
     */
    public function renum($id, $old_key,$type)
    {
        $dat['content_id'] = $id;
        $this->db->where('type', $type);
        $this->db->where('content_id', $old_key);
        $this->db->where('delete_flag', 0);
        if ($this->db->update('t_attachment_file',$dat)) {
            return false;
        } else {
            return true;
        }
    }
}
