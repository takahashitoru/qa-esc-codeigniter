<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 管理者データ管理
 */
class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * 質問登録
     * @param $dat
     * @return bool
     */
    public function set_question($dat)
    {
        $this->db->trans_start();
        /*data set*/
        $user_dat['name'] = $dat['name'];
        if($dat['type'] != 'within') {
            $user_dat['affiliation'] = $dat['affiliation'];
            $user_dat['affiliation_category'] = $dat['affiliation_category'];
        }
        $user_dat['department'] = $dat['department'];
        $user_dat['laboratory'] = $dat['laboratory'];
        $user_dat['tel'] = $dat['tel'];
        $user_dat['email'] = $dat['email'];
        $insert_dat['question_title'] = $dat['question_title'];
        $insert_dat['question_body'] = $dat['question_body'];
        $insert_dat['lang_cd'] = $dat['lang'];

        /*user_data登録*/
        $this->db->insert('user_data', $user_dat);
        $user_id = $this->db->insert_id();

        /*質問内容登録*/
        $this->db->insert('s_question_hst', $insert_dat);
        $id = $this->db->insert_id();

        /*file、user_data紐付け*/
        $file_dat['file_url'] = $dat['file_path'];
        $file_dat['user_id'] = $user_id;
        $this->db->where('s_question_hst.id', $id);
        $this->db->update('s_question_hst', $file_dat);

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 管理者登録
     * @param $dat
     * @return bool
     */
    public function set_data($dat)
    {
        if ($dat['id'] == '') {//新規
            $insert_dat['name'] = $dat['name'];
            $insert_dat['email'] = $dat['email'];
            $insert_dat['user_code'] = $dat['user_code'];
            $insert_dat['password'] = md5($dat['password']);
            $this->db->insert('m_admin', $insert_dat);
            $result = $this->db->insert_id();
            if ($result == array()) {
                return false;
            } else {
                return $result;
            }
        } else {//編集
            $update_dat['name'] = $dat['name'];
            $update_dat['email'] = $dat['email'];
            $update_dat['user_code'] = $dat['user_code'];
            $update_dat['password'] = md5($dat['password']);
            $this->db->where('m_admin.id', $dat['id']);
            $this->db->where('m_admin.del_flg', 0);
            if ($this->db->update('m_admin', $update_dat)) {
                return $dat['id'];
            } else {
                return false;
            }
        }
    }

    /**
     * 管理者詳細取得
     * @param $id
     * @return bool
     */
    public function get_detail($id)
    {
        $this->db->where('m_admin.id', $id);
        $this->db->where('m_admin.del_flg', 0);
        $query = $this->db->get('m_admin');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * メアドチェック
     * @param $mail
     * @param $id
     * @return bool
     */
    public function email_chk($mail, $id = '')
    {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        $this->db->where('email', $mail);
        $this->db->where('del_flg', 0);
        $query = $this->db->get('m_admin');
        $result = $query->row_array();
        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ログインIDチェック
     * @param $code
     * @return bool
     */
    public function user_code_chk($code, $id = '')
    {
        if ($id != '') {
            $this->db->where('id !=', $id);
        }
        $this->db->where('user_code', $code);
        $this->db->where('del_flg', 0);
        $query = $this->db->get('m_admin');
        $result = $query->row_array();

        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }
}
