<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *マスタデータ管理
 */
class Master_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * affiliationマスタ
     * @param $lang
     * @return array|bool
     */
    public function get_affiliation($lang = '')
    {
        if ($lang == 'ja') {
            $this->db->select('id,code,name_ja AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_affiliation');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } elseif ($lang == 'en') {
            $this->db->select('id,code,name_en AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_affiliation');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } else {
            $this->db->order_by('disp_id', 'asc');
            $this->db->where('del_flg', 0);
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_affiliation');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        }
    }

    /**
     * facultyマスタ
     * @param $lang
     * @return array|bool
     */
    public function get_faculty($lang = '')
    {
        if ($lang == 'ja') {
            $this->db->select('id,code,name_ja AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_faculty');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } elseif ($lang == 'en') {
            $this->db->select('id,code,name_en AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_faculty');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } else {
            $this->db->order_by('disp_id', 'asc');
            $this->db->where('del_flg', 0);
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_faculty');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        }
    }

    /**
     * faculty単体取得
     * @param $code
     * @return array|bool
     */
    public function get_faculty_data($code)
    {

        $this->db->where('code', $code);
        $this->db->where('del_flg', 0);
        //$this->db->order_by('id', 'asc');
        $query_a = $this->db->get('m_faculty');
        $result_a = $query_a->row_array();
        if ($result_a == array()) {
            return false;
        } else {
            return $result_a;
        }
    }

    /**
     * affiliation単体取得
     * @param $code
     * @return array|bool
     */
    public function get_affiliation_category_data($code)
    {

        $this->db->where('id', $code);
        $this->db->where('del_flg', 0);
        //$this->db->order_by('id', 'asc');
        $query_a = $this->db->get('m_affiliation');
        $result_a = $query_a->row_array();
        if ($result_a == array()) {
            return false;
        } else {
            return $result_a;
        }
    }

    /**
     * カテゴリマスタ
     * @param $lang
     * @return array|bool
     */
    public function get_category($lang = '')
    {
        if ($lang == 'ja') {
            $this->db->select('id,code,name_ja AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } elseif ($lang == 'en') {
            $this->db->select('id,code,name_en AS name');
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        } else {
            $this->db->where('del_flg', 0);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->result_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a;
            }
        }
    }

    /**
     * カテゴリマスタ（詳細）
     * @param $lang
     * @param $id
     * @return bool
     */
    public function get_category_detail($lang = 'ja', $id)
    {
        if ($lang == 'ja') {
            $this->db->select('id,code,name_ja AS name');
            $this->db->where('del_flg', 0);
            $this->db->where('id', $id);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->row_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a['name'];
            }
        } elseif ($lang == 'en') {
            $this->db->select('id,code,name_en AS name');
            $this->db->where('del_flg', 0);
            $this->db->where('id', $id);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->row_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a['name'];
            }
        } else {
            $this->db->select('id,code,name_ja,name_en');
            $this->db->where('del_flg', 0);
            $this->db->where('id', $id);
            $this->db->order_by('disp_id', 'asc');
            //$this->db->order_by('id', 'asc');
            $query_a = $this->db->get('m_category');
            $result_a = $query_a->row_array();
            if ($result_a == array()) {
                return false;
            } else {
                return $result_a['name_ja'] . '/' . $result_a['name_en'];
            }
        }
    }

    /**
     * マスタ登録
     * @param $form_dat
     * @return array|bool
     */
    public function entry($form_dat)
    {
        if ($form_dat['type'] == 'class') {
            $table = 'm_affiliation';
        } elseif ($form_dat['type'] == 'section') {
            $table = 'm_faculty';
        } elseif ($form_dat['type'] == 'category') {
            $table = 'm_category';

        } else {
            $table = 'm_affiliation';
        }
        $dat['code'] = $form_dat['code'];
        $dat['name_ja'] = $form_dat['name_ja'];
        $dat['name_en'] = $form_dat['name_en'];
        $dat['disp_id'] = $form_dat['disp_id'];
        if ($this->db->insert($table, $dat)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * マスタ変更
     * @param $form_dat
     * @return array|bool
     */
    public function change($form_dat)
    {
        if ($form_dat['type'] == 'class') {
            $table = 'm_affiliation';
        } elseif ($form_dat['type'] == 'section') {
            $table = 'm_faculty';
        } elseif ($form_dat['type'] == 'category') {
            $table = 'm_category';

        } else {
            $table = 'm_affiliation';
        }
        $dat['code'] = $form_dat['code'];
        $dat['name_ja'] = $form_dat['name_ja'];
        $dat['name_en'] = $form_dat['name_en'];
        $dat['disp_id'] = $form_dat['disp_id'];
        $this->db->where($table . '.id', $form_dat['id']);
        if ($this->db->update($table, $dat)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * マスタ削除
     * @param $form_dat
     * @return array|bool
     */
    public function list_delete($form_dat)
    {
        if ($form_dat['type'] == 'class') {
            $table = 'm_affiliation';
        } elseif ($form_dat['type'] == 'section') {
            $table = 'm_faculty';
        } elseif ($form_dat['type'] == 'category') {
            $table = 'm_category';
        } else {
            $table = 'm_affiliation';
        }
        $dat['del_flg'] = 1;
        $this->db->where($table . '.id', $form_dat['id']);
        if ($this->db->update($table, $dat)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * マスタ公開設定変更
     * @param $form_dat
     * @return array|bool
     */
    public function list_public($form_dat)
    {
        if ($form_dat['type'] == 'class') {
            $table = 'm_affiliation';
        } elseif ($form_dat['type'] == 'section') {
            $table = 'm_faculty';
        } elseif ($form_dat['type'] == 'category') {
            $table = 'm_category';

        } else {
            $table = 'm_affiliation';
        }
        $dat['pub_flg'] = $form_dat['publish'];
        $this->db->where($table . '.id', $form_dat['id']);
        if ($this->db->update($table, $dat)) {
            return true;
        } else {
            return false;
        }
    }
}

/* End of file Master.php */
/* Location: ./application/models/master_model.php */