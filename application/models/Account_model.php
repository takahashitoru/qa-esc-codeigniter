<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * メアドチェック : OK
     * todo:すでに使われているか？の確認
     * @param $mail
     * @return bool
     */
    public function mail_chk($mail)
    {
        $this->db->where('login_id', $mail);
        $this->db->where('delete_flag', 0);
        $query = $this->db->get('m_account');
        $result = $query->result_array();
        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * url_name chk : OK
     * todo:すでに使われているか？の確認
     * @param $form_dat
     * @return bool
     */
    public function url_chk($form_dat)
    {
        $this->db->where('url_key', $form_dat['url_key']);
        if($form_dat['id']){
            $this->db->where('id !=', $form_dat['id']);
        }
        $this->db->where('delete_flag', 0);
        $query = $this->db->get('m_account');
        $result = $query->row_array();

        if ($result == array()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * アカウントデータ取得（全一覧） : OK
     * @return mixed
     */
    public function get_all_data()
    {
        $this->db->where('m_account.delete_flag', 0);
        $this->db->order_by('m_account.id', 'asc');
        $query = $this->db->get('m_account');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウントデータ取得（管理画面、検索）
     * @param $search_dat
     * @return bool
     */
    public function get_search_data($search_dat)
    {
        if ($search_dat['status'] != '') {
            $this->db->where('m_account.public_flag', $search_dat['status']);
        }

//        if ($search_dat['pref'] != '') {
//            $this->db->where('m_account.PREFECTURE_CODE', $search_dat['pref']);
//        }
        if ($search_dat['name'] != '') {
            $this->db->like('m_account.name', $search_dat['name']);
        }

        $this->db->where('m_account.delete_flag', 0);
        $this->db->order_by('m_account.regist_date', 'desc');
        $query = $this->db->get('m_account');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウントデータ取得（単独）
     * @param $id
     * @return bool
     */
    public function get_data($id)
    {
        $this->db->select('m_account.id,m_account.name,m_account.url_key,m_account.pref
        ,m_account.city,m_account.tel,m_account.email,m_account.login_id,m_account.login_pwd
        ,m_account.regist_date,m_account.update_date,m_account.public_flag,m_account. app_flag');
        //$this->db->join('m_account_info', 'm_account.id = m_account_info.account_id','right outer');
        $this->db->where('m_account.id', $id);
        $this->db->where('m_account.delete_flag', 0);
        //$this->db->where('m_account_info.delete_flag', 0);
        $query = $this->db->get('m_account');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウントデータ取得（単独）
     * @param $id
     * @return bool
     */
    public function _get_data($id)
    {
        $this->db->select('m_account.id,m_account.name,m_account.url_key,m_account.pref
        ,m_account.city,m_account.tel,m_account.email,m_account.login_id,m_account.login_pwd
        ,m_account.regist_date,m_account.update_date,m_account.public_flag,m_account_info.content
        ,m_account_info.comment,m_account_info.address,m_account_info.lat,m_account_info.lng');
        $this->db->join('m_account_info', 'm_account.id = m_account_info.account_id','right outer');
        $this->db->where('m_account.id', $id);
        $this->db->where('m_account.delete_flag', 0);
        $this->db->where('m_account_info.delete_flag', 0);
        $query = $this->db->get('m_account');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウントデータ詳細取得（単独）
     * @param $id
     * @return bool
     */
    public function get_info($id)
    {
        $this->db->select('*');
        $this->db->where('m_account_info.account_id', $id);
        $this->db->where('m_account_info.delete_flag', 0);
        $query = $this->db->get('m_account_info');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }


    /**
     * アカウント検索
     * @param $search_dat
     * @param int $limit
     * @param int $page
     * @return bool
     */
    public function search_data($search_dat, $id, $limit = 20, $page = 1)
    {
        //検索
        if (!empty($search_dat['club_id'])) {
            if ($search_dat['club_id'] != '') {
                $this->db->like('m_account.id', $search_dat['club_id']);
            }
        }
        if (!empty($search_dat['event_name'])) {
            if ($search_dat['event_name'] != '') {
                $this->db->like('m_account.name', $search_dat['event_name']);
            }
        }
        if (!empty($search_dat['club_name'])) {
            if ($search_dat['club_name'] != '') {
                $this->db->like('m_account.name', $search_dat['club_name']);
            }
        }
        if (!empty($search_dat['date'])) {
            if ($search_dat['date'] != '') {
                $this->db->like('m_account.date', $search_dat['date']);
            }
        }
        /*
        if ($search_dat != array()) {
            if (!empty($search_dat['club_id'])) {
                $this->db->where('m_club.id', $search_dat['club_id']);
            }
            if (!empty($search_dat['name'])) {
                $this->db->where('t_member.name', $search_dat['name']);
                $this->db->where('t_member.delete_flag', 0);
            }
            if (!empty($search_dat['status'])) {
                $this->db->where('t_entrance_reserve.public_flag', $search_dat['status']);
            }
            if (!empty($search_dat['email'])) {
                $this->db->where('t_entrance_reserve.email', $search_dat['email']);
            }
        }
        */
        $this->db->where('m_account.delete_flag', 0);

        if ($page == 1) {
            $this->db->limit($limit * $page);
        } else {
            $this->db->limit($limit, $limit * ($page - 1));
        }
        if (!empty($search_dat['sort'])) {
            if ($search_dat['sort'] != '') {
                if ($search_dat['sort'] == 1) {
                    $this->db->order_by('m_account.regist_date', 'asc');
                } elseif ($search_dat['sort'] == 0) {
                    $this->db->order_by('m_account.regist_date', 'desc');
                }
            }
        } else {
            $this->db->order_by('m_account.id', 'desc');
        }

        $query = $this->db->get('m_account');
        $result = $query->result_array();
        //pre_print_r($result);
        //pre_print_r($this->db->last_query());
        //exit;
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウント検索（総件数取得）
     * @param $search_dat
     * @return bool
     */
    public function search_data_count($search_dat)
    {
        $this->db->select('count(m_account.id) AS count');
        //$this->db->join('m_account_emp', 'm_account.id = m_account_emp.account_id');
        $this->db->join('m_account_info', 'm_account.id = m_account_info.account_id');
        //検索
        if (!empty($search_dat['club_id'])) {
            if ($search_dat['club_id'] != '') {
                $this->db->like('m_club.id', $search_dat['club_id']);
            }
        }
        if (!empty($search_dat['event_name'])) {
            if ($search_dat['event_name'] != '') {
                $this->db->like('m_account.name', $search_dat['event_name']);
            }
        }
        if (!empty($search_dat['club_name'])) {
            if ($search_dat['club_name'] != '') {
                $this->db->like('m_club.name', $search_dat['club_name']);
            }
        }
        if (!empty($search_dat['date'])) {
            if ($search_dat['date'] != '') {
                $this->db->like('m_account.date', $search_dat['date']);
            }
        }
        $this->db->where('m_account_info.delete_flag', 0);
        $this->db->where('m_account.delete_flag', 0);

        $query = $this->db->get('m_account');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * アカウントデータ登録/編集
     * @param $form_dat
     * @return mixed
     */
    public function entry_data($form_dat)
    {
        $this->db->trans_start();
        $dat['pref'] = $form_dat['pref'];
        $dat['city'] = $form_dat['city'];
        $dat['name'] = $form_dat['name'];
        $dat['tel'] = $form_dat['tel'];
        $dat['email'] = $form_dat['email'];

        //if($this->valid($form_dat['url_key'],'url_key')){
        //    $dat['url_key'] = $form_dat['url_key'];
        //}
        $dat['url_key'] = $form_dat['url_key'];
        $dat['login_id'] = $form_dat['login_id'];
        if (!empty($form_dat['app_flag'])) {//変更あれば更新
            $dat['app_flag'] = ($form_dat['app_flag']);
        }
        if (!empty($form_dat['login_pwd'])) {//変更あれば更新
            //$dat['login_pwd'] = sha1($form_dat['login_pwd']);//暗号化
            $dat['login_pwd'] = ($form_dat['login_pwd']);
        }

        if ($form_dat['rollback'] == base_url('admin/account/entry')) {//登録
            $dat['regist_date'] = date("Y-m-d H:i:s", time());
            $this->db->insert('m_account', $dat);
            $account_id = $this->db->insert_id();
            $result = $this->entry_info($account_id, $form_dat);//詳細情報
        } else {//編集
            $dat['update_date'] = date("Y-m-d H:i:s", time());
            $this->db->where('id', $form_dat['id']);
            $this->db->update('m_account', $dat);
            $account_id = $form_dat['id'];
            $result = $this->entry_info($account_id, $form_dat);//詳細情報
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 付加情報登録/編集
     * @param $account_id
     * @param $form_dat
     * @return bool
     */
    public function entry_info($account_id, $form_dat)
    {
        $chk = $this->chk_info($account_id, $form_dat);//存在するかチェック
        $dat['account_id'] = $account_id;
        $dat['comment'] = $form_dat['comment'];
        $dat['address'] = $form_dat['address'];

        $dat['regist_date'] = date("Y-m-d H:i:s", time());

        if ($chk == false) {//登録
            $dat['regist_date'] = date("Y-m-d H:i:s", time());
            $this->db->insert('m_account_info', $dat);

        } else {//編集
            $dat['update_date'] = date("Y-m-d H:i:s", time());
            $this->db->where('id', $form_dat['id']);
            $this->db->update('m_account_info', $dat);
        }
    }

    /**
     * 存在するかチェック
     * @param $account_id
     * @return bool
     */
    public function chk_info($account_id)
    {
        $this->db->select('*');
        $this->db->where('account_id', $account_id);
        $this->db->where('delete_flag', 0);
        $query = $this->db->get('m_account_info');
        $result = $query->row_array();
        if ($result == array()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * バリデーション
     * @param $data
     * @param $type
     * @return bool
     */
    public function valid($data, $type)
    {
        if($type == 'url_key'){
            $this->db->select('*');
            $this->db->where('url_key', $data);
            $this->db->where('delete_flag', 0);
            $query = $this->db->get('m_account');
            $result = $query->row_array();
            if ($result == array()) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }
    }
}