<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 公開用質問データ管理
 */
class Faq_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * 公開用質問一覧取得*
     * @param string $page
     * @param string $limit
     * @param string $type
     * @param array $search
     * @param string $category
     * @return bool
     */
    public function get_data($page = '', $limit = '', $type = 'list', $search = array(), $category = '')
    {
        $result_dat = array();
        if ($type == 'list') {//リスト取得
            if ($category != '') {//カテゴリー区分があれば
                $this->db->select('s_faq_hst.id');
                $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                if ($page == 1) {
                    $this->db->limit($limit * $page);
                } else {
                    $this->db->limit($limit, $limit * ($page - 1));
                }
                $this->db->group_by('s_faq_hst.id');
                $this->db->order_by('s_faq_hst.id', 'desc');
                $query = $this->db->get('s_faq_hst');
                $result = $query->result_array();
                $i = 0;
                foreach ($result as $v) {
                    $this->db->select('s_faq_hst.id,user_data.name,s_faq_hst.updated_at,s_faq_hst.question_category_id,s_faq_hst.answer_change_flg,
            s_faq_hst.question_title,s_faq_hst.question_body,s_faq_hst.question_body,s_faq_hst.answer_status_id,s_faq_hst.created_at,
            s_faq_hst.manage_id,s_faq_hst.re_question_id,');
                    $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                    $this->db->join('m_category', 'm_category.id = s_faq_hst.question_category_id', 'left');
                    if ($category == 'OT') {
                        $array = array(99, 0);
                        $this->db->where_in('s_faq_hst.question_category_id', $array);
                    } else {
                        $this->db->where('m_category.code', $category);
                    }
                    $this->db->where('s_faq_hst.del_flg', 0);
                    $this->db->where('s_faq_hst.id', $v['id']);
                    $query = $this->db->get('s_faq_hst');
                    $result_a = $query->row_array();
                    if ($result == array()) {
                        $result_dat[$i] = '';
                    } else {
                        $result_dat[$i] = $result_a;
                    }
                    ++$i;
                }
                return $result_dat;
            } else {
                $this->db->select('s_faq_hst.id');
                $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                if ($limit != 9999) {
                    if ($page == 1) {
                        $this->db->limit($limit * $page);
                    } else {
                        $this->db->limit($limit, $limit * ($page - 1));
                    }
                }
                $this->db->where('s_faq_hst.del_flg', 0);
                $this->db->group_by('s_faq_hst.id');
                $this->db->order_by('s_faq_hst.id', 'desc');
                $query = $this->db->get('s_faq_hst');
                $result = $query->result_array();
                $i = 0;
                foreach ($result as $v) {
                    $this->db->select('s_faq_hst.id,user_data.name,s_faq_hst.updated_at,s_faq_hst.question_category_id,s_faq_hst.manage_id,s_faq_hst.re_question_id,
            s_faq_hst.question_title,s_faq_hst.question_body,s_faq_hst.answer_status_id,s_faq_hst.created_at,s_faq_hst.answer_change_flg,s_answer_hst.answer_body');
                    $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                    $this->db->where('s_faq_hst.del_flg', 0);
                    $this->db->where('s_faq_hst.id', $v['id']);
                    if (!empty($search['s_date'])) {
                        if ($search['s_date'] != '') {
                            $this->db->where('s_faq_hst.created_at >=', $search['s_date']);
                        }
                    }
                    if (!empty($search['e_date'])) {
                        if ($search['e_date'] != '') {
                            $this->db->where('s_faq_hst.created_at <=', $search['e_date']);
                        }
                    }
                    $query = $this->db->get('s_faq_hst');
                    $result_a = $query->row_array();
                    if ($result == array()) {
                        $result_dat[$i] = '';
                    } else {
                        $result_dat[$i] = $result_a;
                    }
                    ++$i;
                }
                return $result_dat;
            }
        } elseif ($type == 'count') {//データ件数取得
            if ($category != '') {//カテゴリー区分があれば
                $this->db->select('COUNT(s_faq_hst.id) AS count');
                $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                $this->db->join('m_category', 'm_category.id = s_faq_hst.question_category_id', 'left');
                if ($category == 'OT') {
                    $array = array(99, 0);
                    $this->db->where_in('s_faq_hst.question_category_id', $array);
                } else {
                    $this->db->where('m_category.code', $category);
                }
                $this->db->where('s_faq_hst.del_flg', 0);
                $this->db->where('r_question_answer.answer_id', NULL);
                $query = $this->db->get('s_faq_hst');
                $result = $query->row_array();
                if ($result == array()) {
                    return false;
                } else {
                    return $result;
                }
            } else {
                $this->db->select('s_faq_hst.id');
                $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                $this->db->where('s_faq_hst.del_flg', 0);
                $this->db->group_by('s_faq_hst.id');
                $this->db->order_by('s_faq_hst.id', 'desc');
                $query = $this->db->get('s_faq_hst');
                $result = $query->result_array();
                $i = 0;
                $result_dat['count'] = 0;
                foreach ($result as $v) {
                    $this->db->select('s_faq_hst.id,user_data.name,s_faq_hst.updated_at,s_faq_hst.question_category_id,s_faq_hst.manage_id,s_faq_hst.re_question_id,
            s_faq_hst.question_title,s_faq_hst.question_body,s_faq_hst.answer_status_id,s_faq_hst.created_at,s_faq_hst.answer_change_flg,s_answer_hst.answer_body');
                    $this->db->join('r_question_answer', 's_faq_hst.id = r_question_answer.question_id', 'left');
                    $this->db->join('s_answer_hst', 's_answer_hst.question_id = r_question_answer.question_id', 'left');
                    $this->db->join('user_data', 'user_data.id = s_faq_hst.user_id');
                    $this->db->where('s_faq_hst.del_flg', 0);
                    $this->db->where('s_faq_hst.id', $v['id']);
                    $query = $this->db->get('s_faq_hst');
                    $result_a = $query->row_array();
                    if ($result_a == array()) {
                        //
                    } else {
                        ++$result_dat['count'];
                    }
                    ++$i;
                }
                return $result_dat;
            }
        }
    }
    /**
     * 質問取得取得（カテゴリごと）
     * @param string $category
     * @param string $front
     * @return bool
     */
    public function get_qa($category = '', $front = '')
    {
        $this->db->select('s_faq_hst.id,s_faq_hst.created_at,s_faq_hst.updated_at,s_faq_hst.question_title,
        s_faq_hst.question_body,s_faq_hst.file_url,s_faq_hst.file_name,s_faq_hst.lang_cd,s_faq_hst.type,,s_faq_hst.manage_id,
        s_faq_hst.question_category_id,s_faq_hst.pub_flg,s_faq_hst.answer_status_id,s_faq_hst.answer_body');
        //$this->db->join('r_question_answer', 'r_question_answer.question_id = s_faq_hst.id');
        //$this->db->join('s_answer_hst', 's_answer_hst.id = r_question_answer.answer_id');
        if ($category == 99) {
            $array = array(99, 0);
            $this->db->where_in('s_faq_hst.question_category_id', $array);
        } else {
            $this->db->where('s_faq_hst.question_category_id', $category);
        }

        if ($front == 'front') {
            $this->db->where('s_faq_hst.pub_flg', 1);
        }
        $this->db->where('s_faq_hst.del_flg', 0);
        $this->db->where_in('s_faq_hst.answer_status_id', array(2, 4));
        $this->db->order_by('s_faq_hst.id', 'asc');
        $query = $this->db->get('s_faq_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * 再質問取得
     * @param $q_id
     * @param string $front
     * @return bool
     */
    public function get_requestion($q_id, $front = '')
    {
        $this->db->select('s_faq_hst.id,s_faq_hst.created_at,s_faq_hst.updated_at,s_faq_hst.question_title,
        s_faq_hst.question_body,s_faq_hst.file_url,s_faq_hst.file_name,s_faq_hst.lang_cd,s_faq_hst.type,
        s_faq_hst.question_category_id,s_faq_hst.pub_flg,s_faq_hst.answer_body');
        $this->db->where('s_faq_hst.re_question_id', $q_id);
        $this->db->where('s_faq_hst.answer_status_id', 4);
        if ($front == 'front') {
            $this->db->where('s_faq_hst.pub_flg', 1);
        }
        $this->db->where('s_faq_hst.del_flg', 0);
        $this->db->order_by('s_faq_hst.id', 'asc');
        $query = $this->db->get('s_faq_hst');
        $result = $query->result_array();
        if ($result == array()) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * FAQ表示/非表示
     * @param $type
     * @param $id
     * @param $roll
     * @return bool
     */
    public function publish_data($type, $id, $roll)
    {
        if ($type == 'disp') {
            $type = 1;
        } else {
            $type = 0;
        }
        $dat['pub_flg'] = $type;
        $dat['updated_at'] = date("Y-m-d H:i:s", time());
        $this->db->where('s_faq_hst.id', $id);

        if ($this->db->update('s_faq_hst', $dat)) {
            return true;
        } else {
            return false;
        }
    }
}
