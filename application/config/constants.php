<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*以下追加*/
define('LOGO_ALT', '東京大学環境安全研究センター');//ロゴ alt
define('SITE_NAME', '東京大学環境安全研究センターQAシステム');//サイト名
define('COPYRIGHT', '©Environmental Science Center, The University of Tokyo');//コピーライト

//define('ADMIN_MAIL', 'qa-noreply@esc.u-tokyo.ac.jp');//管理メールアドレス todo:変更してください
define('ADMIN_MAIL', 'takahashitoru@mac.com');//管理メールアドレス todo:変更してください
//define('REPLY_MAIL', 'office@esc.u-tokyo.ac.jp');//フッター記載メールアドレス
define('REPLY_MAIL', 'takahashitoru@mac.com');//フッター記載メールアドレス

define('MAIL_TITLE_Q', '【東大ESC/ESC UTokyo】ご質問を受け付けました');//質問メールタイトル
define('MAIL_TITLE_Q_EN', '【東大ESC/ESC UTokyo】Question reception completed');//質問メールタイトル
define('MAIL_TITLE_Q_ADMIN', '【東大ESC/ESC UTokyo】ご質問を受け付けました');//質問メールタイトル//管理者向け
define('MAIL_TITLE_A', '【東大ESC/ESC UTokyo】回答がありました');//回答メールタイトル
define('MAIL_TITLE_A_EN', '【東大ESC/ESC UTokyo】Answer completed');//回答メールタイトル
define('MAIL_TITLE_A_ADMIN', '【東大ESC/ESC UTokyo】回答がありました');//回答メールタイトル//管理者向け
define('MAIL_TITLE_N', '【東大ESC/ESC UTokyo】質問の登録がありました');//通知メールタイトル

//define('MAIL_TITLE_A_CNG', '【東大ESC/ESC UTokyo】回答者の指定変更がありました');//回答者指定変更メールタイトル
define('MAIL_TITLE_A_CNG', '【東大ESC/ESC UTokyo】回答してください (Q&A回答依頼)　ID: ');//回答者指定変更メールタイトル
define('MAIL_TITLE_A_ENTRY', '【東大ESC/ESC UTokyo】回答者の登録がありました');//回答者登録メールタイトル
define('MAIL_TITLE_A_ENTRY_ADMIN', '【東大ESC/ESC UTokyo】管理者の登録がありました');//管理者登録メールタイトル
define('MAIL_TITLE_A_EDIT', '【東大ESC/ESC UTokyo】回答者の情報変更がありました');//回答者情報変更メールタイトル
define('MAIL_TITLE_A_EDIT_ADMIN', '【東大ESC/ESC UTokyo】管理者の情報変更がありました');//管理者情報変更メールタイトル
define('MAIL_TITLE_A_DELETE', '【東大ESC/ESC UTokyo】回答者の削除をしました');//回答者削除メールタイトル
define('MAIL_TITLE_A_DELETE_ADMIN', '【東大ESC/ESC UTokyo】管理者の削除をしました');//管理者削除メールタイトル

define('MAIL_TITLE_REMINDER', '【東大ESC/ESC UTokyo】パスワード変更依頼受付');//リマインダーメールタイトル

define('ANGOU_KEY', 'XEsdZWIP');//暗号化キー
