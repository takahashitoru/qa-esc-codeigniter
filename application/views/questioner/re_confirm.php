<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/re_process?id=' . $this->input->get('id') . '&lang=' . $this->input->get('lang') . '&action=' . $this->input->get('action')) ?>"
                  class="col s12 m8" style="margin-right: auto;" method="post" enctype="multipart/form-data">
                <input name="lang" type="hidden" value="<?= $this->input->get('lang') ?>">
                <input name="type" type="hidden" value="<?= $this->input->get('action') ?>">
                <input name="edit" type="hidden" value="<?= $edit ?>">
                <input name="id" type="hidden" value="<?= $form_dat['id'] ?>">
                <input type="hidden" name="re_question_id" value="<?= $form_dat['re_question_id'] ?>">
                <input type="hidden" name="manage_id" value="<?= $form_dat['manage_id'] ?>">

                <div class="input-field">
                    <h5>再質問登録フォーム / Re-Question form
                    </h5>

                    <div class="input-field">
                        <input id="date" type="text" class="" name="date" value="<?= $answer_dat['created_at'] ?>"
                               readonly>
                        <label for="date" class="active">回答日時 / Date & time</label>
                    </div>
                    <?php
                    $answer = $this->answer_model->get_detail($answer_dat['answer_id']);
                    ?>
                    <div class="input-field">
                        <input id="answer" type="text" class="" name="answer" value="<?= $answer['name'] ?>"
                               readonly>
                        <label for="answer" class="active">回答者 / Respondent</label>
                    </div>
                    <input name="answer_email" type="hidden" value="<?= $answer['email'] ?>">

                    <div class="input-field">
                        <input id="name" type="text" class="" name="name" value="<?= $form_dat['name'] ?>" readonly>
                        <label for="name" class="active">お名前 / Name</label>
                    </div>

                    <?php
                    if ($this->input->get('action') == 'outside') {
                        ?>
                        <!--学外-->
                        <div class="input-field">
                            <input id="affiliation" type="text" name="affiliation"
                                   value="<?= $form_dat['affiliation'] ?>" readonly>
                            <label for="affiliation">ご所属 / Affiliation</label>
                        </div>
                        <div class="input-field input-radio">
                            <p>ご所属分類 / Affiliation category</p>
                            <?php
                            foreach ($affiliation as $cat) {
                                $chk = '';
                                if (!empty($form_dat['affiliation_category'])) {
                                    if ($form_dat['affiliation_category'] == $cat['id']) {
                                        $chk = "checked";
                                    }
                                }
                                ?>
                                <label>
                                    <input class="with-gap" name="affiliation_category" value="<?= $cat['id'] ?>"
                                           type="radio" <?= $chk ?> disabled/>
                                    <span><?= $cat['name'] ?></span>
                                </label>
                                <?php
                            }
                            ?>
                            <input name="affiliation_category" type="hidden"
                                   value="<?= $form_dat['affiliation_category'] ?>">
                        </div>
                        <?php
                    }
                    ?>

                    <div class="input-field">
                        <input id="tel" name="tel" type="tel" value="<?= $form_dat['tel'] ?>" readonly>
                        <label for="tel">電話番号(内線) / Phone(ext.)</label>
                    </div>
                    <div class="input-field">
                        <input id="email" type="email" name="email" value="<?= $form_dat['email'] ?>" readonly>
                        <label for="email" class="active">E-mail</label>
                    </div>

                    <?php
                    if ($this->input->get('action') == 'within') {
                        ?>
                        <!--学内-->
                        <!--
                        <div class="input-field" style="height: 60px;">
                            <p>部局 / Faculty・School</p>
                            <label>
                                <select class="browser-default" name="faculty_code"
                                        style="margin-top: 20px; margin-bottom: 50px;" >
                                    <option value="" disabled
                                            selected>選択してください / Please select</option>
                                    <?php
                        foreach ($faculty as $item) {
                            ?>
                                        <option value="<?= $item['code'] ?>"
                                            <?php
                            if (!empty($form_dat['faculty_code'])) {
                                if ($form_dat['faculty_code'] == $item['code']) {
                                    ?>
                                                    selected
                                                    <?php
                                }
                            }
                            ?>
                                        ><?= $item['name'] ?></option>
                                        <?php
                        }
                        ?>
                                </select>
                            </label>
                        </div>
                        -->
                        <div class="input-field">
                            <?php
                            foreach ($faculty as $item) {
                                if (!empty($form_dat['faculty_code'])) {
                                    if ($form_dat['faculty_code'] == $item['code']) {
                                        $faculty_code_name = $item['name_ja'] . ' / ' . $item['name_en'];
                                        ?>
                                        <input type="hidden" name="faculty_code" value="<?= $item['code'] ?>">
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <input id="department" name="faculty_code_name" type="text"
                                   value="<?= $faculty_code_name ?>" readonly>
                            <label for="department">部局 / Faculty・School</label>
                        </div>
                        <div class="input-field">
                            <input id="department" name="department" type="text" value="<?= $form_dat['department'] ?>"
                                   readonly>
                            <label for="department">専攻 / Department</label>
                        </div>
                        <div class="input-field">
                            <input id="laboratory" name="laboratory" type="text" value="<?= $form_dat['laboratory'] ?>"
                                   readonly>
                            <label for="laboratory">研究室 / Laboratory</label>
                        </div>
                        <div class="input-field">
                            <?php
                            foreach ($category as $item) {
                                if (!empty($form_dat['question_category_id'])) {
                                    if ($form_dat['question_category_id'] == $item['id']) {
                                        $question_category_name = $item['name_ja'] . ' / ' . $item['name_en'];
                                        ?>
                                        <input type="hidden" name="question_category_id" value="<?= $item['code'] ?>">
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <input id="department" name="question_category_id_name" type="text"
                                   value="<?= $question_category_name ?>" readonly>
                            <label for="department">質問カテゴリ / Question category</label>
                        </div>
                        <!--
                        <div class="input-field input-radio">
                            <p>質問カテゴリ / Question category</p>
                            <?php
                        foreach ($category as $item) {
                            ?>
                                <label>
                                    <input class="with-gap" name="question_category_id" type="radio"
                                           value="<?= $item['id'] ?>"
                                        <?php
                            if (!empty($form_dat['question_category_id'])) {
                                if ($form_dat['question_category_id'] == $item['id']) {
                                    ?>
                                                checked
                                                <?php
                                }
                            }
                            ?>readonly/>
                                    <span><?= $item['name'] ?></span>
                                </label>
                                <?php
                        }
                        ?>
                        </div>
                        -->
                        <?php
                    }
                    ?>
                    <!--
                    <div class="input-field" style="background-color: yellow">
                        <input id="requestion-title" type="text"  name="question_title" value="<?= $re_form_dat['question_title'] ?>" readonly>
                        <label for="requestion-title">再質問タイトル / Re-Question title</label>
                    </div>
                    -->

                    <!--初回-->
                    <div class="input-field">
                        <input id="question-title" type="text" name="question_title"
                               value="<?= $form_dat['question_title']; ?>" readonly>
                        <label for="question-title">質問タイトル / Question title</label>
                    </div>
                    <?php
                    $requestion = $this->question_model->get_base_q($q_id);
                    $i = 1;
                    foreach ($requestion as $req) {
                        if ($i % 2 === 0) {
                            $color = '#FFEEFF';
                        } else {
                            $color = '#EEFFFF';
                        } ?>
                        <div style="padding:  20px; background-color: <?= $color ?>">
                            <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $req['question_body'] ?></textarea>
                                <label for="question-contents">質問内容 / Question</label>
                            </div>
                            <div class="file-field input-field btn-highlight btn-alignleft">
                                <?php
                                $file = $this->file_model->get_file('question', $req['id']);
                                if (!empty($file)) {
                                    ?>
                                    <div class="input-field">
                                        <?php
                                        foreach ($file as $val) {
                                            ?>
                                            <a href="<?= $val['file_url'] ?>" download>
                                                <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                     style="width: 50px"
                                                     alt=""/><?= $this->lang->line('attachment_file') ?>
                                                （<?= $val['file_name'] ?>）</a><br/><br/>
                                            <?php
                                        }
                                        ?>
                                        <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            $ans = $this->answer_model->get_answer_detail($req['id']);
                            if (!empty($ans)) {
                                ?>
                                <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $ans[0]['answer_body'] ?></textarea>
                                    <label for="question-contents">回答 / Answer</label>
                                </div>
                                <div class="file-field input-field btn-highlight btn-alignleft">

                                    <?php
                                    $file = $this->file_model->get_file('answer', $ans[0]['id']);
                                    if (!empty($file)) {
                                        ?>
                                        <div class="input-field">
                                            <?php
                                            foreach ($file as $val) {
                                                ?>
                                                <a href="<?= $val['file_url'] ?>" download>
                                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                         style="width: 50px"
                                                         alt=""/><?= $this->lang->line('attachment_file') ?>
                                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                                <?php
                                            }
                                            ?>
                                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        ++$i;
                    }
                    ?>

                    <div class="input-field input-textarea with_file" style="background-color: yellow">
                        <textarea id="requestion-contents" name="question_body" maxlength="2000"
                                  readonly><?= $re_form_dat['question_body'] ?></textarea>
                        <label for="requestion-contents">再質問内容 / Re - Question</label>

                    </div>
                    <?php
                    $file = $this->file_model->get_file('question', $_SESSION['tmp_key']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <input id="file_name" name="file_name" type="text" value="<?= $val['file_name'] ?>"
                                       readonly>
                                <?php
                            }
                            ?>
                            <label for="file_name">添付ファイル / Attachment file</label>
                            <span class="helper-text">※「戻る」をクリックした場合には、添付ファイルを再度選択する必要があります / If you click 'Back', you need to select the attached file again</span>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <a href="<?= base_url('question/re_question?id=' . $this->input->get('id') . '&lang=' . $this->input->get('lang') . '&action=' . $this->input->get('action')); ?>"
                       class="waves-effect waves-light btn-large btn_secondary">戻る / Back
                    </a>
                    </a>
                    <button class="waves-effect waves-light btn-large btn_primary"
                            type="submit">送信 / Send
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>