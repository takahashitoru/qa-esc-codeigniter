<style id="compiled-css" type="text/css">
    select, .ui-select-menu {
        float: left;
        margin-right: 10px;
    }

    .wrap span.ui-selectmenu-item-header,
    .wrap ul.ui-selectmenu-menu li a {
        text-decoration: underline !important;
    }

    .ui-selectmenu-button.ui-button {
        margin-top: 20px;
        background: #ffffff;
    }

    .ui-selectmenu-menu.ui-front.ui-selectmenu-open {
        overflow-x: hidden;
        overflow-y: auto;
        height: 350px;
        width: 600px;
    }

    @media only screen and (max-width: 600px) {
        .ui-selectmenu-menu.ui-front.ui-selectmenu-open {
            overflow-x: hidden;
            overflow-y: auto;
            height: 250px;
            width: 340px;
        }
    }
</style>
<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/re_confirm?id=' . $this->input->get('id') . '&action=' . $this->input->get('action')) ?>"
                  class="col s12 m8" style="margin-right: auto;" method="post" enctype="multipart/form-data">
                <input name="type" type="hidden" value="<?= $this->input->get('action') ?>">
                <input name="lang" type="hidden" value="<?= $this->input->get('lang') ?>">
                <input name="edit" type="hidden" value="<?= $edit ?>">
                <input name="id" type="hidden" value="<?= $form_dat['id'] ?>">
                <div class="input-field">
                    <h5>再質問登録フォーム / Re-Question form<span
                                class="required">※必須 / Required</span>
                    </h5>

                    <div class="input-field">
                        <input id="date" type="text" class="" name="date" value="<?= $answer_dat['created_at'] ?>"
                               readonly>
                        <label for="date" class="active">回答日時 / Date & time</label>
                    </div>
                    <?php
                    $answer = $this->answer_model->get_detail($answer_dat['answer_id']);
                    ?>
                    <div class="input-field">
                        <input id="answer" type="text" class="" name="answer" value="<?= $answer['name'] ?>"
                               readonly>
                        <label for="answer" class="active">回答者 / Respondent</label>
                    </div>
                    <input name="answer_email" type="hidden" value="<?= $answer['email'] ?>">


                    <div class="input-field">
                        <input id="name" type="text" class="" name="name" value="<?= $form_dat['name'] ?>" readonly>
                        <label for="name" class="active">お名前 / Name</label>
                    </div>

                    <?php
                    if ($this->input->get('action') == 'outside') {
                        ?>
                        <!--学外-->
                        <div class="input-field">
                            <input id="affiliation" type="text" name="affiliation"
                                   value="<?= $form_dat['affiliation'] ?>" readonly>
                            <label for="affiliation">ご所属 / Affiliation</label>
                        </div>
                        <div class="input-field input-radio">
                            <p>ご所属分類 / Affiliation category</p>
                            <?php
                            foreach ($affiliation as $cat) {
                                $chk = '';
                                if (!empty($form_dat['affiliation_category'])) {
                                    if ($form_dat['affiliation_category'] == $cat['id']) {
                                        $chk = "checked";
                                    }
                                }
                                ?>
                                <label>
                                    <input class="with-gap" name="affiliation_category" value="<?= $cat['id'] ?>"
                                           type="radio" <?= $chk ?> disabled/>
                                    <span><?= $cat['name'] ?></span>
                                </label>
                                <?php
                            }
                            ?>
                            <input name="affiliation_category" type="hidden"
                                   value="<?= $form_dat['affiliation_category'] ?>">
                        </div>
                        <?php
                    }
                    ?>

                    <div class="input-field">
                        <input id="tel" name="tel" type="tel" value="<?= $form_dat['tel'] ?>" readonly>
                        <label for="tel">電話番号(内線) / Phone(ext.)</label>
                    </div>
                    <div class="input-field">
                        <input id="email" type="email" name="email" value="<?= $form_dat['email'] ?>" readonly>
                        <label for="email" class="active">E-mail</label>
                    </div>

                    <?php
                    if ($this->input->get('action') == 'within') {
                        ?>
                        <!--学内-->
                        <!--
                        <div class="input-field" style="height: 60px;">
                            <p>部局 / Faculty・School</p>
                            <label>
                                <select class="browser-default" name="faculty_code"
                                        style="margin-top: 20px; margin-bottom: 50px;" >
                                    <option value="" disabled
                                            selected>選択してください / Please select</option>
                                    <?php
                        foreach ($faculty as $item) {
                            ?>
                                        <option value="<?= $item['code'] ?>"
                                            <?php
                            if (!empty($form_dat['faculty_code'])) {
                                if ($form_dat['faculty_code'] == $item['code']) {
                                    ?>
                                                    selected
                                                    <?php
                                }
                            }
                            ?>
                                        ><?= $item['name'] ?></option>
                                        <?php
                        }
                        ?>
                                </select>
                            </label>
                        </div>
                        -->
                        <div class="input-field">
                            <?php
                            foreach ($faculty as $item) {
                                if (!empty($form_dat['faculty_code'])) {
                                    if ($form_dat['faculty_code'] == $item['code']) {
                                        $faculty_code_name = $item['name_ja'] . ' / ' . $item['name_en'];
                                        ?>
                                        <input type="hidden" name="faculty_code" value="<?= $item['code'] ?>">
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <input id="department" name="faculty_code_name" type="text"
                                   value="<?= $faculty_code_name ?>" readonly>
                            <label for="department">部局 / Faculty・School</label>
                        </div>
                        <div class="input-field">
                            <input id="department" name="department" type="text" value="<?= $form_dat['department'] ?>"
                                   readonly>
                            <label for="department">専攻 / Department</label>
                        </div>
                        <div class="input-field">
                            <input id="laboratory" name="laboratory" type="text" value="<?= $form_dat['laboratory'] ?>"
                                   readonly>
                            <label for="laboratory">研究室 / Laboratory</label>
                        </div>

                        <div class="input-field input-radio">
                            <p>質問カテゴリ / Question category</p>
                            <label>
                                <select class="browser-default" name="question_category_id" id="select_category"
                                        style="margin-top: 20px; margin-bottom: 50px; width: 100%">
                                    <option value="" disabled
                                            selected>選択してください / Please select
                                    </option>
                                    <?php
                                    //pre_print_r($category);
                                    foreach ($category as $item) {
                                        ?>
                                        <option value="<?= $item['id'] ?>"
                                            <?php
                                            if (!empty($form_dat['question_category_id'])) {
                                                if ($form_dat['question_category_id'] == $item['id']) {
                                                    ?>
                                                    selected
                                                    <?php
                                                }
                                            }
                                            ?>
                                        ><?= $item['name_ja'] ?> / <?= $item['name_en'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                        <?php
                    }
                    ?>
                    <!--
                    <div class="input-field" style="background-color: yellow">
                        <input id="requestion-title" type="text" class="validate" name="question_title" value="<?php
                    if (!empty($re_form_dat['question_title'])) {
                        echo $re_form_dat['question_title'];
                    } ?>" maxlength="80">
                        <label for="requestion-title">再質問タイトル / Re-Question title<span
                                    class="required">※</span></label>
                        <?php
                    if (isset($_SESSION['er_msg_question_title'])) {
                        ?>

                            <span class="helper-text">必須項目(※は入力必須です)</span>
                            <?php
                    }
                    ?>
                    </div>
                    -->
                    <?php
                    //pre_print_r($_SESSION)
                    ?>

                    <!--初回-->
                    <div class="input-field">
                        <input id="question-title" type="text" name="question_title"
                               value="<?= $form_dat['question_title']; ?>" readonly>
                        <label for="question-title">質問タイトル / Question title</label>
                    </div>


                    <?php
                    $requestion = $this->question_model->get_base_q($q_id);
                    $i = 1;
                    foreach ($requestion as $req) {
                        if ($i % 2 === 0) {
                            $color = '#FFEEFF';
                        } else {
                            $color = '#EEFFFF';
                        } ?>
                        <div style="padding:  20px; background-color: <?= $color ?>">
                            <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $req['question_body'] ?></textarea>
                                <label for="question-contents">質問内容 / Question</label>
                            </div>
                            <div class="file-field input-field btn-highlight btn-alignleft">
                                <?php
                                $file = $this->file_model->get_file('question', $req['id']);
                                if (!empty($file)) {
                                    ?>
                                    <div class="input-field">
                                        <?php
                                        foreach ($file as $val) {
                                            ?>
                                            <a href="<?= $val['file_url'] ?>" download>
                                                <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>" style="width: 50px"
                                                     alt=""/><?= $this->lang->line('attachment_file') ?>
                                                （<?= $val['file_name'] ?>）</a><br/><br/>
                                            <?php
                                        }
                                        ?>
                                        <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            $ans = $this->answer_model->get_answer_detail($req['id']);
                            if (!empty($ans)) {
                                ?>
                                <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $ans[0]['answer_body'] ?></textarea>
                                    <label for="question-contents">回答 / Answer</label>
                                </div>
                                <div class="file-field input-field btn-highlight btn-alignleft">
                                    <?php
                                    $file = $this->file_model->get_file('answer', $ans[0]['id']);
                                    if (!empty($file)) {
                                        ?>
                                        <div class="input-field">
                                            <?php
                                            foreach ($file as $val) {
                                                ?>
                                                <a href="<?= $val['file_url'] ?>" download>
                                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                         style="width: 50px"
                                                         alt=""/><?= $this->lang->line('attachment_file') ?>
                                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                                <?php
                                            }
                                            ?>
                                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        ++$i;
                    }
                    ?>

                    <div class="input-field input-textarea with_file" style="background-color: yellow">
                        <textarea id="requestion-contents" name="question_body" maxlength="2000"><?php
                            if (!empty($re_form_dat['question_body'])) {
                                echo $re_form_dat['question_body'];
                            } ?></textarea>
                        <label for="requestion-contents">再質問内容 / Re - Question<span
                                    class="required">※</span></label>
                        <?php
                        if (isset($_SESSION['er_msg_question_body'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">必須項目(※は入力必須です)</span>
                            <?php
                        }
                        ?>
                    </div>
                    <!--
                    <div class="file-field input-field btn-highlight btn-alignleft">
                        <div class="btn">
                            <label style="color:#fff;font-size: medium;">こちらをクリックで添付ファイルダウンロード / Click here to download attached file
                                <input type="file" name="userfile" id="upfile" style="display:none" onchange="$('#filename').html($(this).prop('files')[0].name)">
                            </label>
                        </div>
                        <span class="btn-comment" id="filename">ファイル未選択 / File not selected</span>
                        <?php
                    //ファイルエラー
                    if (isset($_SESSION['file'])) {
                        ?>
                            <span class="helper-text"><?= $_SESSION['file'] ?></span>
                            <?php
                    }
                    ?>

                    </div>
                -->
                    <ul id="filelist">
                        <strong>添付ファイルアップロード（PDFのみ）/ Upload attached file (PDF only)</strong>
                        <li><input type="file" name="userfile[]" class="fileinput"
                                   multiple/></li>
                        <?php
                        //ファイルエラー
                        if (isset($_SESSION['file'])) {
                            ?>
                            <span class="helper-text"><?= $_SESSION['file'] ?></span>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
                //pre_print_r($form_dat)?>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <a href="<?= base_url('question/answer') . '?lang=' . $form_dat['lang_cd'] . '&id=' . $form_dat['id'] ?>"
                       class="waves-effect waves-light btn-large btn_secondary">戻る / Back
                    </a>
                    <button class="waves-effect waves-light btn-large btn_primary"
                            type="submit">確認 / Confirm
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
<script>
    jQuery(function () {
        $(".fileinput").change(fileInputChange);
    });

    function fileInputChange() {
        if ($(".fileinput").last().val() != "") {
            $("#filelist").append('<input type="file" multiple name="userfile[]" class="fileinput" />')
                .bind('change', fileInputChange);
        }
    }

</script>
<script type="text/javascript">
    jQuery(function () {
        jQuery("#select_category").selectmenu({
            style: 'popup',
            width: 340,
            format: listFormatting
        });
    });

    var listFormatting = function (text) {
        var newText = text;
        //array of find replaces
        var findreps = [
            {find: /^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find: /([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find: /([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
            {find: /([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find: /(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
        ];

        for (var i in findreps) {
            newText = newText.replace(findreps[i].find, findreps[i].rep);
        }
        return newText;
    }
</script>
