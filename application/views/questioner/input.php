<style id="compiled-css" type="text/css">
    select,.ui-select-menu { float: left; margin-right: 10px; }
    .wrap span.ui-selectmenu-item-header,
    .wrap ul.ui-selectmenu-menu li a { text-decoration: underline !important; }
    .ui-selectmenu-button.ui-button { margin-top: 20px; background: #ffffff; }
    .ui-selectmenu-menu.ui-front.ui-selectmenu-open { overflow-x: hidden; overflow-y: auto; height: 350px; width: 600px; }

    @media only screen and (max-width: 600px) {
        .ui-selectmenu-menu.ui-front.ui-selectmenu-open { overflow-x: hidden; overflow-y: auto; height: 250px; width: 340px; }
    }
</style>
<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>

    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/confirm') ?>" class="col s12 m8" style="margin-right: auto;"
                  method="post" enctype="multipart/form-data">
                <input name="type" type="hidden" value="<?= $action ?>">
                <input name="lang" type="hidden" value="">
                <input name="secret_id" type="hidden" value="<?= $this->input->get('id') ?>">
                <input name="edit" type="hidden" value="<?= $edit ?>">
                <input name="file_url" type="hidden" value="<?php
                if (isset($_SESSION['form_dat']['file_url'])) {
                    echo $_SESSION['form_dat']['file_url'];
                } ?>">
                <input name="id" type="hidden" value="<?php
                if (!empty($form_dat['id'])) {
                    echo $form_dat['id'];
                } ?>">
                <div class="input-field">
                    <h5><?php
                        //pre_print_r($form_dat);
                        if ($edit == true) {
                            echo '質問編集フォーム / Question edit form';
                        } else {
                            echo '質問登録フォーム / Question form';
                        } ?><span
                                class="required">※必須 / Required</span></h5>
                    <div class="input-field"><!--エラー時は.input-fieldに.errorを付与-->
                        <input id="name" type="text" class="validate" name="name" value="<?php
                        if (!empty($form_dat['name'])) {
                            echo $form_dat['name'];
                        } ?>" maxlength="40"><!--エラー時はinputにvalueを付与-->
                        <label for="name" <?php
                        if (isset($_SESSION['er_msg_name'])) {
                            ?> class="active" <?php
                        }
                        ?>>お名前 / Name<span class="required">※</span></label>
                        <?php
                        if (isset($_SESSION['er_msg_name'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                            <?php
                        }
                        ?>
                    </div>

                    <?php
                    if ($action == 'outside') {
                        ?>
                        <!--学外-->
                        <div class="input-field">
                            <input id="affiliation" type="text" class="validate" name="affiliation" value="<?php
                            if (!empty($form_dat['affiliation'])) {
                                echo $form_dat['affiliation'];
                            } ?>">
                            <label for="affiliation">ご所属 / Affiliation<span
                                        class="required">※</span></label>
                            <?php
                            if (isset($_SESSION['er_msg_affiliation'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="input-field input-radio">
                            <p>ご所属分類 / Affiliation category<span class="required">※</span></p>
                            <?php
                            foreach ($affiliation as $cat) {
                                $chk = '';
                                if (!empty($form_dat['affiliation_category'])) {
                                    if ($form_dat['affiliation_category'] == $cat['id']) {
                                        $chk = "checked";
                                    }
                                }
                                ?>
                                <label>
                                    <input class="with-gap" name="affiliation_category" value="<?= $cat['id'] ?>"
                                           type="radio" <?= $chk ?>/>
                                    <span><?= $cat['name_ja'] ?> / <?= $cat['name_en'] ?></span>
                                </label>
                                <?php
                            }
                            if (isset($_SESSION['er_msg_affiliation_category'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="input-field">
                        <input id="tel" name="tel" type="tel" class="validate" value="<?php
                        if (!empty($form_dat['tel'])) {
                            echo $form_dat['tel'];
                        } ?>" maxlength="11">
                        <label for="tel">電話番号(内線) / Phone(ext.)</label>
                    </div>
                    <div class="input-field">
                        <input id="email" type="email" class="validate" name="email" value="<?php
                        if (!empty($form_dat['email'])) {
                            echo $form_dat['email'];
                        } ?>" maxlength="40">
                        <label for="email" <?php
                        if (isset($_SESSION['er_msg_email'])) {
                            ?>
                            class="active"
                            <?php
                        }
                        ?>>E-mail<span class="required">※</span></label>
                        <?php
                        if (isset($_SESSION['er_msg_email'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                            <?php
                        }
                        ?>
                        <?php
                        if (isset($_SESSION['er_msg_email_check'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">確認用と一致しません / It does not match for confirmation</span>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($edit == true) {//編集
                        ?>
                        <!--メール確認なし-->
                        <?php
                    } else {//新規
                        ?>
                        <div class="input-field">
                            <input id="email2" type="email" class="validate" name="email_check" value="<?php
                            if (!empty($form_dat['email_check'])) {
                                echo $form_dat['email_check'];
                            } ?>" maxlength="40">
                            <label for="email2">確認用 / E-mail (confirmation)<span class="required">※</span></label>
                        </div>
                        <?php
                    } ?>

                    <?php
                    if ($action == 'within') {
                        ?>
                        <!--学内-->
                        <div class="input-field" style="height: 60px;">
                            <p>部局 / Faculty・School<span class="required">※</span></p>
                            <?php
                            if (isset($_SESSION['er_msg_faculty_school'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                            <label>
                                <select class="browser-default" name="faculty_code" id="select_faculty"
                                        style="margin-top: 20px; margin-bottom: 50px;">
                                    <option value="" disabled
                                            selected>選択してください / Please select
                                    </option>
                                    <?php
                                    foreach ($faculty as $item) {
                                        ?>
                                        <option value="<?= $item['code'] ?>"
                                            <?php
                                            if (!empty($form_dat['faculty_code'])) {
                                                if ($form_dat['faculty_code'] == $item['code']) {
                                                    ?>
                                                    selected
                                                    <?php
                                                }
                                            }
                                            ?>
                                        ><?= $item['name_ja'] ?> / <?= $item['name_en'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                        <div class="input-field">
                            <input id="department" name="department" type="text" class="validate" value="<?php
                            if (!empty($form_dat['department'])) {
                                echo $form_dat['department'];
                            } ?>">
                            <label for="department">専攻 / Department<span
                                        class="required">※</span></label>
                            <?php
                            if (isset($_SESSION['er_msg_department'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="input-field">
                            <input id="laboratory" name="laboratory" type="text" class="validate" value="<?php
                            if (!empty($form_dat['laboratory'])) {
                                echo $form_dat['laboratory'];
                            } ?>">
                            <label for="laboratory">研究室 / Laboratory<span
                                        class="required">※</span></label>
                            <?php
                            if (isset($_SESSION['er_msg_laboratory'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="input-field" style="height: 60px;">
                            <p>質問カテゴリ / Question category<span class="required">※</span></p>
                            <label>
                                <select class="browser-default" name="question_category_id" id="select_category"
                                        style="margin-top: 20px; margin-bottom: 50px; width: 100%">
                                    <option value="" disabled
                                            selected>選択してください / Please select
                                    </option>
                                    <?php
                                    //pre_print_r($category);
                                    foreach ($category as $item) {
                                        ?>
                                        <option value="<?= $item['id'] ?>"
                                            <?php
                                            if (!empty($form_dat['question_category_id'])) {
                                                if ($form_dat['question_category_id'] == $item['id']) {
                                                    ?>
                                                    selected
                                                    <?php
                                                }
                                            }
                                            ?>
                                        ><?= $item['name_ja'] ?> / <?= $item['name_en'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </label>
                            <?php
                            if (isset($_SESSION['er_msg_question_category_id'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="input-field">
                        <input id="question-title" type="text" class="validate" name="question_title" value="<?php
                        if (!empty($form_dat['question_title'])) {
                            echo $form_dat['question_title'];
                        } ?>" maxlength="80">
                        <label for="question-title">質問タイトル / Question title<span
                                    class="required">※</span></label>
                        <?php
                        if (isset($_SESSION['er_msg_question_title'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" name="question_body" maxlength="2000"><?php
                            if (!empty($form_dat['question_body'])) {
                                echo $form_dat['question_body'];
                            } ?></textarea>
                        <label for="question-contents">質問内容 / Question<span class="required">※</span></label>
                        <?php
                        if (isset($_SESSION['er_msg_question_body'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($edit == true) {
                    ?>
                    <div class="file-field input-field btn-highlight btn-alignleft">
                        <?php
                        $file = $this->file_model->get_file('question', $form_dat['id']);
                        if (!empty($file)) {
                            ?>
                            <div class="input-field">
                                <?php
                                foreach ($file as $val) {
                                    ?>
                                    <input id="file_name" name="file_name" type="text"
                                           value="<?= $val['file_name'] ?>"
                                           readonly>
                                    <?php
                                }
                                ?>
                                <label for="file_name">添付ファイル / Attachment file</label>
                                <span class="helper-text">※「戻る」をクリックした場合には、添付ファイルを再度選択する必要があります / If you click 'Back', you need to select the attached file again</span>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    }
                    ?>

                    <!--
                    <div class="file-field input-field btn-highlight btn-alignleft">
                        <div class="btn">
                            <label style="color:#fff;font-size: medium;" id="filelist">添付ファイル / Attachment file
                                <input type="file" name="userfile[]" id="upfile" style="display:none"value=""multiple
                                       onchange="$('#filename').html($(this).prop('files')[0].name)" class="fileinput">
                            </label>
                        </div>
                        <span class="btn-comment" id="filename">ファイル未選択 / File not selected</span>
                        <?php
                    //ファイルエラー
                    if (isset($_SESSION['file'])) {
                        ?>
                            <span class="helper-text"><?=$_SESSION['file']?></span>
                            <?php
                    }
                    ?>
                    </div>
                    -->
                    <ul id="filelist">
                        <strong>添付ファイルアップロード（PDFのみ）/ Upload attached file (PDF only)</strong>
                        <li><input type="file" name="userfile[]" class="fileinput"
                                   multiple/></li>
                        <?php
                        //ファイルエラー
                        if (isset($_SESSION['file'])) {
                            ?>
                            <span class="helper-text"><?=$_SESSION['file']?></span>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <?php
                    if ($edit == true) {//編集
                        ?>
                        <a href="<?= base_url() ?>" class="waves-effect waves-light btn-large btn_secondary">Topへ戻る /
                            Top page</a>
                        <?php
                    } else {//新規
                        ?>
                        <button class="waves-effect waves-light btn-large btn_secondary" name="prev" value="prev"
                                type="submit">戻る / Back
                        </button>
                        <?php
                    } ?>
                    <button class="waves-effect waves-light btn-large btn_primary"
                            type="submit">確認 / Confirm
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
<script>
    jQuery(function () {
        $(".fileinput").change(fileInputChange);
    });

    function fileInputChange() {
        if ($(".fileinput").last().val() != "") {
            $("#filelist").append('<li><input type="file" multiple name="userfile[]" class="fileinput" /></li>')
                .bind('change', fileInputChange);
        }
    }
</script>
<script type="text/javascript">
    jQuery(function(){
        jQuery("#select_faculty").selectmenu({
            style:'popup',
            width: 340,
            format: listFormatting
        });
    });
    jQuery(function(){
        jQuery("#select_category").selectmenu({
            style:'popup',
            width: 340,
            format: listFormatting
        });
    });

    var listFormatting = function(text){
        var newText = text;
        //array of find replaces
        var findreps = [
            {find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
            {find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
            {find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
        ];

        for(var i in findreps){
            newText = newText.replace(findreps[i].find, findreps[i].rep);
        }
        return newText;
    }
</script>
