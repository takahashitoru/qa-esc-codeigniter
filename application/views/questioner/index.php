<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <div class="input-field btn-wrapper btn-aligncenter btn-full mt50">
                <button onclick="location.href='<?= base_url('question/input/within') ?>'"
                        class="waves-effect waves-light btn-large btn_secondary" type="submit" name="action"
                        value="within">学内の方 / UTokyo member
                </button>
                <button onclick="location.href='<?= base_url('question/input/outside') ?>'"
                        class="waves-effect waves-light btn-large btn_primary" type="submit" name="action"
                        value="outside">学外の方 / Non UTokyo-member
                </button>
                <button onclick="location.href='<?= base_url('faq') ?>'"
                        class="waves-effect waves-light btn-large btn_secondary" type="submit" name="action"
                        value="outside">faq
                </button>
            </div>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
