<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/process') ?>" class="col s12 m8" style="margin-right: auto;"
                  method="post" enctype="multipart/form-data">
                <input name="type" type="hidden" value="<?= $action ?>">
                <input name="lang" type="hidden" value="">
                <input name="img_delete" type="hidden" value="<?php
                if (!empty($form_dat['img_delete'])) {
                    echo $form_dat['img_delete'];
                } ?>">
                <input name="file_url" type="hidden" value="<?php
                if (!empty($form_dat['file_url'])) {
                    echo $form_dat['file_url'];
                } ?>">
                <input name="id" type="hidden" value="<?php
                if (!empty($form_dat['id'])) {
                    echo $form_dat['id'];
                } ?>">
                <div class="input-field">
                    <h5><?php
                        //pre_print_r($_SESSION);
                        if ($edit == true) {
                            echo '質問編集フォーム / Question edit form';
                        } else {
                            echo '質問登録フォーム / Question form';
                        } ?></h5>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $form_dat['name'] ?>" type="text" readonly>
                        <label for="name">お名前 / Name</label>
                    </div>

                    <?php
                    if ($action == 'outside') {
                        ?>
                        <!--学外-->
                        <div class="input-field">
                            <input id="affiliation" name="affiliation" value="<?php
                            if (!empty($form_dat['affiliation'])) {
                                echo $form_dat['affiliation'];
                            } ?>" type="text" readonly>
                            <label for="affiliation">ご所属 / Affiliation</label>
                        </div>
                        <div class="input-field">
                            <?php
                            $affiliation_name = '';
                            $affiliation_code = '';
                            foreach ($affiliation as $cat) {
                                if (!empty($form_dat['affiliation_category'])) {
                                    if ($form_dat['affiliation_category'] == $cat['id']) {
                                        $affiliation_name = $cat['name_ja'] . ' / ' . $cat['name_en'];
                                        $affiliation_code = $cat['id'];
                                    }
                                }
                            } ?>
                            <input id="faculty_school" type="text" value="<?= $affiliation_name ?>" readonly>
                            <input type="hidden" name="faculty_school" value="<?= $affiliation_code ?>">
                            <label for="faculty_school">ご所属分類 / Affiliation category</label>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="input-field">
                        <input id="tel" name="tel" type="tel" value="<?php
                        if (!empty($form_dat['tel'])) {
                            echo $form_dat['tel'];
                        } ?>" readonly>
                        <label for="tel">電話番号(内線) / Phone(ext.)</label>
                    </div>
                    <div class="input-field">
                        <input id="email" name="email" type="email" value="<?= $form_dat['email'] ?>" readonly>
                        <label for="email">E-mail</label>
                    </div>

                    <?php
                    if ($action == 'within') {
                        ?>
                        <!--学内-->
                        <div class="input-field">
                            <?php
                            $faculty_name = '';
                            $faculty_code = '';

                            foreach ($faculty as $item) {
                                if (!empty($form_dat['faculty_code'])) {
                                    if ($form_dat['faculty_code'] == $item['code']) {
                                        $faculty_name = $item['name_ja'] . ' / ' . $item['name_en'];
                                        $faculty_code = $item['code'];
                                    }
                                }
                            } ?>
                            <input id="faculty_school" type="text" value="<?= $faculty_name ?>" readonly>
                            <input type="hidden" name="question_category_id" value="<?= $faculty_code ?>">
                            <label for="faculty_school">部局 / Faculty・School</label>
                        </div>
                        <div class="input-field">
                            <input id="department" name="department" type="text" class="validate" value="<?php
                            if (!empty($form_dat['department'])) {
                                echo $form_dat['department'];
                            } ?>">
                            <label for="department">専攻 / Department</label>
                            <?php
                            if (isset($_SESSION['er_msg_department'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="input-field">
                            <input id="laboratory" name="laboratory" type="text" class="validate" value="<?php
                            if (!empty($form_dat['laboratory'])) {
                                echo $form_dat['laboratory'];
                            } ?>">
                            <label for="laboratory">研究室 / Laboratory</label>
                            <?php
                            if (isset($_SESSION['er_msg_laboratory'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text">必須項目(※は入力必須です) / Compulsory field</span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="input-field">
                            <?php
                            $category_name = '';
                            $category_code = '';
                            foreach ($category as $item) {
                                if (!empty($form_dat['question_category_id'])) {
                                    if ($form_dat['question_category_id'] == $item['id']) {
                                        $category_name = $item['name_ja'] . ' / ' . $item['name_en'];
                                        $category_code = $item['id'];
                                    }
                                }
                            } ?>
                            <input id="faculty_school" type="text" value="<?= $category_name ?>" readonly>
                            <input type="hidden" name="question_category_id" value="<?= $category_code ?>">
                            <label for="faculty_school">質問カテゴリ / Question category</label>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="input-field">
                        <input id="question-title" name="question_title" type="text"
                               value="<?= $form_dat['question_title'] ?>" readonly>
                        <label for="question-title">質問タイトル / Question title</label>
                    </div>
                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" name="question_body"
                                  readonly><?= $form_dat['question_body'] ?></textarea>
                        <label for="question-contents">質問内容 / Question</label>
                    </div>
                    <?php
                    $file = $this->file_model->get_file('question', $_SESSION['tmp_key']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <input id="file_name" name="file_name" type="text" value="<?= $val['file_name'] ?>"
                                       readonly>
                                <?php
                            }
                            ?>
                            <label for="file_name">添付ファイル / Attachment file</label>
                            <span class="helper-text">※「戻る」をクリックした場合には、添付ファイルを再度選択する必要があります / If you click 'Back', you need to select the attached file again</span>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <?php
                    if ($edit == true) {//編集
                        ?>
                        <a href="<?= $_SERVER['HTTP_REFERER']; ?>"
                           class="waves-effect waves-light btn-large btn_secondary">戻る / Prev
                        </a>
                        <?php
                    } else {//新規
                        ?>
                        <a href="javascript:history.back()" class="waves-effect waves-light btn-large btn_secondary">TOPへ戻る
                            / Top page</a>
                        <?php
                    } ?>
                    <button class="waves-effect waves-light btn-large btn_primary"
                            type="submit">送信 / Send
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>