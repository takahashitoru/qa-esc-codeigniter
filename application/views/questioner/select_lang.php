<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?=base_url('question')?>" class="col s12 m8" style="margin-right: auto;">
                <div class="input-field btn-wrapper btn-aligncenter btn-full mt50">
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="lang" value="ja">日本語</button>
                    <button class="waves-effect waves-light btn-large btn_secondary" type="submit" name="lang" value="en">English</button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?=COPYRIGHT?></div>
    </div>
</div>

<script>
    var ws = new WebSocket('ws://echo.websocket.org');
    // WebsocketClient.readyState が OPEN になったときに呼ばれるコールバック
    ws.onopen = function () {
        console.log("Connecting is success!!");
        ws.send("Hello world!");
    };
    // サーバからメッセージを受信したときに呼ばれるコールバック
    // このコールバックの第一引数に messageEvent というオブジェクトが格納されている（...らしい）
    // このオブジェクトの data 属性に受け取ったメッセージが格納されている.
    ws.onmessage = function (me) {
        var recievedData = me.data;
        console.log("Message is exists!!");
        console.log("recievedData: " + recievedData);
    };
</script>