<body class="confirm">
<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <div class="input-field">

                <h5>ご質問を受け付けました。</h5>
                <h5>Your question has been submitted .</h5>

                <div class="input-field">
                    <p>確認のメールが登録されたメールアドレスに送信されます。</p><p>回答がされますと、メールで通知されます。</p><p>いましばらくお待ちください。</p>
                </div>
                <div class="input-field">
                    <p>A confirmation email will be sent to the entered address . </p><p> Notification email will be sent once an answer is made .</p><p> Please wait a while.</p>
                </div>

            </div>

        </div>
        <div class="contents-footer"><?=COPYRIGHT?></div>
    </div>

</div>