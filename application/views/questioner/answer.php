<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/answer_complete') ?>" class="col s12 m8" style="margin-right: auto;"
                  method="post">
                <input type="hidden" name="q_id" value="<?= base64_encode($result_dat['id']) ?>">
                <div class="input-field">
                    <h5>回答表示 / Answer</h5>
                    <div class="input-field">
                        <input id="name" disabled value="<?= $result_dat['question_title'] ?>" type="text"
                               class="validate">
                        <label for="disabled">質問タイトル / Question title</label>
                    </div>

                    <?php
                    $q_id = $this->input->get('id');
                    $q_id = base64_decode($q_id);
                    $requestion = $this->question_model->get_base_q($q_id);
                    $i = 1;
                    foreach ($requestion as $req) {
                        if ($i % 2 === 0) {
                            $color = '#FFEEFF';
                        } else {
                            $color = '#EEFFFF';
                        }
                        //pre_print_r($req);
                        ?>
                        <div style="padding:  20px; background-color: <?= $color ?>">
                            <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $req['question_body'] ?></textarea>
                                <label for="question-contents">質問内容 / Question</label>
                            </div>
                            <div class="file-field input-field btn-highlight btn-alignleft">
                                <?php
                                $file = $this->file_model->get_file('question', $req['id']);
                                if (!empty($file)) {
                                    ?>
                                    <div class="input-field">
                                        <?php
                                        foreach ($file as $val) {
                                            ?>
                                            <a href="<?= $val['file_url'] ?>" download>
                                                <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>" style="width: 50px"
                                                     alt=""/><?= $this->lang->line('attachment_file') ?>
                                                （<?= $val['file_name'] ?>）</a><br/><br/>
                                            <?php
                                        }
                                        ?>
                                        <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            $ans = $this->answer_model->get_answer_detail($req['id']);
                            if (!empty($ans)) {
                                ?>
                                <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $ans[0]['answer_body'] ?></textarea>
                                    <label for="question-contents">回答 / Answer</label>
                                </div>
                                <div class="file-field input-field btn-highlight btn-alignleft">
                                    <?php
                                    $file = $this->file_model->get_file('answer', $ans[0]['id']);
                                    if (!empty($file)) {
                                        ?>
                                        <div class="input-field">
                                            <?php
                                            foreach ($file as $val) {
                                                ?>
                                                <a href="<?= $val['file_url'] ?>" download>
                                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                         style="width: 50px"
                                                         alt=""/><?= $this->lang->line('attachment_file') ?>
                                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                                <?php
                                            }
                                            ?>
                                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        ++$i;
                    }
                    ?>

                    <!--
                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" disabled><?= $result_dat['question_body'] ?></textarea>
                        <label for="disabled" class="active">質問内容 / Question</label>
                    </div>

                    <div class="file-field input-field btn-highlight btn-alignleft">
                        <?php
                        if (!empty($result_dat['file_url'])) {
                            $url = $str = ltrim($result_dat['file_url'], './');
                            $url = base_url() . $url;
                            $file_name = $result_dat['file_name'];
                            ?>
                            <a href="<?= $url ?>" download>添付ファイル / Attachment file（<?= $file_name ?>）</a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" disabled><?= $answer_dat['answer_body'] ?></textarea>
                        <label for="disabled" class="active">回答表示 / Answer</label>
                    </div>
                    <?php
                    $file = $this->file_model->get_file('answer', $answer_dat['id']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <a href="<?= $val['file_url'] ?>" download>
                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"style="width: 50px" alt=""/><?= $this->lang->line('attachment_file') ?>
                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                <?php
                            }
                            ?>
                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                        </div>
                        <?php
                    }
                    ?>
                    -->
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <!--<button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action" value="complete" style="width: 500px">回答内容確認 / Confirm and close this Q&A session</button>-->

                    <?php
                    if($this->question_model->chk_answer($_GET['id'])){
                        echo '再質問済みです';
                    }else{
                        ?>
                        <button class="waves-effect waves-light btn-large btn_tertiary" type="submit" name="action"
                                value="requestion" style="width: 300px">再質問 / Continue question
                        </button>
                    <?php
                    }
                    ?>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
<script>
    $("document").ready(function () {
        $(".fileinput").change(fileInputChange);
    });

    function fileInputChange() {
        if ($(".fileinput").last().val() != "") {
            $("#filelist").append('<input type="file" multiple name="userfile[]" class="fileinput" />')
                .bind('change', fileInputChange);
        }
    }

</script>