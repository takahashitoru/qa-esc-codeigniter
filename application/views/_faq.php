<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title">
        <a href="<?= base_url() ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">
                <div class="title-btn">
                    <h5>FAQ</h5>
                    <div class="btn-wrapper btn-aligncenter btn-highlight">
                        <!--
                        <form action="<?= base_url('admin/faq_maintenance') ?>" method="get">
                            <select class="browser-default" name="category" onChange="this.form.submit()">
                                <option value="" disabled selected>カテゴリを選択</option>
                                <?php
                        foreach ($category_dat as $v) {
                            $sel = '';
                            if ($v['code'] == $this->input->get('category')) {
                                $sel = 'selected';
                            }
                            ?>
                                    <option value="<?= $v['code'] ?>"<?= $sel ?>><?= $v['name'] ?></option>
                                    <?php
                        }
                        ?>
                            </select>
                        </form>
                        -->
                    </div>
                </div>

                <div class="input-field">
                    <?php
                    if (isset($_SESSION['er_msg'])) {
                        ?>
                        <div class="alert alert-block alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Error!</h4>
                            <p><?= $_SESSION['er_msg'] ?></p>
                        </div>
                        <?php
                    }
                    if (isset($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-block alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Success!</h4>
                            <p><?= $_SESSION['msg'] ?></p>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="accbox">
                        <?php
                        foreach ($category_dat as $v) {
                            $sel = '';
                            if ($v['code'] == $this->input->get('category')) {
                                $sel = 'selected';
                            }
                            ?>
                            <input type="checkbox" id="label<?= $v['code'] ?>" class="cssacc"/>
                            <label for="label<?= $v['code'] ?>"><?= $v['name'] ?></label>
                            <div class="accshow">
                                <?php
                                $faq = $this->question_model->get_qa($v['id'],'front');
                                //pre_print_r($faq);
                                if ($faq) {
                                    $i = 1;
                                    foreach ($faq as $f) {
                                        $ans = $this->answer_model->get_qa($f['id']);
                                        $requestion = $this->question_model->get_requestion($f['id'],'front');//2回目以降の質問
                                        if ($requestion) {
                                            $i_val = '-1';
                                        } else {
                                            $i_val = '';
                                        }
                                        //pre_print_r($requestion);
                                        ?>
                                        <input type="checkbox" id="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>" class="cssacc"/>
                                        <label for="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>">Q<?= $i ?><?= $i_val ?>
                                            ．<?= nl2br($f['question_body']) ?>
                                        </label>
                                        <?php
                                        if ($ans) {
                                            ?>
                                            <div class="accshow">
                                                <?= nl2br($ans['answer_body']) ?>
                                            </div>
                                            <?php
                                        }
                                        if ($requestion) {
                                            $i2 = 2;
                                            foreach ($requestion as $re) {
                                                $re_ans = $this->answer_model->get_qa($re['id']);
                                                $i_val = '-'.$i2;
                                                ?>
                                                <input type="checkbox" id="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>" class="cssacc"/>
                                                <label for="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>">Q<?= $i ?><?= $i_val ?>
                                                    ．<?= nl2br($re['question_body']) ?>
                                                </label>
                                                <?php
                                                if ($re_ans) {
                                                    ?>
                                                    <div class="accshow">
                                                        <?= nl2br($re_ans['answer_body']) ?>
                                                    </div>
                                                    <?php
                                                }
                                                $i2++;
                                            }
                                        }
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>

                <!--
                <table class="highlight">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>問合せ内容</th>
                        <th>回答</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                //pre_print_r($result_dat);
                if (!empty($result_dat)) {
                    foreach ($result_dat as $v) {
                        ?>
                            <tr>
                                <td><?= sprintf('%04d', $v['id']) ?></td>
                                <td><?= $v["question_body"] ?></td>
                                <td>

                                </td>
                            </tr>
                            <?php
                    }
                }
                ?>
                    </tbody>
                </table>
                -->
            </div>
            <!--
            <?php
            //pager
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/faq_maintenance/';
            pager($now_page, $total_count, $count, $key)
            ?>
            -->
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>

</div>
