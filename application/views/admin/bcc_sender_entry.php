<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>

    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>bcc送信者登録</h5>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="<?=base_url('admin/bcc_sender_form')?>" aria-label="回答者登録">
                                <input type="hidden" name="id" value="<?php
                                if(!empty($form_dat['id'])){
                                    echo $form_dat['id'];
                                }?>">
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">名前</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control<?php
                                        if(isset($_SESSION['er_msg_name'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="name" value="<?php
                                        if(!empty($form_dat['name'])){
                                            echo $form_dat['name'];
                                        }?>"  autofocus>

                                        <?php
                                        if(isset($_SESSION['er_msg_name'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?=$this->lang->line('compulsory')?></strong>
                                    </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control<?php
                                        if(isset($_SESSION['er_msg_email'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="email" value="<?php
                                        if(!empty($form_dat['email'])){
                                            echo $form_dat['email'];
                                        }?>" >

                                        <?php
                                        if(isset($_SESSION['er_msg_email'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>メールアドレスが登録されていません</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if(isset($_SESSION['er_msg_email_double'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>既に登録されているメールアドレスです。</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="input-field btn-wrapper btn-aligncenter mt50">
                                    <button class="waves-effect waves-light btn-large btn_secondary" name="prev" value="prev" type="submit">戻る
                                    </button>
                                    <button class="waves-effect waves-light btn-large btn_primary" type="submit"
                                            onclick="return confirm('このbcc送信者を登録しますか？');">登録
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>

</div>

