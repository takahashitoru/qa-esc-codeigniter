<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout') ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>リストメンテナンス画面</h5>
                    <div class="btn-wrapper btn-aligncenter btn-highlight">
                        <form action="<?= base_url('admin/list_maintenance') ?>" method="get">
                            <?php
                            $class = 'secondary';
                            $section = 'secondary';
                            $category = 'secondary';
                            if ($this->input->get('search') == 'class') {
                                $class = '';
                            } elseif ($this->input->get('search') == 'section') {
                                $section = '';
                            } elseif ($this->input->get('search') == 'category') {
                                $category = '';
                            } else {
                                $class = '';
                            }
                            ?>
                            <button typeof="submit" name="search" value="class"
                                    class="waves-effect waves-light btn <?= $class ?> wide">所属分類
                            </button>
                            <button typeof="submit" name="search" value="section"
                                    class="waves-effect waves-light btn <?= $section ?> wide">部局リスト
                            </button>
                            <button typeof="submit" name="search" value="category"
                                    class="waves-effect waves-light btn <?= $category ?> wide">質問カテゴリ
                            </button>
                        </form>
                    </div>
                </div>

                <table class="highlight">
                    <thead>
                    <tr>
                        <?php
                        if ($this->input->get('search') == 'class') {
                            $type = 'class';
                            $list = '所属リストID';
                            $name_ja = '所属分類名(Ja)';
                            $name_en = '所属分類名(En)';
                        } elseif ($this->input->get('search') == 'section') {
                            $type = 'section';
                            $list = '部局ID';
                            $name_ja = '部局名(Ja)';
                            $name_en = '部局名(En)';
                        } elseif ($this->input->get('search') == 'category') {
                            $type = 'category';
                            $list = 'カテゴリID';
                            $name_ja = 'カテゴリ名(Ja)';
                            $name_en = 'カテゴリ名(En)';
                        } else {
                            $type = 'class';
                            $list = '所属リストID';
                            $name_ja = '所属分類名(Ja)';
                            $name_en = '所属分類名(En)';
                        }
                        ?>
                        <th><?= $list ?></th>
                        <th>並び順</th>
                        <th>コード</th>
                        <th><?= $name_ja ?></th>
                        <th><?= $name_en ?></th>
                        <th>表示</th>
                        <th>更新</th>
                        <th>削除</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //pre_print_r($result_dat);
                    if (!empty($result_dat)) {
                        foreach ($result_dat as $v) {
                            ?>
                            <tr>
                                <td><?= sprintf('%04d', $v['id']) ?></td>
                                <td><?= $v["disp_id"] ?></td>
                                <td><?= $v["code"] ?></td>
                                <td><?= $v["name_ja"] ?></td>
                                <td><?= $v["name_en"] ?></td>
                                <td>
                                    <?php
                                    //表示非表示
                                    if ($v['pub_flg'] == 1) {
                                        ?>
                                        <form action="<?= base_url('admin/list_public') ?>" method="post">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                            <input type="hidden" name="publish" value="0">
                                            <input type="hidden" name="type" value="<?= $type ?>">
                                            <button class="btn btn-primary" type="submit">表示中
                                            </button>
                                        </form>
                                        <?php
                                    } else {
                                        ?>
                                        <form action="<?= base_url('admin/list_public') ?>" method="post">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                            <input type="hidden" name="publish" value="1">
                                            <input type="hidden" name="type" value="<?= $type ?>">
                                            <button class="btn btn-primary" type="submit">非表示
                                            </button>
                                        </form>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#selectModal<?= $v['id'] ?>">
                                        更新
                                    </button>
                                </td>
                                <td>
                                    <form action="<?= base_url('admin/list_delete') ?>" method="post">
                                        <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                        <input type="hidden" name="type" value="<?= $type ?>">
                                        <button class="btn-delete" type="submit"
                                                onclick="return confirm('このリストを削除しますか？削除したリストは復帰できません。');">削除
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade " id="selectModal<?= $v['id'] ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="selectModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">リスト編集</h5>
                                        </div>
                                        <form action="<?= base_url('admin/list_change') ?>" method="post">
                                            <input type="hidden" name="type" value="<?= $type ?>">
                                            <div class="modal-body">
                                                <div class="input-field">
                                                    <input id="id" name="id" type="text" class=""
                                                           value="<?= sprintf('%04d', $v['id']) ?>" readonly>
                                                    <label for="id">ID</label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="disp_id" name="disp_id" type="text" class=""
                                                           value="<?= $v['disp_id'] ?>">
                                                    <label for="disp_id">並び順（小さい順に表示されます）</label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="code" name="code" type="text" class=""
                                                           value="<?= $v['code'] ?>">
                                                    <label for="code">コード</label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="name_ja" name="name_ja" type="text" class=""
                                                           value="<?= $v['name_ja'] ?>">
                                                    <label for="name_ja"><?= $name_ja ?></label>
                                                </div>
                                                <div class="input-field">
                                                    <input id="name_en" name="name_en" type="text" class=""
                                                           value="<?= $v['name_en'] ?>">
                                                    <label for="name_en"><?= $name_en ?></label>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    閉じる
                                                </button>
                                                <input type="submit" class="btn btn-primary" value="更新">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <!-- Modal -->
                    <div class="modal fade " id="ModalNew" tabindex="-1" role="dialog"
                         aria-labelledby="selectModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">リスト新規登録</h5>
                                </div>
                                <form action="<?= base_url('admin/list_entry') ?>" method="post">
                                    <input type="hidden" name="type" value="<?= $type ?>">
                                    <div class="modal-body">
                                        <div class="input-field">
                                            <input id="id" name="id" type="text" class=""
                                                   value="※自動採番されます" readonly>
                                            <label for="id">ID</label>
                                        </div>
                                        <div class="input-field">
                                            <input id="disp_id" name="disp_id" type="text" class=""
                                                   value="0">
                                            <label for="disp_id">並び順（小さい順に表示されます）</label>
                                        </div>
                                        <div class="input-field">
                                            <input id="code" name="code" type="text" class=""
                                                   value="">
                                            <label for="code">コード</label>
                                        </div>
                                        <div class="input-field">
                                            <input id="name_ja" name="name_ja" type="text" class=""
                                                   value="">
                                            <label for="name_ja"><?= $name_ja ?></label>
                                        </div>
                                        <div class="input-field">
                                            <input id="name_en" name="name_en" type="text" class=""
                                                   value="">
                                            <label for="name_en"><?= $name_en ?></label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            閉じる
                                        </button>
                                        <input type="submit" class="btn btn-primary" value="登録">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </tbody>
                </table>
                <br/>
                <div class="btn-wrapper btn-alignleft btn-highlight">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#ModalNew">
                        新規登録
                    </button>
                </div>

            </div>

        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
