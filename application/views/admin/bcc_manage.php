<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">
                <div class="title-btn">
                    <h5>bcc送信者管理画面</h5>
                </div>
                <table id="datatable" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>bcc送信者ID</th>
                        <th>更新日時</th>
                        <th>名前</th>
                        <th>メールアドレス</th>
                        <th>管理</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($result_dat)) {
                        foreach ($result_dat as $v) {
                            ?>
                            <tr>
                                <td><?= sprintf('%04d', $v['id']) ?></td>
                                <td><?= $v["updated_at"] ?></td>
                                <td><?= $v["name"] ?></td>
                                <td><?= $v["email"] ?></td>
                                <td>
                                    <div class="btn-wrapper btn-alignleft btn-highlight">
                                        <form action="<?= base_url('admin/bcc_sender_edit') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$v['id']?>">
                                            <button class="btn-manage" type="submit">編集
                                            </button>
                                        </form>
                                        <form action="<?= base_url('admin/bcc_sender_delete') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$v['id']?>">
                                        <button class="btn-delete" type="submit"
                                                onclick="return confirm('この回答者を削除しますか？削除した回答者は復帰できません。');">削除
                                        </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
                <div class="btn-wrapper btn-alignleft btn-highlight">
                    <form action="<?= base_url('admin/bcc_sender_entry') ?>" method="post">
                        <button class="btn-regist" type="submit">新規登録
                        </button>
                    </form>
                </div>
            </div>

            <!--
            <?php
            //pager使用の際は
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/bcc_manage/';
            pager($now_page, $total_count, $count, $key)
            ?>
            -->

        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
