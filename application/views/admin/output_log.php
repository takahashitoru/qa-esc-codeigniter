<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>ログ出力画面</h5>
                </div>
                <div style="display: inline;">
                    <?php
                    if (isset($_SESSION['er_msg'])) {
                        ?>
                        <div class="alert alert-block alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Error!</h4>
                            <p><?= $_SESSION['er_msg'] ?></p>
                        </div>
                        <?php
                    }
                    if (isset($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-block alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Success!</h4>
                            <p><?= $_SESSION['msg'] ?></p>
                        </div>
                        <?php
                    }
                    ?>
                    <form name="log-span" action="<?= base_url('admin/output') ?>" method="post">
                        <label for="start">取得開始日 - 取得終了日</label>
                        <input type="text" class="auto-kal"  name="range" data-kal="format: 'YYYY/MM/DD', months: 2, mode: 'range', direction: 'today-past'" readonly>
                        <div class="input-field btn-wrapper btn-aligncenter mt50">
                            <a href="<?=base_url('admin')?>" class="waves-effect waves-light btn-large btn_secondary">戻る</a>
                            <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action">取得</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>

</div>
