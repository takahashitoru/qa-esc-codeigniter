<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja-JP" class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html lang="ja-JP" class="no-js ie lt-ie9 lt-ie8"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if IE 8]>
<html lang="ja-JP" class="no-js ie lt-ie9"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if IE 9]>
<html lang="ja-JP" class="no-js ie"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if gt IE 9]><!-->
<html lang="ja-JP" class="no-js" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ログ出力画面(管理者)│東京大学環境安全研究センターQAシステム</title>
    <meta name="format-detection" content="telephone=no">

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/add.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="js/kalendae.standalone.min.js" type="text/javascript"></script>
    <link rel='stylesheet' href='css/kalendae.css' type='text/css' media='all'/>

    <script>
        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth()+1;
        var day   = today.getDate();
        var defDate = yaer+"-"+month+"-"+day;
    </script>
</head>
<body class="one-clum">

<div class="row main-container" style="margin-bottom: 0;">

    <div class="col s12 title2">
        <h1><img src="img/logo2.png" alt="東京大学環境安全研究センター"/></h1>
    </div>

    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>ログ出力画面</h5>
                </div>
                <div style="display: inline;">
                    <form name="log-span">
                        <label for="start">取得開始日 - 取得終了日</label>
                        <input type="text" class="auto-kal" data-kal="format: 'YYYY/MM/DD', months: 2, mode: 'range', direction: 'today-past'">
                        <div class="input-field btn-wrapper btn-aligncenter mt50">
                            <button class="waves-effect waves-light btn-large btn_secondary" type="submit" name="action">戻る</button>
                            <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action">取得</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
        <div class="contents-footer">(c)Environmental Science Center, The University of Tokyo</div>
    </div>

</div>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/add.js"></script>
</body>
</html>
