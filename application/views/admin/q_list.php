<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout') ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>管理者用質問リスト画面</h5>
                    <div class="btn-wrapper btn-aligncenter btn-highlight">
                        <form action="<?= base_url('admin/q_list') ?>" method="get">
                            <?php
                            $all = 'secondary';
                            $notyet = 'secondary';
                            if ($this->input->get('search') == 'all') {
                                $all = '';
                            } elseif ($this->input->get('search') == 'notyet') {
                                $notyet = '';
                            } else {
                                $all = '';
                            }
                            ?>
                            <button typeof="submit" name="search" value="all"
                                    class="waves-effect waves-light btn <?= $all ?> wide">全表示
                            </button>
                            <button typeof="submit" name="search" value="notyet"
                                    class="waves-effect waves-light btn <?= $notyet ?> wide">未振分
                            </button>
                        </form>
                    </div>
                </div>

                <table class="highlight scroll">
                    <thead>
                    <tr>
                        <th>質問ID</th>
                        <th>質問日時</th>
                        <th>質問者</th>
                        <th>E-mail</th>
                        <th>電話番号</th>
                        <th>部局(学内)</th>
                        <th>専攻(学内)</th>
                        <th>研究室(学内)</th>
                        <th>所属分類(学外)</th>
                        <th>カテゴリ</th>
                        <th>タイトル</th>
                        <!--<th>質問内容</th>-->
                        <th>振分日時</th>
                        <!--<th>ステータス</th>-->
                        <th>回答者</th>
                        <th>回答者変更回数</th>
                        <th>質問/回答内容</th>
                        <th>回答日時</th>
                        <th>強制完了</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //pre_print_r($result_dat);
                    if (!empty($result_dat)) {
                        foreach ($result_dat as $v) {
                            $rel_answer = $this->question_model->get_rel_answer($v['id']);
                            $question = $this->question_model->get_re_question($v['id']);
                            if ($question != array()) {
                                $question_count = count($question) + 1;//質問数（+1は親質問）
                            } else {
                                $question_count = 1;
                            }
                            $answer = $this->answer_model->get_answer_detail($v['id']);
                            if ($answer != array()) {
                                $date = $answer[0]['updated_at'];//最終回答日
                                $answer_count = count($answer);//回答数合計
                            } else {
                                $date = '未回答';
                                $answer_count = 0;
                            }
                            //pre_print_r($answer);
                            if ($v["question_category_id"] == '') {
                                $category = '';
                            } else {
                                $category = $this->master_model->get_category_detail('ja', $v["question_category_id"]);
                            }
                            if ($v['re_question_id'] == '') {
                                $id = $v['id'] . '-0';
                            } else {
                                $id = $v['re_question_id'] . '-' . $v['manage_id'];
                            }
                            $answer_change_count = $this->question_model->get_answer_count($v['id']);
                            if (($v['answer_status_id'] == 1) or ($v['answer_status_id'] == 3)) {
                                $status = '未';
                            } else {
                                $status = '完了';
                            }

                            $q_detail = $this->question_model->get_detail($v['id']);
                            $faculty = false;
                            $faculty_data = false;
                            if (!empty($q_detail['faculty_code'])) {
                                $faculty_data = $this->master_model->get_faculty_data($q_detail['faculty_code']);
                            }
                            if ($faculty_data != false) {
                                $faculty = $faculty_data['name_ja'] . '/' . $faculty_data['name_en'];
                            }
                            $affiliation_category = false;
                            $affiliation_category_data = false;
                            if (!empty($q_detail['affiliation_category'])) {
                                $affiliation_category_data = $this->master_model->get_affiliation_category_data($q_detail['affiliation_category']);
                            }
                            if ($affiliation_category_data != false) {
                                $affiliation_category = $affiliation_category_data['name_ja'] . '/' . $affiliation_category_data['name_en'];
                            }
                            if ($this->input->get('search') == 'notyet') {
                                if ($v["answer_status_id"] == 5) {
                                    continue;
                                }
                            }
                            ?>
                            <tr>
                                <td><?= $id ?></td>
                                <td><?= $v['created_at'] ?></td>
                                <td><?= $v["name"] ?></td>
                                <td><?= $q_detail["email"] ?></td>
                                <td><?= $q_detail["tel"] ?></td>
                                <td><?= $faculty ?></td>
                                <td><?= $q_detail["department"] ?></td>
                                <td><?= $q_detail["laboratory"] ?></td>
                                <td><?= $affiliation_category ?></td>
                                <td><?= $category ?></td>
                                <td><?= $v["question_title"] ?></td>
                                <!--
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#selectModalContent1<?= $v['id'] ?>">
                                        表示
                                    </button>
                                </td>
                                -->
                                <td><?= $v["updated_at"] ?></td>
                                <!--<td><?= $status ?></td>-->
                                <td><?php
                                    if ($rel_answer == false) {
                                        if (($v['answer_status_id'] == 1) or ($v['answer_status_id'] == 3)) {
                                            ?>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#selectModal<?= $v['id'] ?>">
                                                未振分
                                            </button>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <p><?= $rel_answer['name'] ?></p>
                                        <?php
                                        $category_id = $this->answer_model->get_category($rel_answer['id']);
                                        $category = $this->master_model->get_category('');
                                        $c_val = '';
                                        foreach ($category as $val) {
                                            if ($category_id != false) {
                                                foreach ($category_id as $c) {
                                                    if ($val['id'] == $c["category_id"]) {
                                                        $c_val .= $val['name_ja'] . ',';
                                                    }
                                                }
                                            }
                                        }
                                        $c_val = rtrim($c_val, ',');
                                        ?>
                                        <p><?= $c_val ?></p>
                                        <?php
                                        if (($v['answer_status_id'] == 1) or ($v['answer_status_id'] == 3)) {
                                            ?>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#selectModal<?= $v['id'] ?>">
                                                変更
                                            </button>
                                            <?php
                                        }
                                    } ?>
                                </td>
                                <td><?= $answer_change_count ?></td>
                                <!--旧
                                <td>
                                    <?php
                                if ($answer != array()) {
                                    ?>
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#selectModalContent2<?= $v['id'] ?>">
                                            表示
                                        </button>
                                        <?php
                                } else {
                                    ?>
                                        未回答
                                        <?php
                                }
                                ?>
                                </td>
                                -->
                                <td>
                                    <a href="<?= base_url('admin/confirm?q_id=') . base64_encode($v['id']) ?>"
                                       class="btn btn-primary btn-small">
                                        表示
                                    </a>
                                </td>
                                <td><?= $date ?></td>
                                <td>
                                    <?php
                                    if (($v['answer_status_id'] == 1) or ($v['answer_status_id'] == 3)) {
                                        ?>
                                        <div class="btn-wrapper btn-alignleft btn-highlight">
                                            <form action="<?= base_url('admin/forced_done') ?>" method="get">
                                                <input type="hidden" name="q_id" value="<?= $v['id'] ?>">
                                                <button class="btn-manage" type="submit"
                                                        onclick="return confirm('この質問を完了しますか？');">
                                                    完了
                                                </button>
                                            </form>
                                        </div>
                                        <?php
                                    } elseif ($v['answer_status_id'] == 5) {
                                        ?>
                                        <p style="color: red">強制完了</p>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade " id="selectModalContent1<?= $v['id'] ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="selectModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">質問内容</h5>
                                        </div>
                                        <?= nl2br($v['question_body']) ?>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                閉じる
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade " id="selectModalContent2<?= $v['id'] ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="selectModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">回答内容</h5>
                                        </div>
                                        <?= nl2br($answer[0]['answer_body']) ?>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                閉じる
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade " id="selectModal<?= $v['id'] ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="selectModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">回答者選択</h5>
                                        </div>
                                        <form action="<?= base_url('admin/answer_change') ?>" method="post">
                                            <input type="hidden" name="id" value="<?= $v['id'] ?>">
                                            <div class="modal-body">
                                                <div class="input-field" style="height: 60px;">
                                                    <p>回答者<span class="required"></span></p>
                                                    <label>
                                                        <?php
                                                        //pre_print_r($answer_dat)
                                                        ?>
                                                        <select class="browser-default" name="answer_id"
                                                                style="margin-top: 20px; margin-bottom: 50px;">
                                                            <option value="" disabled
                                                                    selected>回答者を選択してください
                                                            </option>
                                                            <?php
                                                            foreach ($answer_dat as $val) {
                                                                ?>
                                                                <option value="<?= $val['id'] ?>"><?= $val['name'] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    閉じる
                                                </button>
                                                <input type="submit" class="btn btn-primary" value="保存">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <?php
            //pre_print_r($total_count);
            //pager
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/q_list';
            pager($now_page, $total_count, $count, $key)
            ?>

        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
