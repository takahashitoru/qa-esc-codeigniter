<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>ログ管理画面</h5>
                </div>

                <table class="highlight">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>日時</th>
                        <th>受付/回答</th>
                        <th>名前</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if (!empty($result_dat)) {
                        foreach ($result_dat as $v) {
                            $name = $this->activity_model->get_act($v);
                            ?>
                            <tr>
                                <td><?=$v['id']?></td>
                                <td><?=$v['updated_at']?></td>
                                <td><?=$v['name']?></td>
                                <td><?=$name?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <?php
            //pager
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/log_manage/';
            pager($now_page, $total_count, $count, $key)
            ?>

        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
