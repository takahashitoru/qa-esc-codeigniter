<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m12 contents">
        <div class="contents-inner">
            <form action="<?=base_url('admin/admin_form')?>" class="col s12 m12" style="margin-right: auto;" method="post">
                <input type="hidden" name="id" value="<?php
                if(!empty($form_dat['id'])){
                    echo $form_dat['id'];
                }?>">
                <div class="title-btn">
                    <h5>管理者登録</h5>
                </div>
                新たに管理者を登録します。
                <div class="input-field">
                    <input id="name" type="text" name="name"  class="validate" value="<?php
                    if(!empty($form_dat['name'])){
                        echo $form_dat['name'];
                    }?>">
                    <label for="name">お名前<span class="required">※</span></label>
                </div>
                <div class="input-field">
                    <input id="user_code" type="text" name="user_code" class="validate" value="<?php
                    if(!empty($form_dat['user_code'])){
                        echo $form_dat['user_code'];
                    }?>">
                    <label for="name">ログインID<span class="required">※</span></label>
                </div>
                <div class="input-field">
                    <input id="email" type="text" name="email" class="validate" value="<?php
                    if(!empty($form_dat['email'])){
                        echo $form_dat['email'];
                    }?>" >
                    <label for="email">メールアドレス<span class="required">※</span></label>
                </div>
                <div class="input-field">
                    <input id=”password” type="password" name="password" class="validate">
                    <label for="password">パスワード<span class="required">※</span></label>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <button class="waves-effect waves-light btn-large btn_secondary" type="submit" name="cancel">キャンセル
                    </button>
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit"
                            onclick="return confirm('この管理者を登録しますか？');">登録
                    </button>
                </div>
            </form>
            <div class="contents-footer"><?= COPYRIGHT ?></div>
        </div>
    </div>

