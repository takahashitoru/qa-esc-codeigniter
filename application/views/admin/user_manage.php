<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">
                <div class="title-btn">
                    <h5>回答者管理画面</h5>
                </div>
                <table id="datatable" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>回答者ID</th>
                        <th>更新日時</th>
                        <th>名前</th>
                        <th>メールアドレス</th>
                        <th>ログインID</th>
                        <th>担当カテゴリ</th>
                        <th>最新回答日時</th>
                        <th>管理質問数</th>
                        <th>管理</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($result_dat)) {
                        foreach ($result_dat as $v) {
                            ?>
                            <tr>
                                <td><?= sprintf('%04d', $v['id']) ?></td>
                                <td><?= $v["updated_at"] ?></td>
                                <td><?= $v["name"] ?></td>
                                <td><?= $v["email"] ?></td>
                                <td><?= $v["user_code"] ?></td>
                                <?php
                                $category_id = $this->answer_model->get_category($v['id']);
                                $c_val = '';
                                    foreach ($category as $val){
                                        if($category_id != false){
                                            foreach ($category_id as $c){
                                                if($val['id'] == $c["category_id"]){
                                                    $c_val .= $val['name'].',';
                                                }
                                            }
                                        }
                                        /*
                                        if($val['id'] == $v["category"]){
                                            $c_val = $val['name'];
                                        }
                                        */
                                    }
                                    $c_val = rtrim($c_val, ',');
                                ?>
                                <td><?= $c_val ?></td>
                                <?php
                                $latest_answer_time = $this->question_model->latest_answer_time($v['id']);
                                $latest_answer_count = $this->question_model->latest_answer_count($v['id']);
                                ?>
                                <td><?= $latest_answer_time ?></td>
                                <td><?= $latest_answer_count ?></td>
                                <td>
                                    <div class="btn-wrapper btn-alignleft btn-highlight">
                                        <form action="<?= base_url('admin/answer_edit') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$v['id']?>">
                                            <button class="btn-manage" type="submit">編集
                                            </button>
                                        </form>
                                        <form action="<?= base_url('admin/answer_delete') ?>" method="post">
                                            <input type="hidden" name="id" value="<?=$v['id']?>">
                                        <button class="btn-delete" type="submit"
                                                onclick="return confirm('この回答者を削除しますか？削除した回答者は復帰できません。');">削除
                                        </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
                <div class="btn-wrapper btn-alignleft btn-highlight">
                    <form action="<?= base_url('admin/answer_entry') ?>" method="post">
                        <button class="btn-regist" type="submit">新規登録
                        </button>
                    </form>
                </div>
            </div>

            <!--
            <?php
            //pager使用の際は
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/user_manage/';
            pager($now_page, $total_count, $count, $key)
            ?>
            -->

        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
