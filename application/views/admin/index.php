<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?=base_url('admin/page')?>" class="col s12 m8" style="margin-right: auto;" method="post">
                <div class="input-field btn-wrapper btn-aligncenter btn-full mt50">
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="list">管理者用質問リスト画面</button>
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="user_manage">回答者管理</button>
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="bcc_manage">bcc送信先管理</button>
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="list_maintenance">リストメンテナンス</button>
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="log_manage">ログ出力</button>
                    <!--<button class="waves-effect waves-light btn-large" type="submit" name="type" value="log_output">ログ出力</button>-->
                    <button class="waves-effect waves-light btn-large" type="submit" name="type" value="faq_maintenance">FAQ</button>
                    <!--<button class="waves-effect waves-light btn-large" type="submit" name="type" value="faq_edit">FAQ編集</button>-->
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
