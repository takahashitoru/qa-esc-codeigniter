<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>

    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">

                <div class="title-btn">
                    <h5>回答者登録</h5>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="<?=base_url('admin/answer_form')?>" aria-label="回答者登録">
                                <input type="hidden" name="id" value="<?php
                                if(!empty($form_dat['id'])){
                                    echo $form_dat['id'];
                                }?>">
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">名前</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control<?php
                                        if(isset($_SESSION['er_msg_name'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="name" value="<?php
                                        if(!empty($form_dat['name'])){
                                            echo $form_dat['name'];
                                        }?>"  autofocus>

                                        <?php
                                        if(isset($_SESSION['er_msg_name'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?=$this->lang->line('compulsory')?></strong>
                                    </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">担当カテゴリ</label>
                                    <div class="col-md-6">
                                    <?php
                                    $category_id = false;
                                    if(!empty($form_dat['id'])){
                                        $category_id = $this->answer_model->get_category($form_dat['id']);
                                    }

                                    foreach ($category as $val){
                                        $chk = '';
                                        if($category_id != false){
                                            foreach ($category_id as $c){
                                                if($val['id'] == $c["category_id"]){
                                                    $chk = "checked";
                                                }
                                            }
                                        }

                                        /*
                                        if(!empty($form_dat['category'])) {
                                            if ($form_dat['category'] == $cat['id']) {
                                                $chk = "checked";
                                            }
                                        }
                                        */
                                        ?>
                                        <label>
                                            <input class="with-gap" name="category[]" value="<?=$val['id']?>" type="checkbox" <?=$chk?>/>
                                            <span><?=$val['name']?></span>
                                        </label>
                                        <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control<?php
                                        if(isset($_SESSION['er_msg_email'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="email" value="<?php
                                        if(!empty($form_dat['email'])){
                                            echo $form_dat['email'];
                                        }?>" >

                                        <?php
                                        if(isset($_SESSION['er_msg_email'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>メールアドレスが登録されていません</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if(isset($_SESSION['er_msg_email_double'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>既に登録されているメールアドレスです。</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="user_code" class="col-md-4 col-form-label text-md-right">ログインID</label>

                                    <div class="col-md-6">
                                        <input id="user_code" type="text" class="form-control<?php
                                        if(isset($_SESSION['er_msg_user_code'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="user_code" value="<?php
                                        if(!empty($form_dat['user_code'])){
                                            echo $form_dat['user_code'];
                                        }?>" >

                                        <?php
                                        if(isset($_SESSION['er_msg_user_code'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>ログインIDが入力されていません</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if(isset($_SESSION['er_msg_user_code_double'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>既に登録されているログインIDです。</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">パスワード</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control<?php
                                        if(isset($_SESSION['er_msg_password'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" name="password" >

                                        <?php
                                        if(isset($_SESSION['er_msg_password'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>パスワードが入力されていません</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">パスワード確認</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" name="password_confirm" class="form-control<?php
                                        if(isset($_SESSION['er_msg_password_confirm'])){
                                            ?> is-invalid <?php
                                        }
                                        ?>" >
                                        <?php
                                        if(isset($_SESSION['er_msg_password_confirm'])){
                                            ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>確認用と一致しません</strong>
                                            </span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="input-field btn-wrapper btn-aligncenter mt50">
                                    <button class="waves-effect waves-light btn-large btn_secondary" name="prev" value="prev" type="submit">戻る
                                    </button>
                                    <button class="waves-effect waves-light btn-large btn_primary" type="submit"
                                            onclick="return confirm('この回答者を登録しますか？');">登録
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>

</div>

