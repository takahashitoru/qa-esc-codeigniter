<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('admin') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('admin/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">
                <div class="title-btn">
                    <h5>FAQ管理画面</h5>
                    <div class="btn-wrapper btn-aligncenter btn-highlight">
                        <!--
                        <form action="<?= base_url('admin/faq_maintenance') ?>" method="get">
                            <select class="browser-default" name="category" onChange="this.form.submit()">
                                <option value="" disabled selected>カテゴリを選択</option>
                                <?php
                        foreach ($category_dat as $v) {
                            $sel = '';
                            if ($v['code'] == $this->input->get('category')) {
                                $sel = 'selected';
                            }
                            ?>
                                    <option value="<?= $v['code'] ?>"<?= $sel ?>><?= $v['name'] ?></option>
                                    <?php
                        }
                        ?>
                            </select>
                        </form>
                        -->
                    </div>
                </div>

                <div class="input-field">
                    <?php
                    if (isset($_SESSION['er_msg'])) {
                        ?>
                        <div class="alert alert-block alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Error!</h4>
                            <p><?= $_SESSION['er_msg'] ?></p>
                        </div>
                        <?php
                    }
                    if (isset($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-block alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <h4 class="alert-heading">Success!</h4>
                            <p><?= $_SESSION['msg'] ?></p>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="accbox">
                        <?php
                        foreach ($category_dat as $v) {
                            $sel = '';
                            if ($v['code'] == $this->input->get('category')) {
                                $sel = 'selected';
                            }
                            ?>
                            <input type="checkbox" id="label<?= $v['code'] ?>" class="cssacc"/>
                            <label for="label<?= $v['code'] ?>"><?= $v['name'] ?></label>
                            <div class="accshow">
                                <?php
                                $faq = $this->faq_model->get_qa($v['id']);
                                //pre_print_r($faq);
                                if ($faq) {
                                    $i = 0;
                                    foreach ($faq as $f) {
                                        if ($f['manage_id'] == 1) {
                                            ++$i;
                                        }
                                        $i_val = '-' . $f['manage_id'];
                                        ?>
                                        <input type="checkbox" id="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>"
                                               class="cssacc"/>
                                        <label for="label<?= $v['code'] ?>-<?= $i ?><?= $i_val ?>">Q<?= $i ?><?= $i_val ?>
                                            ．<?= nl2br($f['question_body']) ?>
                                            <br/>
                                            <br/>
                                            <?php
                                            //表示非表示
                                            if ($f['pub_flg'] == 1) {
                                                ?>
                                                <a href="<?= base_url('admin/publish/hide/' . $f['id']) ?>/"
                                                   class="btn <?= $i ?>">
                                                    表示中</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?= base_url('admin/publish/disp/' . $f['id']) ?>/"
                                                   class="btn <?= $i ?>">非表示</a>
                                                <?php
                                            }
                                            ?>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#selectModal1<?= $f['id'] ?>">
                                                編集
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade " id="selectModal1<?= $f['id'] ?>" tabindex="-1" role="dialog"
                                                 aria-labelledby="selectModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">編集</h5>
                                                        </div>
                                                        <form action="<?= base_url('admin/faq_edit') ?>" method="post">
                                                            <input type="hidden" name="id" value="<?= $f['id'] ?>">
                                                            <div class="modal-body">
                                                                <div class="input-field">
                                                                    <p>質問内容<span class="required"></span></p>
                                                                    <textarea id="question-contents" name="question_body"><?php
                                                                        if (!empty($f['question_body'])) {
                                                                            echo $f['question_body'];
                                                                        } ?></textarea>
                                                                    <p>回答内容<span class="required"></span></p>
                                                                    <textarea id="question-contents" name="answer_body"><?php
                                                                        if (!empty($f['answer_body'])) {
                                                                            echo $f['answer_body'];
                                                                        } ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                    閉じる
                                                                </button>
                                                                <input type="submit" style="display:inline;" class="btn btn-primary" value="保存">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                        <div class="accshow">
                                            <?= nl2br($f['answer_body']) ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>

                <!--
                <table class="highlight">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>問合せ内容</th>
                        <th>回答</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                //pre_print_r($result_dat);
                if (!empty($result_dat)) {
                    foreach ($result_dat as $v) {
                        ?>
                            <tr>
                                <td><?= sprintf('%04d', $v['id']) ?></td>
                                <td><?= $v["question_body"] ?></td>
                                <td>

                                </td>
                            </tr>
                            <?php
                    }
                }
                ?>
                    </tbody>
                </table>
                -->
            </div>
            <!--
            <?php
            //pager
            if ($this->input->get('page')) {
                $now_page = $this->input->get('page');
            } else {
                $now_page = 1;
            }
            $count = 20;//1ページ10件
            $key = 'admin/faq_maintenance/';
            pager($now_page, $total_count, $count, $key)
            ?>
            -->
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>

</div>
