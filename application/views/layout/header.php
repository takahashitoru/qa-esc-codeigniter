<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ja-JP" class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html lang="ja-JP" class="no-js ie lt-ie9 lt-ie8"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if IE 8]>
<html lang="ja-JP" class="no-js ie lt-ie9"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if IE 9]>
<html lang="ja-JP" class="no-js ie"  xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<![endif]-->
<!--[if gt IE 9]><!-->
<html lang="ja-JP" class="no-js" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<!--<![endif]-->
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=$page_title?> │ <?=SITE_NAME?></title>
    <meta name="format-detection" content="telephone=no">

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <?php
    if(!empty($datatable)){
        ?>
        <link rel="stylesheet" href="https://cdn.datatables.net/t/bs-3.3.6/jqc-1.12.0,dt-1.10.11/datatables.min.css"/>
        <script src="https://cdn.datatables.net/t/bs-3.3.6/jqc-1.12.0,dt-1.10.11/datatables.min.js"></script>
        <script>
            jQuery(function($){
                $.extend( $.fn.dataTable.defaults, {
                    language: {
                        url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Japanese.json"
                    }
                });

                $("#datatable").dataTable();
            });
        </script>
        <?php
    }
    ?>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/materialize.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/add.css" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <script src="<?=base_url()?>assets/js/kalendae.standalone.min.js" type="text/javascript"></script>
    <link rel='stylesheet' href='<?=base_url()?>assets/css/kalendae.css' type='text/css' media='all'/>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script>
        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth()+1;
        var day   = today.getDate();
        var defDate = year+"-"+month+"-"+day;
    </script>

</head>
<?php
if($page_type == 'common'){
?>
<body>
<?php

}elseif($page_type == 'one'){
?>
<body class="one-clum">
<?php
}elseif($page_type == 'confirm'){
?>
<body class="confirm">
<?php
}?>