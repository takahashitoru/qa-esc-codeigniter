<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <a href="<?= base_url('center') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?=base_url('center/login')?>" class="col s12 m8" style="margin-right: auto;" method="get">
                <div class="input-field btn-wrapper btn-aligncenter btn-full mt50">
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="role" value="answer">回答者
                    </button>
                    <button class="waves-effect waves-light btn-large btn_secondary" type="submit" name="role" value="admin">管理者
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>