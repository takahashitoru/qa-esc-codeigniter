
<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <a href="<?= base_url('center') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form method="post" action="<?=base_url('center/login_process')?>" class="col s12 m6">
                <div class="input-field">
                    <input placeholder="Usercode" id="usercode" type="text" class="validate" name="user_code" value="<?php
                    if(!empty($form_dat['user_code'])){
                        echo $form_dat['user_code'];
                    }?>" required autofocus>
                </div>
                <div class="input-field">
                    <input placeholder="Password" id="password" type="password" class="validate" name="password" value="<?php
                    if(!empty($form_dat['password'])){
                        echo $form_dat['password'];
                    }?>" required autofocus>
                </div>

                <div class="input-field">
                    <?php
                    if(isset($_SESSION['er_msg_wrong'])){
                        ?>
                        <span class="required">User ID password is wrong.</span><br>
                        <?php
                    }
                    ?>
                    <?php
                    if(isset($_SESSION['er_msg_required'])){
                        ?>
                        <span class="required">Please enter correct user ID and password.</span>
                        <?php
                    }
                    ?>
                </div>

                <div class="input-field" style="overflow: hidden;">
                    <div class="col s6">
                        <!--
                        <label>
                            <input type="checkbox" name="remember" class="filled-in" />
                            <span>Remember me</span>
                        </label>
                        -->
                    </div>
                    <div class="col s6" style="text-align: right;">
                        <a href="<?=base_url('center/reminder?role='.$_GET['role'])?>">Forgot Password</a>
                    </div>
                </div>

                <div class="input-field mt50">
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action">LOGIN</button>
                </div>
                <input type="hidden" name="role" value="<?=$this->input->get('role')?>">
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
