<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('center') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m12 contents">
        <div class="contents-inner noflex">
            <form action="<?= base_url('center/reminder_process') ?>" class="col s12 m12" style="margin-right: auto;" method="post">
                <input type="hidden" name="role" value="<?=$_GET['role']?>">
                <div class="title-btn">
                    <?php
                    if($_GET['role'] == 'admin'){
                        $name = '管理者';
                    }else{
                        $name = '回答者';
                    }
                    ?>
                    <h5>パスワード設定メール送信(<?=$name?>)</h5>
                </div>
                登録されているメールアドレスに変更用のURLをお知らせします。
                <div class="input-field">
                    <input id="id" type="text" class="validate" name="user_code" value="<?php
                    if (!empty($form_dat['user_code'])) {
                        echo $form_dat['user_code'];
                    } ?>">
                    <label for="id">ログインID<span class="required">※</span></label>
                    <?php
                    if (isset($_SESSION['er_msg_user_code'])) {
                        ?>
                        <!--エラー時はlabelに.activeを付与-->
                        <span class="helper-text"><?=  "必須項目(※は入力必須です)"; ?></span>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field">
                    <input id="email" type="text" class="validate" name="email" value="<?php
                    if (!empty($form_dat['email'])) {
                        echo $form_dat['email'];
                    } ?>">
                    <label for="email">メールアドレス<span class="required">※</span></label>
                    <?php
                    if (isset($_SESSION['er_msg_email'])) {
                        ?>
                        <!--エラー時はlabelに.activeを付与-->
                        <span class="helper-text"><?=  "必須項目(※は入力必須です)"; ?></span>
                        <?php
                    }
                    ?>
                    <?php
                    if (isset($_SESSION['er_msg_wrong'])) {
                        ?>
                        <!--エラー時はlabelに.activeを付与-->
                        <span class="helper-text">該当するアカウントが見つかりません</span>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <a href="<?= base_url('center') ?>" class="waves-effect waves-light btn-large btn_secondary">戻る
                    </a>
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action">送信
                    </button>
                </div>
            </form>
            <div class="contents-footer"><?= COPYRIGHT ?></div>
        </div>
    </div>