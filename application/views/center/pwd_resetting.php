<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url('center') ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m12 contents">
        <div class="contents-inner">
            <form action="<?=base_url('center/pwd_resetting_process')?>" method="post" class="col s12 m12" style="margin-right: auto;">
                <input type="hidden" name="hash" value="<?=$this->input->get('hash')?>">
                <input type="hidden" name="role" value="<?=$this->input->get('role')?>">
                <div class="title-btn">
                    <?php
                    if($role == 'admin'){
                        $name = '管理者';
                    }else{
                        $name = '回答者';
                    }
                    ?>
                    <h5>パスワード再設定 (<?=$name?>)</h5>
                </div>
                新しいパスワードを登録してください。
                <div class="input-field">
                    <input id="id" name="user_code" type="text" class="validate" value="<?php
                    if (!empty($form_dat['user_code'])) {
                        echo $form_dat['user_code'];
                    } ?>">
                    <label for="id">ログインID<span class="required">※</span></label>
                    <?php
                    if (isset($_SESSION['er_msg_user_code'])) {
                        ?>
                        <!--エラー時はlabelに.activeを付与-->
                        <span class="helper-text"><?=  "必須項目(※は入力必須です)"; ?></span>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field">
                    <input id="password" type="password" class="validate" name="password">
                    <label for="password">パスワード<span class="required">※</span></label>
                    <?php
                    if(isset($_SESSION['er_msg_password'])){
                        ?>
                        <span class="invalid-feedback" role="alert">
                            <strong>パスワードが入力されていません</strong>
                        </span>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field">
                    <input id="password2" type="password" class="validate" name="password2">
                    <label for="password2">パスワード確認用<span class="required">※</span></label>
                    <?php
                    if(isset($_SESSION['er_msg_password_confirm'])){
                        ?>
                        <span class="invalid-feedback" role="alert">
                            <strong>確認用と一致しません</strong>
                        </span>
                        <?php
                    }
                    ?>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <a href="<?= base_url('center') ?>" class="waves-effect waves-light btn-large btn_secondary">戻る
                    </a>
                    <button class="waves-effect waves-light btn-large btn_primary" type="submit" name="action">登録
                    </button>
                </div>
            </form>
            <div class="contents-footer"><?= COPYRIGHT ?></div>
        </div>
    </div>
