このメールは送信専用メールアドレスから配信されています。
ご返信いただいてもお答えできませんのでご了承ください。
This is an automated email sent from the system and do not reply to this email.
───────────────────────────────────

{name}様/Dear {name}

以下のとおりご質問を受け付けました。
We have received your question as follows:

受付日時（Date & time） : {date_e}

質問ID（Question ID） : {id}

質問者 : {name}

E-mail : {email}

電話番号 : {tel}

部局(学内) : {faculty}

専攻(学内) : {department}

研究室(学内)　: {laboratory}

所属分類(学外) : {affiliation_category}

カテゴリ : {category}

質問タイトル（Question title） : {question_title}

質問内容 （Question）:
{question_body}


質問の修正はこちらから
Click here to modify your question -> {url}

新規の質問はこちらから
Click here to new question -> {base_url}

───────────────────────────────────
当センターへのお問い合わせ
Any inquiries, contact us at
e-mail : {reply_mail}

東京大学 環境安全研究センター
Environmental Science Center, The University of Tokyo
http://www.esc.u-tokyo.ac.jp/