管理者様

以下のとおりご質問を受け付けました。

受付日時	{date}

ID: {id}

質問者 : {name}

E-mail : {email}

電話番号 : {tel}

部局(学内) : {faculty}

専攻(学内) : {department}

研究室(学内)　: {laboratory}

所属分類(学外) : {affiliation_category}

カテゴリ : {category}

質問タイトル：{question_title}

質問内容：{question_body}

管理画面はこちら→ {admin_url}
