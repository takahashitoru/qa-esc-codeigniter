以下の通り回答されました

受付日時 : {date_e}
質問ID : {id}
質問タイトル : {question_title}

質問者 : {name}

E-mail : {email}

電話番号 : {tel}

部局(学内) : {faculty}

専攻(学内) : {department}

研究室(学内)　: {laboratory}

所属分類(学外) : {affiliation_category}

カテゴリ : {category}

質問内容 :
{question_body}

回答内容 :
{answer_body}