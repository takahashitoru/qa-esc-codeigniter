{name}様

回答者の指定／変更がありました。
回答はこちらから → https://qa.esc.u-tokyo.ac.jp/center/

受付日時 : {date_e}

質問ID : {id}

質問者 : {answer_name}

E-mail : {email}

電話番号 : {tel}

部局(学内) : {faculty}

専攻(学内) : {department}

研究室(学内)　: {laboratory}

所属分類(学外) : {affiliation_category}

カテゴリ : {category}

質問タイトル : {question_title}

質問内容 : {question_body}