Dear {name}

 We answered your question as follows.
Reception : {date_e}

Question ID : {id}

質問者 : {name}

E-mail : {email}

電話番号 : {tel}

部局(学内) : {faculty}

専攻(学内) : {department}

研究室(学内)　: {laboratory}

所属分類(学外) : {affiliation_category}

カテゴリ : {category}

Question title ：{question_title}
Question :
{question_body}

Click here for confirm the answer→ {url}


─────────────Contact Us────────────
This email is delivered from the e-mail address for exclusive use of the transmission.
Because I cannot answer even if I have you reply, thank you for your understanding.

Inquiries to our center (except questions and answers)
e-mail	{admin_mail}
───────────────────────────────────