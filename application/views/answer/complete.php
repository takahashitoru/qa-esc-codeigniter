<body class="confirm">
<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <a href="<?= base_url() . 'answer' ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>

    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <div class="input-field">

                <h5><?=$this->lang->line('sent_questioner_mes1')?></h5>
                <div class="input-field">
                    <?=$this->lang->line('sent_questioner_mes2')?>
                </div>

            </div>

        </div>
        <div class="contents-footer"><?=COPYRIGHT?></div>
    </div>

</div>