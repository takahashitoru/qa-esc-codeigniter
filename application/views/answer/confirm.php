<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <a href="<?= base_url() . 'answer' ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('answer/process') ?>" class="col s12 m8" style="margin-right: auto;"
                  method="post" enctype="multipart/form-data"><?= $this->lang->line('test') ?>
                <input type="hidden" name="q_id" value="<?= $re_form_dat['q_id'] ?>">
                <input type="hidden" name="re_question_id" value="<?= $form_dat['re_question_id'] ?>">
                <input type="hidden" name="manage_id" value="<?= $form_dat['manage_id'] ?>">
                <input type="hidden" name="type" value="<?= $re_form_dat['type'] ?>">

                <div class="input-field">
                    <h5><?= $this->lang->line('answer_confirm') ?></h5>
                    <br/>
                    <?php
                    //pre_print_r($form_dat);
                    if ($form_dat['re_question_id'] == '') {
                        $id = $form_dat['id'] . '-0';
                    } else {
                        $id = $form_dat['re_question_id'] . '-' . $form_dat['manage_id'];
                    }
                    ?>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $id ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('q_id') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $form_dat['created_at'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('q_date') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $form_dat['updated_at'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('sorting_date') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $form_dat['name'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('name') ?></label>
                    </div>

                    <?php
                    //pre_print_r($form_dat);
                    if ($form_dat['type'] == 'outside') {
                        ?>
                        <div class="input-field">
                            <input id="affiliation" name="affiliation" value="<?php
                            if (!empty($form_dat['affiliation'])) {
                                echo $form_dat['affiliation'];
                            } ?>" type="text" readonly>
                            <label for="affiliation"><?= $this->lang->line('affiliation') ?></label>
                        </div>
                        <div class="input-field">
                            <?php
                            $affiliation_name = '';
                            $affiliation_code = '';
                            foreach ($affiliation as $cat) {
                                if (!empty($form_dat['affiliation_category'])) {
                                    if ($form_dat['affiliation_category'] == $cat['id']) {
                                        $affiliation_name = $cat['name'];
                                        $affiliation_code = $cat['id'];
                                    }
                                }
                            } ?>
                            <input id="affiliation" type="text" value="<?= $affiliation_name ?>" readonly>
                            <input type="hidden" name="affiliation" value="<?= $affiliation_code ?>">
                            <label for="affiliation"><?= $this->lang->line('affiliation_category') ?></label>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="input-field">
                        <input id="tel" name="tel" type="tel" value="<?php
                        if (!empty($form_dat['tel'])) {
                            echo $form_dat['tel'];
                        } ?>" readonly>
                        <label for="tel"><?= $this->lang->line('phone') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="email" name="email" type="email" value="<?= $form_dat['email'] ?>" readonly>
                        <label for="email">E-mail</label>
                    </div>

                    <?php
                    if ($form_dat['type'] != 'outside') {
                        ?>
                        <!--学内-->
                        <div class="input-field">
                            <?php
                            $name = '';
                            $code = '';
                            foreach ($faculty as $item) {
                                if (!empty($form_dat['faculty_code'])) {
                                    if ($form_dat['faculty_code'] == $item['code']) {
                                        $name = $item['name'];
                                        $code = $item['code'];
                                    }
                                }
                            }
                            ?>
                            <input id="department" name="faculty_code" type="text" value="<?= $name ?>" readonly>
                            <input name="faculty_code" type="hidden" value="<?= $code ?>" readonly>
                            <label for="department"><?= $this->lang->line('faculty_school') ?></label>
                        </div>
                        <div class="input-field">
                            <input id="department" name="department" type="text" value="<?php
                            if (!empty($form_dat['department'])) {
                                echo $form_dat['department'];
                            } ?>" readonly>
                            <label for="department"><?= $this->lang->line('department') ?></label>
                        </div>

                        <div class="input-field">
                            <input id="laboratory" name="laboratory" type="text" value="<?php
                            if (!empty($form_dat['laboratory'])) {
                                echo $form_dat['laboratory'];
                            } ?>" readonly>
                            <label for="laboratory"><?= $this->lang->line('laboratory') ?></label>
                        </div>
                        <div class="input-field">
                            <?php
                            $category_name = '';
                            $category_code = '';
                            foreach ($category as $item) {
                                if (!empty($form_dat['question_category_id'])) {
                                    if ($form_dat['question_category_id'] == $item['id']) {
                                        $category_name = $item['name'];
                                        $category_code = $item['id'];
                                    }
                                }
                            } ?>
                            <input id="faculty_school" type="text" value="<?= $category_name ?>" readonly>
                            <input type="hidden" name="question_category_id" value="<?= $category_code ?>">
                            <label for="faculty_school"><?= $this->lang->line('question_category') ?></label>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="input-field">
                        <input id="question-title" name="question_title" type="text"
                               value="<?= $form_dat['question_title'] ?>" readonly>
                        <label for="question-title"><?= $this->lang->line('question_title') ?></label>
                    </div>
                    <?php
                    $requestion = $this->question_model->get_base_q($re_form_dat['q_id']);
                    $i = 1;
                    if (!empty($requestion)) {
                        foreach ($requestion as $req) {
                            if ($i % 2 === 0) {
                                $color = '#FFEEFF';
                            } else {
                                $color = '#EEFFFF';
                            }
                            //pre_print_r($req);
                            ?>
                            <div style="padding:  20px; background-color: <?= $color ?>">
                                <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $req['question_body'] ?></textarea>
                                    <label for="question-contents">質問内容 / Question</label>
                                </div>
                                <div class="file-field input-field btn-highlight btn-alignleft">
                                    <?php
                                    $file = $this->file_model->get_file('question', $req['id']);
                                    if (!empty($file)) {
                                        ?>
                                        <div class="input-field">
                                            <?php
                                            foreach ($file as $val) {
                                                ?>
                                                <a href="<?= $val['file_url'] ?>" download>
                                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                         style="width: 50px"
                                                         alt=""/><?= $this->lang->line('attachment_file') ?>
                                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                                <?php
                                            }
                                            ?>
                                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                $ans = $this->answer_model->get_answer_detail($req['id']);
                                if (!empty($ans)) {
                                    ?>
                                    <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" name="question_body" maxlength="2000"
                                      readonly><?= $ans[0]['answer_body'] ?></textarea>
                                        <label for="question-contents">回答 / Answer</label>
                                    </div>
                                    <div class="file-field input-field btn-highlight btn-alignleft">
                                        <?php
                                        $file = $this->file_model->get_file('answer', $ans[0]['id']);
                                        if (!empty($file)) {
                                            ?>
                                            <div class="input-field">
                                                <?php
                                                foreach ($file as $val) {
                                                    ?>
                                                    <a href="<?= $val['file_url'] ?>" download>
                                                        <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"
                                                             style="width: 50px"
                                                             alt=""/><?= $this->lang->line('attachment_file') ?>
                                                        （<?= $val['file_name'] ?>）</a><br/><br/>
                                                    <?php
                                                }
                                                ?>
                                                <span class="helper-text">※ファイルをクリックでダウンロード</span>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            ++$i;
                        }
                    }
                    ?>

                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" name="question_body"
                                  readonly><?= $form_dat['question_body'] ?></textarea>
                        <label for="question-contents"><?= $this->lang->line('question') ?></label>
                    </div>
                    <?php
                    $file = $this->file_model->get_file('question', $form_dat['id']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <a href="<?= $val['file_url'] ?>" download>
                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>" style="width: 50px" alt=""/>
                                    <?= $this->lang->line('attachment_file') ?>（<?= $val['file_name'] ?>）</a><br/><br/>
                                <?php
                            }
                            ?>
                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                        </div>
                        <?php
                    }
                    ?>
                    <div style="background-color: yellow"><!--わかりやすく黄色にしています-->
                        <div class="input-field input-textarea with_file">
                            <textarea id="answer-contents" name="answer_body" readonly><?php
                                if (!empty($re_form_dat['answer_body'])) {
                                    echo $re_form_dat['answer_body'];
                                } ?></textarea>

                            <label for="answer-contents"><?= $this->lang->line('answer_body') ?></label>
                            <?php
                            if (isset($_SESSION['er_msg_answer_body'])) {
                                ?>
                                <!--エラー時はlabelに.activeを付与-->
                                <span class="helper-text"><?= $this->lang->line('compulsory') ?></span>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        $file = $this->file_model->get_file('answer', $_SESSION['tmp_key']);
                        if (!empty($file)) {
                            ?>
                            <div class="input-field">
                                <?php
                                foreach ($file as $val) {
                                    ?>
                                    <input id="file_name" name="file_name" type="text" value="<?= $val['file_name'] ?>"
                                           readonly>
                                    <?php
                                }
                                ?>
                                <label for="file_name">添付ファイル / Attachment file</label>
                                <span class="helper-text">※「戻る」をクリックした場合には、添付ファイルを再度選択する必要があります / If you click 'Back', you need to select the attached file again</span>
                            </div>
                            <?php
                        }
                        //pre_print_r($re_form_dat);
                        ?>
                    </div>
                    <!--cc追加-->
                    <div class="input-field" style="height: 150px;">
                        <p>メール送信先CC追加</p>
                        <label>
                            <select class="browser-default" name="add_mail[]"
                                    style="margin-top: 40px; margin-bottom: 50px;height: 100px" multiple disabled>
                                <!--<option value="">回答者を選択してください</option>-->
                                <?php
                                $this->db->where('m_bcc_sender.del_flg', 0);
                                $this->db->order_by('m_bcc_sender.id', 'desc');
                                $query = $this->db->get('m_bcc_sender');
                                $answer_dat = $query->result_array();
                                if ($answer_dat != array()) {
                                    foreach ($answer_dat as $val) {
                                        $sel = '';
                                        if (!empty($re_form_dat['add_mail'])) {
                                            foreach ($re_form_dat['add_mail'] as $add_mail) {
                                                if ($val['email'] == $add_mail) {
                                                    $sel = 'selected';
                                                }
                                            }
                                        }
                                        ?>
                                        <option value="<?= $val['email'] ?>" <?= $sel ?>><?= $val['name'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </label>
                    </div>
                    <div class="input-field">
                        <input id="add_mail_free" type="text" name="add_mail_free" value="<?php
                        if (!empty($re_form_dat['add_mail_free'])) {
                            echo $re_form_dat['add_mail_free'];
                        } ?>" maxlength="255" disabled>
                        <label for="add_mail_free">メール送信先CC追加（フリー入力）</label>
                        <span class="helper-text">※複数のメールアドレスは「,」(カンマ)で区切ってください</span>
                        <?php
                        if (isset($_SESSION['er_msg_add_mail_free'])) {
                            ?>
                            <!--エラー時はlabelに.activeを付与-->
                            <span class="helper-text">有効でないメールアドレスが含まれています</span>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <button class="waves-effect waves-light btn-large btn_secondary" name="prev" value="prev"
                            type="submit"><?= $this->lang->line('prev') ?>
                    </button>
                    <button class="waves-effect waves-light btn-large btn_primary"
                            type="submit"><?= $this->lang->line('send') ?>
                    </button>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
