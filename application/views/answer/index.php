<div class="row" style="margin-bottom: 0;">
    <div class="col s12 title2">
        <a href="<?= base_url() . 'answer' ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <a href="<?= base_url('top/logout' ) ?>/" class="btn ">logout</a>
    <div class="col s12 contents">
        <div class="contents-inner">
            <div class="table-field">
                <div class="title-btn">
                    <h5>質問リスト</h5>
                    <div class="btn-wrapper btn-aligncenter btn-highlight">
                        <button class="waves-effect waves-light btn wide" type="button"
                                onclick="location.href='https://www.google.co.jp/'">全表示
                        </button>
                        <button class="waves-effect waves-light btn secondary wide" type="button"
                                onclick="location.href='https://www.google.co.jp/'">未振分
                        </button>
                        <button class="waves-effect waves-light btn secondary wide" type="button"
                                onclick="location.href='https://www.google.co.jp/'">未回答
                        </button>
                    </div>
                </div>
                @dump($question)

                <table class="highlight">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>日時</th>
                        <th>カテゴリ</th>
                        <th>質問者</th>
                        <th>タイトル</th>
                        <th>回答者</th>
                        <th>最新回答日時</th>
                        <th>質問数</th>
                        <th>回答数</th>
                        <th>回答者変更フラグ</th>
                        <th>ステータス</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $question as $item )
                    <tr>
                        <td>{{sprintf('%04d', $item->question_id)}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>{{$item->name_ja}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->question_title}}</td>
                        <td>
                            @if( $item->answer_user_id === null )
                            {{$item->title}}
                            @else
                            {{$item->answer_user_id}}
                            @endif
                        </td>
                        <td>{{$item->updated_at}}</td>
                        <td>{{$item->question_count}}</td>
                        <td>{{$item->answer_count}}</td>
                        <td>{{$item->answer_change_hst}}</td>
                        <td>{{$item->title}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="input-field btn-aligncenter mt50">
                <ul class="pagination">
                    {{ $question->links() }}
                </ul>
            </div>
            <div class="input-field btn-aligncenter mt50">
                <ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!">4</a></li>
                    <li class="waves-effect"><a href="#!">5</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>