<div class="row main-container" style="margin-bottom: 0;">
    <div class="col s12 m3 title2">
        <?php
        if (isset($_SESSION['admin_dat'])) {
            $url_key = 'admin';
        }else{
            $url_key = 'answer';
        }
        ?>
        <a href="<?= base_url($url_key) ?>">
            <h1><img src="<?= base_url() . 'assets/img/logo2.png' ?>" alt="<?= LOGO_ALT ?>"/></h1>
        </a>
    </div>
    <div class="col s12 m9 contents">
        <div class="contents-inner">
            <form action="<?= base_url('question/answer_complete') ?>" class="col s12 m8" style="margin-right: auto;"
                  method="post">
                <input type="hidden" name="q_id" value="<?= base64_encode($result_dat['id']) ?>">
                <div class="input-field">
                    <h5>質問/回答内容表示</h5>
                    <br/>
                    <?php
                    //pre_print_r($form_dat);
                    if ($result_dat['re_question_id'] == '') {
                        $id = $result_dat['id'] . '-0';
                    } else {
                        $id = $result_dat['re_question_id'] . '-' . $result_dat['manage_id'];
                    }
                    ?>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $id ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('q_id') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $result_dat['created_at'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('q_date') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $result_dat['updated_at'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('sorting_date') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" name="name" value="<?= $result_dat['name'] ?>" type="text" readonly>
                        <label for="name"><?= $this->lang->line('name') ?></label>
                    </div>
                    <div class="input-field">
                        <input id="name" disabled value="<?= $result_dat['question_title'] ?>" type="text"
                               class="validate">
                        <label for="disabled">質問タイトル / Question title</label>
                    </div>


                    <div class="input-field input-textarea with_file">
                        <textarea id="question-contents" disabled><?= $result_dat['question_body'] ?></textarea>
                        <label for="disabled" class="active">質問内容 / Question</label>
                    </div>

                    <?php
                    $file = $this->file_model->get_file('question', $result_dat['id']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <a href="<?= $val['file_url'] ?>" download>
                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"style="width: 50px" alt=""/><?= $this->lang->line('attachment_file') ?>
                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                <?php
                            }
                            ?>
                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if($answer_dat['answer_body'] == ''){
                        ?>
                        <!--未回答-->
                        <?php
                    }else{
                        ?>
                        <div class="input-field input-textarea with_file">
                            <textarea id="question-contents" disabled><?= $answer_dat['answer_body'] ?></textarea>
                            <label for="disabled" class="active">回答表示 / Answer</label>
                        </div>
                    <?php
                    }
                    ?>

                    <!--
                    <div class="file-field input-field btn-highlight btn-alignleft">
                        <?php
                    if (!empty($answer_dat['file_url'])) {
                        $url_a = $str = ltrim($answer_dat['file_url'], './');
                        $url_a = base_url() . $url_a;
                        $file_name_a = $answer_dat['file_name'];
                        ?>
                            <a href="<?= $url_a ?>" download>添付ファイル / Attachment file（<?= $file_name_a ?>）</a>
                            <?php
                    }
                    ?>
                    </div>
                    -->
                    <?php
                    $file = $this->file_model->get_file('answer', $answer_dat['id']);
                    if (!empty($file)) {
                        ?>
                        <div class="input-field">
                            <?php
                            foreach ($file as $val) {
                                ?>
                                <a href="<?= $val['file_url'] ?>" download>
                                    <img src="<?= base_url() . 'assets/img/pdf.jpg' ?>"style="width: 50px" alt=""/><?= $this->lang->line('attachment_file') ?>
                                    （<?= $val['file_name'] ?>）</a><br/><br/>
                                <?php
                            }
                            ?>
                            <span class="helper-text">※ファイルをクリックでダウンロード</span>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <div class="input-field btn-wrapper btn-aligncenter mt50">
                    <a class="waves-effect waves-light btn-large btn_tertiary" href="<?=$_SERVER['HTTP_REFERER']?>" style="width: 300px">戻る
                    </a>
                </div>
            </form>
        </div>
        <div class="contents-footer"><?= COPYRIGHT ?></div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
<script>
    $("document").ready(function () {
        $(".fileinput").change(fileInputChange);
    });

    function fileInputChange() {
        if ($(".fileinput").last().val() != "") {
            $("#filelist").append('<input type="file" multiple name="userfile[]" class="fileinput" />')
                .bind('change', fileInputChange);
        }
    }

</script>
